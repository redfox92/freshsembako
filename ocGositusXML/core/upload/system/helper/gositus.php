<?php
/**
 * gositus helper
 *
 * A helper file for common function
 *
 * @package		Helper
 * @author		Gositus
 * @copyright	Copyright (c) 2016, Gositus.
 * @link			http://gositus.com
 * @since		Version 1.0
 * @filesource
 */

// --------------------------------------------------------------------

/**
 * Print variable in readable format and stop php execution by default
 *
 * @param	mixed	the variable to be printed. Support all variable types (array, object, string, integer, etc) 
 * @param	boolean	whether to stop php execution or not. (Default=true)
 */
if(!function_exists('pre')) {
	function pre($variable, $exit=true) {
		echo '<pre>';
		print_r($variable);
		echo '</pre>';
		if($exit) exit;
	}
}

/**
 * Check if current IP is Gositus IP
 *
 * @return boolean check result (true or false)
 */
if(!function_exists('ip_gositus')) {
	function ip_gositus() {
		if($_SERVER['REMOTE_ADDR']=='122.102.40.67') return true;
		return false;
	}
}