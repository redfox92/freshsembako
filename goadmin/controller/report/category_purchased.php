<?php
class ControllerReportCategoryPurchased extends Controller {
	public function index() {
		$this->load->language('report/category_purchased');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$filter_order_status_id = $this->request->get['filter_order_status_id'];
		} else {
			$filter_order_status_id = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/category_purchased', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->load->model('report/category');

		$data['categories'] = array();

		$filter_data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_order_status_id' => $filter_order_status_id,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin')
		);

		// $category_total = $this->model_report_category->getTotalPurchased($filter_data);

		$results = $this->model_report_category->getPurchased($filter_data);
		$data['categories'] = $results;
		// pre($data['categories']);
		// $grandtotal = 0;
		// $total_quantity = 0;
		// foreach ($results as $result) {
		// 	$data['categories'][] = array(
		// 		'category_id'   => $result['category_id'],
		// 		'category_name' => $result['category_name'],
		// 		'quantity'      => $result['quantity'],
		// 		'total'         => $this->currency->format($result['total'], $this->config->get('config_currency')),
		// 		'href'          => $this->url->link('report/category_purchased/purchasedCustomerByCategory', 'token=' . $this->session->data['token']. '&category_id=' . $result['category_id'], true)
		// 	);
		// 	$total_quantity += $result['quantity'];
		// 	$grandtotal += $result['total'];
		// }
		// $data['total_quantity'] = $total_quantity;
		$data['total_quantity'] = '';
		// $data['grandtotal'] = $this->currency->format($grandtotal, $this->config->get('config_currency'));
		$data['grandtotal'] = '';

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_all_status'] = $this->language->get('text_all_status');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_category'] = $this->language->get('column_category');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_stock'] = $this->language->get('column_stock');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_grandtotal'] = $this->language->get('column_grandtotal');

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		// $pagination = new Pagination();
		// $pagination->total = $category_total;
		// $pagination->page = $page;
		// $pagination->limit = $this->config->get('config_limit_admin');
		// $pagination->url = $this->url->link('report/category_purchased', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		// $data['pagination'] = $pagination->render();
		$data['pagination'] = '';

		// $data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));
		$data['results'] = '';

		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;
		$data['filter_order_status_id'] = $filter_order_status_id;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/category_purchased', $data));
	}

	public function purchasedCustomerByCategory()
	{
		$this->load->language('report/purchased_customer_by_category');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');

		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_invoice_no'] = $this->language->get('column_invoice_no');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_grandtotal'] = $this->language->get('column_grandtotal');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('title_before'),
			'href' => $this->url->link('report/category_purchased', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/category_purchased/purchasedCustomerByProduct', 'token=' . $this->session->data['token'], true)
		);

		$this->load->model('catalog/category');

		$category = $this->model_catalog_category->getCategory($this->request->get['category_id']);

		$data['product_name'] = $category['name'];
		$data['product_href'] = $this->url->link('catalog/category/edit', 'token=' . $this->session->data['token'] . '&category_id=' . $category['category_id'], true);

		$this->load->model('report/category');
		$results = $this->model_report_category->getPurchasedCustomerByCategory($this->request->get['category_id']);

		$data['customers'] = array();
		$grand_total = 0;
		foreach ($results as $result) {
			$data['customers'][] = array(
				'order_id'   => $result['order_id'],
				'invoice_no' => $result['invoice_no'],
				'href'       => $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'], true),
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'quantity'   => $result['quantity'],
				'total'      => $this->currency->format($result['quantity'] * $result['price'], $this->config->get('config_currency')),
			);
			$grand_total += ($result['quantity'] * $result['price']);
		}

		$data['grand_total'] = $this->currency->format($grand_total, $this->config->get('config_currency'));

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/purchased_customer_by_category', $data));
	}
}