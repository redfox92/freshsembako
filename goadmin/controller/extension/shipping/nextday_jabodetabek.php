<?php
class ControllerExtensionShippingNextdayJabodetabek extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/shipping/nextday_jabodetabek');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('nextday_jabodetabek', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');

		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		// pre($this->weight->getUnit($this->config->get('config_weight_class_id')));

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_shipping'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/shipping/nextday_jabodetabek', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('extension/shipping/nextday_jabodetabek', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true);

		if (isset($this->request->post['nextday_jabodetabek_geo_zone_id'])) {
			$data['nextday_jabodetabek_geo_zone_id'] = $this->request->post['nextday_jabodetabek_geo_zone_id'];
		} else {
			$data['nextday_jabodetabek_geo_zone_id'] = $this->config->get('nextday_jabodetabek_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['nextday_jabodetabek_weight'])) {
			$data['nextday_jabodetabek_weight'] = $this->request->post['nextday_jabodetabek_weight'];
		} elseif ($this->config->get('nextday_jabodetabek_weight')) {
			$data['nextday_jabodetabek_weight'] = $this->config->get('nextday_jabodetabek_weight');
		} else {
			$data['nextday_jabodetabek_weight'] = 15;
		}

		if (isset($this->request->post['nextday_jabodetabek_price'])) {
			$data['nextday_jabodetabek_price'] = $this->request->post['nextday_jabodetabek_price'];
		} else {
			$data['nextday_jabodetabek_price'] = $this->config->get('nextday_jabodetabek_price');
		}

		if (isset($this->request->post['nextday_jabodetabek_status'])) {
			$data['nextday_jabodetabek_status'] = $this->request->post['nextday_jabodetabek_status'];
		} else {
			$data['nextday_jabodetabek_status'] = $this->config->get('nextday_jabodetabek_status');
		}

		if (isset($this->request->post['nextday_jabodetabek_sort_order'])) {
			$data['nextday_jabodetabek_sort_order'] = $this->request->post['nextday_jabodetabek_sort_order'];
		} else {
			$data['nextday_jabodetabek_sort_order'] = $this->config->get('nextday_jabodetabek_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/shipping/nextday_jabodetabek', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/shipping/nextday_jabodetabek')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}