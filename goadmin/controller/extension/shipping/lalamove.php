<?php
class ControllerExtensionShippingLalamove extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/shipping/lalamove');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('lalamove', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_none'] = $this->language->get('text_none');
		$data['entry_weight_class'] = $this->language->get('entry_weight_class');

		$data['help_weight_class'] = $this->language->get('help_weight_class');

		$data['entry_weight'] = $this->language->get('entry_weight');
		$data['entry_max_weight'] = $this->language->get('entry_max_weight');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		// pre($this->weight->getUnit($this->config->get('config_weight_class_id')));

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_shipping'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/shipping/lalamove', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('extension/shipping/lalamove', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true);

		if (isset($this->request->post['lalamove_geo_zone_id'])) {
			$data['lalamove_geo_zone_id'] = $this->request->post['lalamove_geo_zone_id'];
		} else {
			$data['lalamove_geo_zone_id'] = $this->config->get('lalamove_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['lalamove_max_weight'])) {
			$data['lalamove_max_weight'] = $this->request->post['lalamove_max_weight'];
		} else {
			$data['lalamove_max_weight'] = $this->config->get('lalamove_max_weight');
		}

		if (isset($this->request->post['lalamove_status'])) {
			$data['lalamove_status'] = $this->request->post['lalamove_status'];
		} else {
			$data['lalamove_status'] = $this->config->get('lalamove_status');
		}

		if (isset($this->request->post['sicepat_weight_class_id'])) {
			$data['sicepat_weight_class_id'] = $this->request->post['sicepat_weight_class_id'];
		} else {
			$data['sicepat_weight_class_id'] = $this->config->get('sicepat_weight_class_id');
		}

		$this->load->model('localisation/weight_class');

		$data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();

		if (isset($this->request->post['lalamove_sort_order'])) {
			$data['lalamove_sort_order'] = $this->request->post['lalamove_sort_order'];
		} else {
			$data['lalamove_sort_order'] = $this->config->get('lalamove_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/shipping/lalamove', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/shipping/lalamove')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}