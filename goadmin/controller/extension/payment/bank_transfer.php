<?php
class ControllerExtensionPaymentBankTransfer extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/payment/bank_transfer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('bank_transfer', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true));
		}

		// get bank transfer list
		if (isset($this->request->post['bank_transfer_list'])) {
			$data['bank_transfer_list'] = $this->request->post['bank_transfer_list'];
		}
		else {
			$data['bank_transfer_list'] = $this->config->get('bank_transfer_list');
		}

		// get bank image
		if (isset($this->request->post['bank_transfer_list_image'])) {
			$data['bank_transfer_list_image'] = $this->request->post['bank_transfer_list_image'];
		}
		else {
			$data['bank_transfer_list_image'] = $this->config->get('bank_transfer_list_image');
		}

		$this->load->model('tool/image');
		// default image
		$data['bank_transfer_list_image_default'] = $this->model_tool_image->resize('catalog/bank_transfer/default.png', 100, 100);

		// set bank image
		if (isset($data['bank_transfer_list'])) {
			foreach ($data['bank_transfer_list'] as $key => $value) {
				$data['bank_transfer_list_image_thumb'][$value] = $this->model_tool_image->resize($data['bank_transfer_list_image'][$value], 100, 100);

				if (empty($data['bank_transfer_list_image_thumb'][$value])) {
					$data['bank_transfer_list_image_thumb'][$value] = $data['bank_transfer_list_image_default'];
				}
			}
		}

		$data['bank_transfer_list_bank'] = $this->config->get('bank_transfer_list_bank');
		
		// set bank name
		if (isset($this->request->post['bank_transfer_list_name'])) {
			$data['bank_transfer_list_name'] = $this->request->post['bank_transfer_list_name'];
		}
		else {
			$data['bank_transfer_list_name'] = $this->config->get('bank_transfer_list_name');
		}

		// set bank code
		if (isset($this->request->post['bank_transfer_list_code'])) {
			$data['bank_transfer_list_code'] = $this->request->post['bank_transfer_list_code'];
		}
		else {
			$data['bank_transfer_list_code'] = $this->config->get('bank_transfer_list_code');
		}

		// set bank status
		if (isset($this->request->post['bank_transfer_list_code'])) {
			$data['bank_transfer_list_code'] = $this->request->post['bank_transfer_list_code'];
		}
		else {
			$data['bank_transfer_list_code'] = $this->config->get('bank_transfer_list_code');
		}

		// set bank status
		if (isset($this->request->post['bank_transfer_list_status'])) {
			$data['bank_transfer_list_status'] = $this->request->post['bank_transfer_list_status'];
		}
		else {
			$data['bank_transfer_list_status'] = $this->config->get('bank_transfer_list_status');
		}

		// set bank sort
		if (isset($this->request->post['bank_transfer_list_sort'])) {
			$data['bank_transfer_list_sort'] = $this->request->post['bank_transfer_list_sort'];
		}
		else {
			$data['bank_transfer_list_sort'] = $this->config->get('bank_transfer_list_sort');
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_general'] = $this->language->get('text_general');
		$data['text_new_bank'] = $this->language->get('text_new_bank');

		$data['entry_bank'] = $this->language->get('entry_bank');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_order_status'] = $this->language->get('entry_order_status');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_bank_image'] = $this->language->get('entry_bank_image');
		$data['entry_bank_name'] = $this->language->get('entry_bank_name');
		$data['entry_bank_code'] = $this->language->get('entry_bank_code');
		$data['entry_bank_sort'] = $this->language->get('entry_bank_sort');

		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();

		// for error each field
		if (isset($this->request->post['bank_transfer_list'])) {
			foreach ($this->request->post['bank_transfer_list'] as $key => $value) {
				// error description
				foreach ($languages as $language) {
					if (isset($this->error['bank_transfer_list_bank_' . $value . '_' .  $language['language_id']])) {
						$data['error_bank_transfer_list_bank_' . $value . '_' .  $language['language_id']] = $this->error['bank_transfer_list_bank_' . $value . '_' .  $language['language_id']];
					} else {
						$data['error_bank_transfer_list_bank_' . $value . '_' .  $language['language_id']] = '';
					}
				}

				// error bank name
				if (isset($this->error['bank_transfer_list_name_' . $value])) {
					$data['error_bank_transfer_list_name_' . $value] = $this->error['bank_transfer_list_name_' . $value];
				}

				// error bank code
				if (isset($this->error['bank_transfer_list_code_' . $value])) {
					$data['error_bank_transfer_list_code_' . $value] = $this->error['bank_transfer_list_code_' . $value];
				}
			}
		}
		else {
			if (isset($data['bank_transfer_list'])) {
				foreach ($data['bank_transfer_list'] as $key => $value) {
					// error description
					foreach ($languages as $language) {
						$data['error_bank_transfer_list_bank_' . $value . '_' .  $language['language_id']] = '';
					}

					// error bank name
					$data['error_bank_transfer_list_name_' . $value] = '';

					// error bank code
					$data['error_bank_transfer_list_code_' . $value] = '';
				}
			}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/payment/bank_transfer', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('extension/payment/bank_transfer', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true);

		$this->load->model('localisation/language');

		if (isset($data['bank_transfer_list'])) {
			foreach ($data['bank_transfer_list'] as $key => $value) {
				foreach ($languages as $language) {
					if (isset($this->request->post['bank_transfer_list_bank' . $language['language_id']])) {
						$data['bank_transfer_list_bank' . $language['language_id']][$value] = $this->request->post['bank_transfer_list_bank' . $language['language_id']][$value];
					} else {
						$data['bank_transfer_list_bank' . $language['language_id']][$value] = $this->config->get('bank_transfer_list_bank' . $language['language_id'])[$value];
					}
				}
			}
		}

		$data['languages'] = $languages;

		if (isset($this->request->post['bank_transfer_name'])) {
			$data['bank_transfer_name'] = $this->request->post['bank_transfer_name'];
		} else {
			$data['bank_transfer_name'] = $this->config->get('bank_transfer_name');
		}

		if (isset($this->request->post['bank_transfer_total'])) {
			$data['bank_transfer_total'] = $this->request->post['bank_transfer_total'];
		} else {
			$data['bank_transfer_total'] = $this->config->get('bank_transfer_total');
		}

		if (isset($this->request->post['bank_transfer_order_status_id'])) {
			$data['bank_transfer_order_status_id'] = $this->request->post['bank_transfer_order_status_id'];
		} else {
			$data['bank_transfer_order_status_id'] = $this->config->get('bank_transfer_order_status_id');
		}

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['bank_transfer_geo_zone_id'])) {
			$data['bank_transfer_geo_zone_id'] = $this->request->post['bank_transfer_geo_zone_id'];
		} else {
			$data['bank_transfer_geo_zone_id'] = $this->config->get('bank_transfer_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['bank_transfer_status'])) {
			$data['bank_transfer_status'] = $this->request->post['bank_transfer_status'];
		} else {
			$data['bank_transfer_status'] = $this->config->get('bank_transfer_status');
		}

		if (isset($this->request->post['bank_transfer_sort_order'])) {
			$data['bank_transfer_sort_order'] = $this->request->post['bank_transfer_sort_order'];
		} else {
			$data['bank_transfer_sort_order'] = $this->config->get('bank_transfer_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/payment/bank_transfer', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/bank_transfer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();
		foreach ($this->request->post['bank_transfer_list'] as $key => $value) {
			foreach ($languages as $language) {
				if (empty($this->request->post['bank_transfer_list_bank' . $language['language_id']][$value]) && $this->request->post['bank_transfer_list_bank' . $language['language_id']][$value] != '<p><br></p>') {
					$this->error['bank_transfer_list_bank_' . $value . '_' .  $language['language_id']] = $this->language->get('error_bank');
				}
			}
			if (empty($this->request->post['bank_transfer_list_name'][$value])) {
				$this->error['bank_transfer_list_name_'. $value] = $this->language->get('error_bank_name');
			}

			if (empty($this->request->post['bank_transfer_list_code'][$value])) {
				$this->error['bank_transfer_list_code_'. $value] = $this->language->get('error_bank_code');
			}
		}

		return !$this->error;
	}
}