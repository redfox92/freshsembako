<?php
// Heading
$_['heading_title']       = 'Subscribes';

// Text
$_['text_success']        = 'Success: You have modified subscribes!';
$_['text_list']           = 'Subscribe List';
$_['text_add']            = 'Add Subscribe';
$_['text_edit']           = 'Edit Subscribe';
$_['text_percent']        = 'Percentage';
$_['text_amount']         = 'Fixed Amount';
$_['text_bulk']           = '(Bulk) ';

// Column
$_['column_email']        = 'Email';
$_['column_code']         = 'Code';
$_['column_discount']     = 'Discount';
$_['column_date_start']   = 'Date Start';
$_['column_date_end']     = 'Date End';
$_['column_status']       = 'Status';
$_['column_order_id']     = 'Order ID';
$_['column_customer']     = 'Customer';
$_['column_amount']       = 'Amount';
$_['column_date_added']   = 'Date Added';
$_['column_action']       = 'Action';
$_['column_code']         = 'Subscribe Code';

// Entry
$_['entry_name']          = 'Subscribe Name';
$_['entry_code']          = 'Code';
$_['entry_type']          = 'Type';
$_['entry_discount']      = 'Discount';
$_['entry_logged']        = 'Customer Login';
$_['entry_shipping']      = 'Free Shipping';
$_['entry_total']         = 'Total Amount';
$_['entry_category']      = 'Category';
$_['entry_product']       = 'Products';
$_['entry_date_start']    = 'Date Start';
$_['entry_date_end']      = 'Date End';
$_['entry_uses_total']    = 'Uses Per Subscribe';
$_['entry_uses_customer'] = 'Uses Per Customer';
$_['entry_status']        = 'Status';

// Help
$_['help_code']           = 'The code the customer enters to get the discount.';
$_['help_type']           = 'Percentage or Fixed Amount.';
$_['help_logged']         = 'Customer must be logged in to use the subscribe.';
$_['help_total']          = 'The total amount that must be reached before the subscribe is valid.';
$_['help_category']       = 'Choose all products under selected category.';
$_['help_product']        = 'Choose specific products the subscribe will apply to. Select no products to apply subscribe to entire cart.';
$_['help_uses_total']     = 'The maximum number of times the subscribe can be used by any customer. Leave blank for unlimited';
$_['help_uses_customer']  = 'The maximum number of times the subscribe can be used by a single customer. Leave blank for unlimited';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify subscribes!';
$_['error_exists']        = 'Warning: Subscribe code is already in use!';
$_['error_name']          = 'Subscribe Name must be between 3 and 128 characters!';
$_['error_name_unique']   = 'Subscribe Name must be unique';
$_['error_code']          = 'Code must be between 3 and 10 characters!';

//custome text
$_['entry_total']         = 'Minimum Transaction';
$_['entry_uses_total']    = 'Total Subscribe';
$_['entry_uses_customer'] = 'Max Per Customer';
$_['error_bulk_subscribe']   = "Bulk subscribe total is required";