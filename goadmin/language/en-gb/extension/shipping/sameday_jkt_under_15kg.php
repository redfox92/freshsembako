<?php
// Heading
$_['heading_title']      = 'Same Day Jabodetabek <=15kg Porter';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified Same Day Jabodetabek <=15kg Porter shipping!';
$_['text_edit']        = 'Edit Same Day Jabodetabek <=15kg Porter Shipping';

// Entry
$_['entry_price']      = 'Price';
$_['entry_geo_zone']   = 'Geo Zone';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Same Day Jabodetabek <=15kg Porter shipping!';