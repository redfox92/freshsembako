<?php
// Heading
$_['heading_title']    = 'Sicepat REG';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified Sicepat Reg!';
$_['text_edit']        = 'Edit Sicepat Reg Shipping';

// Entry
$_['entry_geo_zone']   = 'Geo Zone';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';
$_['entry_weight_class'] = 'Weight Class';

$_['help_weight_class']  = 'Set to kilogram';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Sicepat Reg!';