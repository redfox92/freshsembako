<?php
// Heading
$_['heading_title']      = 'Same Day Bodetabek';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified same day shipping!';
$_['text_edit']        = 'Edit Same Day Shipping';

// Entry
$_['entry_max_weight'] = 'Max Weight';
$_['entry_price']      = 'Price';
$_['entry_geo_zone']   = 'Geo Zone';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify same day shipping!';