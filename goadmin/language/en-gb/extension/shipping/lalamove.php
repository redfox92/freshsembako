<?php
// Heading
$_['heading_title']      = 'Lalamove';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified Lalamove shipping!';
$_['text_edit']        = 'Edit Lalamove Shipping';

// Entry
$_['entry_max_weight']   = 'Max Weight';
$_['entry_price']        = 'Price';
$_['entry_geo_zone']     = 'Geo Zone';
$_['entry_status']       = 'Status';
$_['entry_sort_order']   = 'Sort Order';
$_['entry_weight_class'] = 'Weight Class';

$_['help_weight_class']  = 'Set to kilogram';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Lalamove shipping!';