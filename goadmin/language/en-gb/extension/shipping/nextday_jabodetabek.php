<?php
// Heading
$_['heading_title']      = 'Next Day Jabodetabek';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified next day shipping!';
$_['text_edit']        = 'Edit Next Day Shipping';

// Entry
$_['entry_price']      = 'Price';
$_['entry_geo_zone']   = 'Geo Zone';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify next day shipping!';