<?php
// Heading
$_['heading_title']    = 'GOJEK';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified GOJEK!';
$_['text_edit']        = 'Edit GOJEK Shipping';

// Entry
$_['entry_geo_zone']   = 'Geo Zone';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';
$_['entry_fee'] = 'Shipping Fee';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify GOJEK!';