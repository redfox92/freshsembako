<?php
// Heading
$_['heading_title']    = 'JNE Ansurance';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Success: You have modified JNE Ansurance total!';
$_['text_edit']        = 'Edit JNE Ansurance';
$_['text_sweden']      = 'Sweden';
$_['text_norway']      = 'Norway';
$_['text_finland']     = 'Finland';
$_['text_denmark']     = 'Denmark';
$_['text_germany']     = 'Germany';
$_['text_netherlands'] = 'The Netherlands';

// Entry
$_['entry_total']      = 'Order Total';
$_['entry_ansurance']  = 'Ansurance Fee';
$_['entry_tax_class']  = 'Tax Class';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify JNE Ansurance total!';