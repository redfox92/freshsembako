<?php
// Heading
$_['heading_title']    = 'Porter Insurance';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Success: You have modified Porter Insurance total!';
$_['text_edit']        = 'Edit Porter Insurance';
$_['text_sweden']      = 'Sweden';
$_['text_norway']      = 'Norway';
$_['text_finland']     = 'Finland';
$_['text_denmark']     = 'Denmark';
$_['text_germany']     = 'Germany';
$_['text_netherlands'] = 'The Netherlands';

// Entry
$_['entry_total']      = 'Order Total';
$_['entry_ansurance']  = 'Ansurance Fee';
$_['entry_tax_class']  = 'Tax Class';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';
$_['entry_min_subtotal'] = 'Min Subtotal';
$_['entry_percent'] = 'Percent';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Porter Insurance total!';