<?php
// Heading
$_['heading_title']     = 'Products Purchased Report';

// Text
$_['text_list']         = 'Products Purchased List';
$_['text_all_status']   = 'All Statuses';

// Column
$_['column_date_start'] = 'Date Start';
$_['column_date_end']   = 'Date End';
$_['column_name']       = 'Product Name';
$_['column_category']   = 'Category';
$_['column_quantity']   = 'Quantity';
$_['column_stock']      = 'Stock';
$_['column_total']      = 'Total';
$_['column_grandtotal'] = 'Grand Total';

// Entry
$_['entry_date_start']  = 'Date Start';
$_['entry_date_end']    = 'Date End';
$_['entry_status']      = 'Order Status';