<?php
// Heading
$_['heading_title']     = 'Purchased Customer By Category';
$_['title_before'] 	    = 'Categories Purchased Report';

$_['text_list']         = 'Customers Purchased Category';

$_['column_order_id']   = 'Order ID';
$_['column_invoice_no'] = 'Invoice No';
$_['column_name']       = 'Name';
$_['column_quantity']   = 'Quantity';
$_['column_total']      = 'Total';
$_['column_grandtotal'] = 'Grand Total';