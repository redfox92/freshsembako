<?php
// Heading
$_['heading_title']		 = 'Bank Transfer';

// Text
$_['text_extension']	 = 'Extensions';
$_['text_success']		 = 'Success: You have modified bank transfer details!';
$_['text_edit']          = 'Edit Bank Transfer';
$_['text_general']       = 'General Settings';
$_['text_new_bank']      = 'Add New Bank';

// Entry
$_['entry_bank']		 = 'Bank Transfer Instructions';
$_['entry_total']		 = 'Total';
$_['entry_order_status'] = 'Order Status';
$_['entry_geo_zone']	 = 'Geo Zone';
$_['entry_status']		 = 'Status';
$_['entry_sort_order']	 = 'Sort Order';
$_['entry_bank_image']   = 'Bank Image';
$_['entry_bank_name']    = 'Bank Name';
$_['entry_bank_code']    = 'Bank Code';
$_['entry_bank_sort']    = 'Bank Sort';

// Help
$_['help_total']		 = 'The checkout total the order must reach before this payment method becomes active.'; 

// Error 
$_['error_permission']   = 'Warning: You do not have permission to modify payment bank transfer!';
$_['error_bank']         = 'Bank Transfer Instructions Required!';
$_['error_bank_name']    = 'Bank Transfer Name Required!';
$_['error_bank_code']    = 'Bank Transfer Code Required!';