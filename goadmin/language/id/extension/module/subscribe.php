<?php
// Heading
$_['heading_title']    = 'Subscribe';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified subscribe module!';
$_['text_edit']        = 'Edit Subscribe Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify subscribe module!';