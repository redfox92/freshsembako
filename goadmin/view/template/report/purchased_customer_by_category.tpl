<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
      	<h3><a href="<?php echo $product_href; ?>" target="_blank"><?php echo $product_name; ?></a></h3>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-left"><?php echo $column_order_id; ?></td>
                <td class="text-left"><?php echo $column_invoice_no; ?></td>
                <td class="text-left"><?php echo $column_name; ?></td>
                <td class="text-right"><?php echo $column_quantity; ?></td>
                <td class="text-right"><?php echo $column_total; ?></td>
              </tr>
            </thead>
            <tbody>
              <?php if ($customers) { ?>
              <?php foreach ($customers as $customer) { ?>
              <tr>
                <td class="text-left"><a href="<?php echo $customer['href']; ?>" target="_blank">#<?php echo $customer['order_id']; ?></a></td>
                <td class="text-left"><?php echo $customer['invoice_no']; ?></td>
                <td class="text-left"><?php echo $customer['name']; ?></td>
                <td class="text-right"><?php echo $customer['quantity']; ?></td>
                <td class="text-right"><?php echo $customer['total']; ?></td>
              </tr>
              <?php } ?>
              <tr>
              	<td colspan="4" class="text-right"><?php echo $column_grandtotal; ?></td>
              	<td class="text-right"><?php echo $grand_total; ?></td>
              </tr>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
  		</div>
  	</div>
  </div>
</div>
<?php echo $footer; ?>