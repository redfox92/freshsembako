<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-bank-transfer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <!-- START Daftar bank -->
        <div class="col-md-2">
          <ul class="nav nav-pills nav-stacked bank-tablist" role="tablist">
            <li role="bank-transfer" class="in active"><a href="#bank-transfer-general" data-toggle="tab"><?php echo $text_general; ?></a></li>
            <?php $i = 0; ?>
            <?php if(isset($bank_transfer_list)) { ?>
            <?php foreach($bank_transfer_list as $bank_list => $bank_list) { ?>
            <?php $i++; ?>
            <li role="bank-transfer"><a href="#bank-transfer<?php echo $bank_list; ?>" data-toggle="tab">Bank <?php echo $bank_list; ?><i class="fa fa-minus li-fa-bank" onclick="removePane(this)"></i></a></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <ul class="nav nav-pills nav-stacked">
            <li role="bank-transfer"><a class="bank-transfer-add" href="#bank-transfer-add"><?php echo $text_new_bank; ?><i class="fa fa-plus li-fa-bank"></i></a></li>
          </ul>
        </div> <!-- END Daftar bank -->

        <!-- START Form -->
        <div class="col-md-10">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-bank-transfer" class="form-horizontal">
            <div class="tab-content bank-content">
              <!-- START General Settings Tab -->
              <div class="tab-pane fade in active" id="bank-transfer-general">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-bank-name"><?php echo $entry_bank_name; ?></label>
                  <div class="col-sm-10">
                    <input type="text" name="bank_transfer_name" value="<?php echo $bank_transfer_name; ?>" placeholder="<?php echo $entry_bank_name; ?>" id="input-sort-order" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-total"><span data-toggle="tooltip" title="<?php echo $help_total; ?>"><?php echo $entry_total; ?></span></label>
                  <div class="col-sm-10">
                    <input type="text" name="bank_transfer_total" value="<?php echo $bank_transfer_total; ?>" placeholder="<?php echo $entry_total; ?>" id="input-total" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                  <div class="col-sm-10">
                    <select name="bank_transfer_order_status_id" id="input-order-status" class="form-control">
                      <?php foreach ($order_statuses as $order_status) { ?>
                      <?php if ($order_status['order_status_id'] == $bank_transfer_order_status_id) { ?>
                      <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-geo-zone"><?php echo $entry_geo_zone; ?></label>
                  <div class="col-sm-10">
                    <select name="bank_transfer_geo_zone_id" id="input-geo-zone" class="form-control">
                      <option value="0"><?php echo $text_all_zones; ?></option>
                      <?php foreach ($geo_zones as $geo_zone) { ?>
                      <?php if ($geo_zone['geo_zone_id'] == $bank_transfer_geo_zone_id) { ?>
                      <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                  <div class="col-sm-10">
                    <select name="bank_transfer_status" id="input-status" class="form-control">
                      <?php if ($bank_transfer_status) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                  <div class="col-sm-10">
                    <input type="text" name="bank_transfer_sort_order" value="<?php echo $bank_transfer_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                  </div>
                </div>
              </div> <!-- END General Settings Tab -->

              <!-- START Foreach bank list -->
              <?php $i = 0; ?>
              <?php if(isset($bank_transfer_list)) { ?>
              <?php foreach($bank_transfer_list as $bank_list => $bank_list) { ?>
              <?php $i++; ?>
              <div class="bank-tab tab-pane fade" id="bank-transfer<?php echo $bank_list; ?>">
                <input type="hidden" name="bank_transfer_list[<?php echo $bank_list; ?>]" value="<?php echo $bank_list; ?>">

                <div class="form-group required">
                  <label class="col-sm-2 control-label"><?php echo $entry_bank; ?></label>
                  <div class="col-sm-10">
                    <ul class="nav nav-tabs" role="tablist">
                    <?php $i = 0; ?>
                    <?php foreach ($languages as $language) { ?>
                      <?php $i++; ?>
                      <li class="<?php if($i==1) echo ' in active' ?>"><a href="#lang-<?php echo $language['code']; ?>-<?php echo $bank_list; ?>" role="tab" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></a></li>
                    <?php } ?>
                    </ul>

                    <!-- START Lang content -->
                    <div class="tab-content">
                    <?php $i = 0; ?>
                    <?php foreach ($languages as $language) { ?>
                    <?php $i++; ?>
                      <div class="tab-pane fade<?php if($i==1) echo ' in active' ?>" id="lang-<?php echo $language['code']; ?>-<?php echo $bank_list; ?>">
                        <div class="input-group">
                          <!-- <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span> -->
                          <textarea name="bank_transfer_list_bank<?php echo $language['language_id']; ?>[<?php echo $bank_list; ?>]" cols="80" rows="10" placeholder="<?php echo $entry_bank; ?>" id="input-bank<?php echo $language['language_id']; ?>" class="form-control summernote"><?php echo isset(${'bank_transfer_list_bank' . $language['language_id']}[$bank_list]) ? ${'bank_transfer_list_bank' . $language['language_id']}[$bank_list] : ''; ?></textarea>
                        </div>
                        <?php if (${'error_bank_transfer_list_bank_' . $bank_list . '_' .  $language['language_id']}) { ?>
                        <div class="text-danger"><?php echo ${'error_bank_transfer_list_bank_' . $bank_list . '_' .  $language['language_id']}; ?></div>
                        <?php } ?>
                      </div>
                    <?php } ?>
                    </div> <!-- END Lang content -->
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_bank_image; ?></label>
                  <div class="col-sm-10">
                    <a href="" id="bank-image-<?php echo $bank_list; ?>" data-toggle="image" class="img-thumbnail">
                      <img src="<?php echo $bank_transfer_list_image_thumb[$bank_list]; ?>" alt="" title="" data-placeholder="" />
                    </a>

                    <input type="hidden" name="bank_transfer_list_image[<?php echo $bank_list; ?>]" value="<?php echo $bank_transfer_list_image[$bank_list]; ?>" id="input-image<?php echo $bank_list; ?>" />
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-bank-name"><?php echo $entry_bank_name; ?></label>
                  <div class="col-sm-10">
                    <input type="text" name="bank_transfer_list_name[<?php echo $bank_list; ?>]" value="<?php echo $bank_transfer_list_name[$bank_list]; ?>" placeholder="<?php echo $entry_bank_name; ?>" id="input-sort-order" class="form-control" />

                    <?php if(!empty(${'error_bank_transfer_list_name_' . $bank_list})) { ?>
                    <div class="text-danger"><?php echo ${'error_bank_transfer_list_name_' . $bank_list}; ?></div>
                    <?php } ?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-bank-code"><?php echo $entry_bank_code; ?></label>
                  <div class="col-sm-10">
                    <input type="text" name="bank_transfer_list_code[<?php echo $bank_list; ?>]" value="<?php echo $bank_transfer_list_code[$bank_list]; ?>" placeholder="<?php echo $entry_bank_code; ?>" id="input-sort-order" class="form-control" />

                    <?php if(!empty(${'error_bank_transfer_list_code_' . $bank_list})) { ?>
                    <div class="text-danger"><?php echo ${'error_bank_transfer_list_code_' . $bank_list}; ?></div>
                    <?php } ?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                  <div class="col-sm-10">
                    <select name="bank_transfer_list_status[<?php echo $bank_list; ?>]" id="input-status" class="form-control">
                      <?php if ($bank_transfer_list_status[$bank_list] == 1) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-bank-sort"><?php echo $entry_bank_sort; ?></label>
                  <div class="col-sm-10">
                    <input type="text" name="bank_transfer_list_sort[<?php echo $bank_list; ?>]" value="<?php echo $bank_transfer_list_sort[$bank_list]; ?>" placeholder="<?php echo $entry_bank_sort; ?>" id="input-sort-order" class="form-control" />
                  </div>
                </div>

              </div>
              <?php } ?> <!-- END Foreach bank list -->
              <?php } ?>
            </div>
          </form>
        </div><!-- END Form -->
  
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
<script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>
<script type="text/javascript">
  // summernote
  $('.summernote').summernote({
    height: 150,
    toolbar: [
      ['Misc', ['codeview']],
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['font', ['']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
    ]
  });
  $(document).ready(function() {

    // add new bank
    $(document).on('click', '.bank-transfer-add', function(event) {
      event.preventDefault();
      var bankTab = $('.bank-tab').length,
          tabNum = bankTab + 1;

      if ($('.bank-tab').length < 1) {
        bankTab = 1; 
      }

      // init html
      var tabList = '';
          tabList += '<li role="bank-transfer"><a id="b-link-'+ tabNum +'" href="#bank-transfer'+ tabNum +'" data-toggle="tab">Bank '+ tabNum +'<i class="fa fa-minus li-fa-bank" onclick="removePane(this)"></i></a></li>';

      var html = '';
          html += '<div class="bank-tab tab-pane fade" id="bank-transfer'+ tabNum +'">';
          html += '<input type="hidden" name="bank_transfer_list['+ tabNum +']" value="'+ tabNum +'">';
          html += '<div class="form-group required">';
          html += '<label class="col-sm-2 control-label"><?php echo $entry_bank; ?></label>';
          html += '<div class="col-sm-10">';
          html += '<ul class="nav nav-tabs" role="tablist">';
          html += '<?php $i = 0; ?>';
          <?php foreach ($languages as $language) { ?>
          <?php $i++; ?>
          html += '<li class="<?php if($i==1) echo ' in active' ?>"><a href="#lang-<?php echo $language["code"]; ?>-'+ tabNum +'" role="tab" data-toggle="tab"><img src="language/<?php echo $language["code"]; ?>/<?php echo $language["code"]; ?>.png" title="<?php echo $language["name"]; ?>" /></a></li>';
          <?php } ?>
          html += '</ul>';

          html += '<div class="tab-content">';
          <?php $i = 0; ?>
          <?php foreach ($languages as $language) { ?>
          <?php $i++; ?>
          html += '<div class="tab-pane fade<?php if($i==1) echo ' in active' ?>" id="lang-<?php echo $language["code"]; ?>-'+ tabNum +'">';
          html += '<div class="input-group">';
          // html += '<span class="input-group-addon"><img src="language/<?php echo $language["code"]; ?>/<?php echo $language["code"]; ?>.png" title="<?php echo $language["name"]; ?>" /></span>';
          html += '<textarea name="bank_transfer_list_bank<?php echo $language["language_id"]; ?>['+ tabNum +']" cols="80" rows="10" placeholder="<?php echo $entry_bank; ?>" id="input-bank<?php echo $language["language_id"]; ?>" class="form-control summernote"></textarea>';
          html += '</div>';
          html += '</div>';
          <?php } ?>
          html += '</div>';
          html += '</div>';
          html += '</div>';

          html += '<div class="form-group">';
          html += '<label class="col-sm-2 control-label"><?php echo $entry_bank_image; ?></label>';
          html += '<div class="col-sm-10">';
          html += '<a href="" id="bank-image-'+ tabNum +'" data-toggle="image" class="img-thumbnail">';
          html += '<img src="<?php echo $bank_transfer_list_image_default; ?>" alt="" title="" data-placeholder="" />';
          html += '</a>';
          html += '<input type="hidden" name="bank_transfer_list_image['+ tabNum +']" value="<?php echo $bank_transfer_list_image_default; ?>" id="input-image<?php echo (isset($bank_list)) ? $bank_list : 1; ?>" />';
          html += '</div>';
          html += '</div>';

          html += '<div class="form-group">';
          html += '<label class="col-sm-2 control-label" for="input-bank-name"><?php echo $entry_bank_name; ?></label>';
          html += '<div class="col-sm-10">';
          html += '<input type="text" name="bank_transfer_list_name['+ tabNum +']" value="" placeholder="<?php echo $entry_bank_name; ?>" id="input-sort-order" class="form-control" />';
          html += '</div>';
          html += '</div>';

          html += '<div class="form-group">';
          html += '<label class="col-sm-2 control-label" for="input-bank-code"><?php echo $entry_bank_code; ?></label>';
          html += '<div class="col-sm-10">';
          html += '<input type="text" name="bank_transfer_list_code['+ tabNum +']" value="" placeholder="<?php echo $entry_bank_code; ?>" id="input-sort-order" class="form-control" />';
          html += '</div>';
          html += '</div>';

          html += '<div class="form-group">';
          html += '<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>';
          html += '<div class="col-sm-10">';
          html += '<select name="bank_transfer_list_status['+ tabNum +']" id="input-status" class="form-control">';
          html += '<option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
          html += '<option value="0"><?php echo $text_disabled; ?></option>';
          html += '</select>';
          html += '</div>';
          html += '</div>';

          html += '<div class="form-group">';
          html += '<label class="col-sm-2 control-label" for="input-bank-sort"><?php echo $entry_bank_sort; ?></label>';
          html += '<div class="col-sm-10">';
          html += '<input type="text" name="bank_transfer_list_sort['+ tabNum +']" value="" placeholder="<?php echo $entry_bank_sort; ?>" id="input-sort-order" class="form-control" />';
          html += '</div>';
          html += '</div>';

          html += '</div>';

      // append html bank list and tab
      $('.bank-tablist').append(tabList);
      setTimeout(function() {
        $('#b-link-' + tabNum).trigger('click');
      }, 10);

      $('.bank-content').append(html);  
      $('.summernote').summernote({
        height: 150,
        toolbar: [
          ['Misc', ['codeview']],
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
        ]
      });
    });
  });

  // Remove pane
  function removePane(e) {
    // get target
    var target = e.parentElement.getAttribute('href'),
        cfm = confirm('Are You Sure?');
    if (cfm == true) {
      // remove pane
      $(target).remove();
      // remove list
      e.parentElement.remove();
      // change active list to general
      $('a[href="#bank-transfer-general"]').trigger('click');
    }
  }
</script>

<?php echo $footer; ?>