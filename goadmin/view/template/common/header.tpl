<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="icon" href="<?php echo HTTP_SERVER; ?>../image/catalog/favicon/FreshSembako-Favicon%2016x16-1.png" >
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>

<link href="view/stylesheet/bootstrap.css" type="text/css" rel="stylesheet" />

<?php if($logged){ ?>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<script src="view/javascript/jquery.tablesorter.min.js" type="text/javascript"></script>

<script src="view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<?php } ?>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />

<script src="view/javascript/jquery.jclock.js" type="text/javascript"></script>

<?php if($logged) { ?>
<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />
<link type="text/css" href="view/stylesheet/tablesorter-style.css" rel="stylesheet" media="screen" />
<?php } ?>

<?php foreach ($styles as $style) { ?>
<link type="text/css" href="<?php echo $style['href']; ?>" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="view/javascript/common.js" type="text/javascript"></script>
<?php if($logged) { ?>
<script src="view/javascript/autoNumeric.js" type="text/javascript"></script>
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>

<link href="view/stylesheet/gositus.css" type="text/css" rel="stylesheet" />

<?php if(isset($gohide) && $gohide == '') { ?>
<style type="text/css">
  .gohide {
    display: block !important;
  }
</style>
<?php } ?>
</head>
<body>
<div id="container">
<?php if ($logged) { ?>
<header id="header" class="navbar navbar-static-top">
  <div class="navbar-header">
    <?php if ($logged) { ?>
    <a type="button" id="button-menu" class="pull-left"><i class="fa fa-indent fa-lg"></i></a>
    <?php } ?>
    <a href="<?php echo $home; ?>" class="navbar-brand"><img src="view/image/logo.png" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" /></a></div>
  <?php if ($logged) { ?>
  <ul class="nav pull-right">
    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><?php if($alerts > 0) { ?><span class="label label-danger pull-left"><?php //echo $alerts; ?></span><?php } ?> <i class="fa fa-bell fa-lg"></i></a>
      <ul class="dropdown-menu dropdown-menu-right alerts-dropdown" style="min-width: 235px;">
        <li class="dropdown-header"><?php echo $text_order; ?></li>
        <?php foreach ($order_statuses as $order_status) { ?>
        <li><a href="<?php echo $order_status['link']; ?>" style="display: block; overflow: auto;"><span class="label <?php echo $order_status['color']; ?> pull-right"><?php echo $order_status['total']; ?></span><?php echo $order_status['name']; ?></a></li>
        <?php } ?>
        <li class="divider <?php echo $gohide ?>"></li>
        <li class="dropdown-header <?php echo $gohide ?>"><?php echo $text_customer; ?></li>
        <li class="<?php echo $gohide ?>"><a href="<?php echo $online; ?>"><span class="label label-success pull-right"><?php echo $online_total; ?></span><?php echo $text_online; ?></a></li>
        <li class="<?php echo $gohide ?>"><a href="<?php echo $customer_approval; ?>"><span class="label label-danger pull-right"><?php echo $customer_total; ?></span><?php echo $text_approval; ?></a></li>
        <li class="divider"></li>
        <li class="dropdown-header"><?php echo $text_product; ?></li>
        <li><a href="<?php echo $product; ?>"><span class="label label-danger pull-right"><?php echo $product_total; ?></span><?php echo $text_stock; ?></a></li>
        <li class="<?php echo $gohide ?>"><a href="<?php echo $review; ?>"><span class="label label-danger pull-right"><?php echo $review_total; ?></span><?php echo $text_review; ?></a></li>
        <li class="divider <?php echo $gohide ?>"></li>
        <li class="dropdown-header <?php echo $gohide ?>"><?php echo $text_affiliate; ?></li>
        <li class="<?php echo $gohide ?>"><a href="<?php echo $affiliate_approval; ?>"><span class="label label-danger pull-right"><?php echo $affiliate_total; ?></span><?php echo $text_approval; ?></a></li>
      </ul>
    </li>
    <li class="dropdown"><a href="<?php echo $store_href; ?>" target="_blank"><i class="fa fa-home fa-lg"></i></a>
      <ul class="dropdown-menu dropdown-menu-right">
        <li class="dropdown-header"><?php echo $text_store; ?></li>
        <?php foreach ($stores as $store) { ?>
        <li><a href="<?php echo $store['href']; ?>" target="_blank"><?php echo $store['name']; ?></a></li>
        <?php } ?>
      </ul>
    </li>
    <li class="dropdown gohide"><a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-life-ring fa-lg"></i></a>
      <ul class="dropdown-menu dropdown-menu-right">
        <li class="dropdown-header"><?php echo $text_help; ?></li>
        <li><a href="http://www.gositus.com" target="_blank"><?php echo $text_homepage; ?></a></li>
        <li><a href="http://www.gositus.com/contact" target="_blank"><?php echo $text_contact; ?></a></li>
      </ul>
    </li>
    <li><a href="<?php echo $logout; ?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_logout; ?></span> <i class="fa fa-sign-out fa-lg"></i></a></li>
  </ul>
  <?php } ?>
</header>
<?php } ?>