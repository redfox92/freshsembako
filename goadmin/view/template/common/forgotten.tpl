<?php echo $header; ?>

<?php if ($error_warning) { ?>
        <div id="login_error" class="notif danger absolute">
            <p><?php echo $error_warning; ?></p>
            <div class="close-notif">
                <a href="javascript:;">
                    <i class="fa fa-close"></i>
                </a>
            </div>
        </div>
<?php } ?>

<div id="home-new">     
    <div class="square-green"></div>
    <div class="line-grey"></div>
    <div class="triangle-yellow"></div>
    <div class="circle-blue"></div>
    <div class="wrap-bg-text vertical">         
        <div class="bg-text"><h1>login</h1></div>
    </div>

    <div class="wrap-form-home">
        <div class="container">
            <div class="row inline">
                <div class="col-lg-7 col-md-4 col-sm-12">
                    <div id="home-logo">
                        <img src="view/image/goadmin/gositus-logo.png" alt="Gositus">
                    </div>
                </div>
                <div class="col-lg-5 col-md-8 col-sm-12">
                    
                    <div id="wrap-login">
                        <h1>
                            <span><?php echo $heading_title; ?></span>
                        </h1>

                        <div class="clock">
                            <label id="date"></label>&nbsp;&nbsp;//&nbsp;&nbsp;<label id="jclock"></label>
                        </div>

                        <div>
                            <div id="login-container">
                                <form id="login" method="post" action="<?php echo $action; ?>" enctype="multipart/form-data">
                                    <p class="<?php echo ($email)? 'filled-text' : ''?>">
                                        <label for="input-email"><?php echo $entry_email; ?></label>
                                        <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control input-text required input_field" autofocus/>
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p>
                                                <button type="submit" class="btn-custom btn-scale btn-grey"><?php echo $button_reset; ?></button>
                                            </p>
                                        </div>
                                        <div class="col-md-6">
                                            <p>
                                                <a href="<?php echo $cancel; ?>" class="btn-custom btn-scale btn-grey">Cancel</a>
                                            </p>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<footer class="home">
    <div class="orbit">
        <div class="orbit-large"></div>
        <div class="orbit-medium"></div>
        <div class="orbit-small"></div>
        <div class="moon"></div>
    </div>
    <div class="wave">
        <div class="wave-grey"></div>   
        <div class="wave-grey-2"></div> 
    </div>
</footer>

<script>
    $(document).ready(function(){
        
        $('#jclock').jclock({
            format: '%I:%M:%S %P'
        });
        
        $('label#date').jclock({
            format: '%A, %d %B %Y '
        });

        $(document).on('click', '.close-notif', function(){
            $(this).parents('.notif').fadeOut('normal');
        });  
    })
</script>

<?php /*
<div id="content">
    <div class="container-fluid"><br />
        <br />
        <div class="row">
            <div class="col-sm-offset-4 col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title"><i class="fa fa-repeat"></i> <?php echo $heading_title; ?></h1>
                    </div>
                    <div class="panel-body">
                        <?php if ($error_warning) { ?>
                        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>
                        <?php } ?>
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="input-email"><?php echo $entry_email; ?></label>
                                <div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> <?php echo $button_reset; ?></button>
                            <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
*/?>
<?php //echo $footer; ?>