<?php echo $header; ?>

<?php if ($error_warning) { ?>
        <div id="login_error" class="notif danger absolute">
            <p><?php echo $error_warning; ?></p>
            <div class="close-notif">
                <a href="javascript:;">
                    <i class="fa fa-close"></i>
                </a>
            </div>
        </div>
<?php } ?>

<?php if ($success) { ?>
 <div id="login_error" class="notif success absolute">
    <p><?php echo $success; ?></p>
    <div class="close-notif">
        <a href="javascript:;">
            <i class="fa fa-close"></i>
        </a>
    </div>
</div>
<?php } ?>

<div id="home-new">     
    <div class="square-green"></div>
    <div class="line-grey"></div>
    <div class="triangle-yellow"></div>
    <div class="circle-blue"></div>
    <div class="wrap-bg-text vertical">         
        <div class="bg-text"><h1>login</h1></div>
    </div>

    <div class="wrap-form-home">
        <div class="container">
            <div class="row inline">
                <div class="col-lg-7 col-md-4 col-sm-5 col-xs-12">
                    <div id="home-logo">
                        <img src="view/image/goadmin/gositus-logo.png" alt="Gositus">
                    </div>
                </div>
                <div class="col-lg-5 col-md-8 col-sm-7 col-xs-12">
                    
                    <div id="wrap-login">
                        <h1 class="title-login">
                            <span>Welcome To Gositus</span>
                        </h1>

                        <h1 class="title-forgot hide">
                            <span>FORGOT YOUR PASSWORD?</span>
                        </h1>

                        <div class="clock">
                            <label id="date"></label>&nbsp;&nbsp;//&nbsp;&nbsp;<label id="jclock"></label>
                        </div>

                        <div>
                            <div id="login-container">
                                <form id="login" method="post" action="<?php echo $action; ?>" enctype="multipart/form-data">
                                    <p class="<?php echo ($username)? 'filled-text' : ''?>">
                                        <label for="input-username"><?php echo $entry_username; ?></label>
                                        <input type="text" name="username" value="<?php echo $username; ?>" id="input-username" class="form-control input-text required input_field" autofocus/>
                                    </p>
                                    <p class="<?php echo ($password)? 'filled-text' : ''?>">
                                        <label for="input-password"><?php echo $entry_password; ?></label>
                                        <input type="password" name="password" value="<?php echo $password; ?>" id="input-password" class="form-control input-text required input_field" />
                                        <span class="eyes">
                                            <a href="javascript:;"><i class="fa fa-eye-slash"></i></a>
                                        </span>
                                    </p>
                                    <?php if ($forgotten) { ?>
                                        <div class="wrap-forgot">
                                            <p>                                        
                                                <span><a href="javascript:;" class="btn-forgot"><?php echo $text_forgotten; ?></a></span>
                                            </p>
                                        </div>
                                    <?php } ?>
                                    
                                    <?php if ($redirect) { ?>
                                    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                                    <?php } ?>
                                    
                                    <p>
                                        <button type="submit" class="btn-custom btn-scale btn-grey"><?php echo $button_login; ?></button>
                                    </p>
                                </form>

                                <!-- FORGOT -->
                                <form id="forgot" class="hide" method="post" action="<?php echo $action; ?>" enctype="multipart/form-data">
                                    <p class="<?php echo ($email)? 'filled-text' : ''?>">
                                        <label for="input-email">Email</label>
                                        <input type="text" name="email" value="" id="input-email" class="form-control input-text required input_field" autofocus/>
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p>
                                                <button type="submit" class="btn-custom btn-scale btn-grey">Reset</button>
                                            </p>
                                        </div>
                                        <div class="col-md-6">
                                            <p>
                                                <a href="javascript:;" class="btn-custom btn-scale btn-grey btn-cancel-forgot">Cancel</a>
                                            </p>
                                        </div>
                                    </div>
                                </form>
                                <!-- FORGOT -->


                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<footer class="home">
    <div class="orbit">
        <div class="orbit-large"></div>
        <div class="orbit-medium"></div>
        <div class="orbit-small"></div>
        <div class="moon"></div>
    </div>
    <div class="wave">
        <div class="wave-grey"></div>   
        <div class="wave-grey-2"></div> 
    </div>
</footer>


<?php /*
<div id="content">
    <div class="container-fluid"><br />
        <br />
        <div class="row">
            <div class="col-sm-offset-4 col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title"><i class="fa fa-lock"></i> <?php echo $text_login; ?></h1>
                    </div>
                    <div class="panel-body">
                        <?php if ($success) { ?>
                        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>
                        <?php } ?>
                        <?php if ($error_warning) { ?>
                        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>
                        <?php } ?>
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="input-username"><?php echo $entry_username; ?></label>
                                <div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" name="username" value="<?php echo $username; ?>" placeholder="<?php echo $entry_username; ?>" id="input-username" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-password"><?php echo $entry_password; ?></label>
                                <div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                                </div>
                                <?php if ($forgotten) { ?>
                                <span class="help-block"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></span>
                                <?php } ?>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-key"></i> <?php echo $button_login; ?></button>
                            </div>
                            <?php if ($redirect) { ?>
                            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
*/?>

<?php if(isset($error_email)) { ?>
<script type="text/javascript">
    $(window).load(function() {
        $('.btn-forgot').trigger('click');
    });
</script>
<?php } ?>

<?php //echo $footer; ?>