<?php
class ModelUserActivity extends Model {
	public function addActivity($key, $data) {

		$user_id = 0;
		if ($data['user_id'] > 0) {
			$user_id = $data['user_id'];
		}

		$this->db->query("INSERT INTO `" . DB_PREFIX . "user_activity` SET `user_id` = '" . (int)$user_id . "', `key` = '" . $this->db->escape($key) . "', `data` = '" . $this->db->escape(json_encode($data)) . "', `ip` = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', `date_added` = NOW()");
	}

	public function getActivities($key) {
		$sql = "
		SELECT
			ua.*,
			`u`.`username`
		FROM
			`user_activity` ua
		JOIN `user` u ON `u`.`user_id` =  `ua`.`user_id`
		WHERE
			`ua`.`key` LIKE '%".$key."%'
		";
		
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getActivitiesData($name, $value) {
		$sql = "
		SELECT
			*
		FROM
			user_activity
		WHERE
			`data` LIKE '%\"" . $name . "\":\"" . $value . "\"}%'
		OR `data` LIKE '%\"" . $name . "\":" . $name . "}%'
		ORDER BY user_activity_id DESC
		";

		$query = $this->db->query($sql);
		return $query;
	}
}