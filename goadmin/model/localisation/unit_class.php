<?php
class ModelLocalisationUnitClass extends Model {
	public function addUnitClass($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "unit_class SET unit_name = '" . $this->db->escape($data['unit_name']) . "'");

		$this->cache->delete('unit_class');
		
		return $unit_class_id;
	}

	public function editUnitClass($unit_class_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "unit_class SET unit_name = '" . $this->db->escape($data['unit_name']) . "' WHERE unit_class_id = '" . (int)$unit_class_id . "'");

		$this->cache->delete('unit_class');
	}

	public function deleteUnitClass($unit_class_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "unit_class WHERE unit_class_id = '" . (int)$unit_class_id . "'");

		$this->cache->delete('unit_class');
	}

	public function getUnitClasses($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "unit_class";

			$sort_data = array(
				'unit_class_id',
				'unit_name'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY unit_name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			// $unit_class_data = $this->cache->get('unit_class.' . (int)$this->config->get('config_language_id'));

			// if (!$unit_class_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "unit_class");

				$unit_class_data = $query->rows;

			// 	$this->cache->set('unit_class.' . (int)$this->config->get('config_language_id'), $unit_class_data);
			// }

			return $unit_class_data;
		}
	}

	public function getUnitClass($unit_class_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "unit_class WHERE unit_class_id = '" . (int)$unit_class_id . "'");

		return $query->row;
	}

	public function getTotalUnitClasses() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "unit_class");

		return $query->row['total'];
	}
}