<?php
class ModelMarketingSubscribe extends Model {

	public function deleteSubscribe($subscribe_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "subscribe WHERE id = '" . (int)$subscribe_id . "'");
	}

	public function getSubscribe($subscribe_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "subscribe WHERE id = '" . (int)$subscribe_id . "'");

		return $query->row;
	}

	public function getSubscribes($data = array())
	{
		$sql = "SELECT * FROM `" . DB_PREFIX . "subscribe`";

		$sort_data = array(
			'email',
			'email',
			'date_added',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY email";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalSubscribes() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "subscribe";

		$sort_data = array(
			'email',
			'date_added',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY email";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}