<?php
class ModelReportCategory extends Model {


	public function getPurchased($data=array())
	{
		$sql = "SELECT p2c.category_id, cd.name as category_name, op.product_id, op.name as product_name, CONCAT(oo.value,' ', oo.name) as `option`, sum(op.quantity) as sold, SUM(op.total) AS total ";
		$sql .= "FROM `order_product` op ";
		$sql .= "LEFT JOIN `order_option` oo ON op.order_product_id = oo.order_product_id ";
		$sql .= "LEFT JOIN `order` o ON op.order_id = o.order_id ";
		$sql .= "LEFT JOIN `product_to_category` p2c ON op.product_id = p2c.product_id ";
		$sql .= "LEFT JOIN `category` c ON p2c.category_id = c.category_id ";
		$sql .= "LEFT JOIN `category_description` cd ON p2c.category_id = cd.category_id ";

		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		$sql .= " GROUP BY oo.product_option_value_id ";
		$sql .= "ORDER BY c.sort_order, op.product_id ASC";

		$query = $this->db->query($sql);
// pre($sql);
		return $query->rows;
	}
	// public function getPurchased($data=array())
	// {
	// 	$sql = "SELECT p2c.category_id, 
	// 		(SELECT name FROM `category_description` WHERE category_id = p2c.category_id) AS category_name, 
	// 		sum(op.quantity) AS quantity,
	// 		sum(op.price) AS total 
	// 	FROM `order_product` op
	// 	LEFT JOIN `order` o ON (op.order_id = o.order_id) 
	// 	LEFT JOIN `product_to_category` p2c ON (op.product_id = p2c.product_id)";

	// 	if (!empty($data['filter_order_status_id'])) {
	// 		$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
	// 	} else {
	// 		$sql .= " WHERE o.order_status_id > '0'";
	// 	}

	// 	if (!empty($data['filter_date_start'])) {
	// 		$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
	// 	}

	// 	if (!empty($data['filter_date_end'])) {
	// 		$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
	// 	}

	// 	$sql .= " GROUP BY p2c.category_id ORDER BY total DESC";

	// 	if (isset($data['start']) || isset($data['limit'])) {
	// 		if ($data['start'] < 0) {
	// 			$data['start'] = 0;
	// 		}

	// 		if ($data['limit'] < 1) {
	// 			$data['limit'] = 20;
	// 		}

	// 		$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
	// 	}

	// 	$query = $this->db->query($sql);

	// 	return $query->rows;
	// }

	public function getTotalPurchased($data = array())
	{
		$sql = "SELECT COUNT(DISTINCT(p2c.category_id)) as total 
		FROM `order_product` op
		LEFT JOIN `order` o ON (op.order_id = o.order_id) 
		LEFT JOIN `product_to_category` p2c ON (op.product_id = p2c.product_id)";

		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getPurchasedCustomerByCategory($category_id)
	{
		$sql = "SELECT o.order_id, o.invoice_no, o.firstname, o.lastname, op.quantity, op.price FROM `order` o RIGHT JOIN `order_product` op ON o.`order_id` = op.`order_id` LEFT JOIN `product_to_category` p2c ON op.`product_id` = p2c.`product_id`";

		$sql .= "WHERE p2c.`category_id` = " . (int)$category_id;

		return $this->db->query($sql)->rows;
	}
}