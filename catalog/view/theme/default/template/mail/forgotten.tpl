<?php echo $header; ?>
		<div style="line-height:22px; font-size: 12px; margin: 0 0 10px;">
			<?php echo $text_greeting; ?>
		</div>
		<div style="line-height:22px; font-size: 12px; margin: 0 0 10px;">
			<?php echo $text_change; ?> 
			<a href="<?php echo $link_reset; ?>" style="font-weight: bold;font-style: italic; text-decoration: none; color: #53d800">
	      <?php echo $text_clickhere; ?>
	    </a>
		</div>
		<div style="line-height:22px; font-size: 12px; margin: 0 0 10px;">
			<?php echo $text_ip; ?>
		</div>
<?php echo $footer; ?>