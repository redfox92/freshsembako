<?php echo $header; ?>
		<div style="line-height:22px; font-size: 12px; margin: 0 0 10px;">
			<?php echo $text_welcome; ?>
		</div>
		<div style="line-height:22px; font-size: 12px; margin: 0 0 10px;">
			<?php echo $text_approval; ?> 
			<a href="<?php echo $link_login; ?>" style="font-weight: bold;font-style: italic; text-decoration: none; color: #53d800">
	      <?php echo $text_clickhere; ?>
	    </a>
		</div>
		<div style="line-height:22px; font-size: 12px; margin: 0 0 10px;">
			<?php echo $text_services; ?>
		</div>
		<div style="line-height:22px; font-size: 12px; margin: 0 0 10px;">
			<?php echo $text_thanks; ?>
			<br>
			<?php echo $config_name; ?>
		</div>
<?php echo $footer; ?>