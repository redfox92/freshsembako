<?php echo $header; ?>
		<div style="line-height:22px; font-size: 12px; margin: 0 0 10px;">
			<?php echo $text_update_order; ?>
		</div>
		<div style="line-height:22px; font-size: 12px; margin: 0 0 10px;">
			<?php echo $text_update_date_added; ?>
		</div>
		<?php if (isset($text_update_order_status)) { ?>
		<div style="line-height:22px; font-size: 12px; margin: 0 0 10px;">
			<?php echo $text_update_order_status; ?> 
			<span style="font-weight: bold;font-style: italic; text-decoration: none; color: #fe6122">
	      <?php echo $order_status_name; ?>
	    </span>
		</div>
		<?php } ?>
		<?php if (isset($text_update_link)) { ?>
		<div style="line-height:22px; font-size: 12px; margin: 0 0 10px;">
			<?php echo $text_update_link; ?> 
			<a href="<?php echo $link_order_info; ?>" style="font-weight: bold;font-style: italic; text-decoration: none; color: #53d800">
	      <?php echo $text_clickhere; ?>
	    </a>
		</div>
		<?php } ?>
		<?php if (isset($text_update_comment)) { ?>
		<div style="line-height:22px; font-size: 12px; margin: 0 0 10px;">
			<?php echo $text_update_comment; ?> <br>
			<span style="font-weight: bold;font-style: italic; text-decoration: none; color: #53d800">
	      <?php echo $comment; ?>
	    </span>
		</div>
		<?php } ?>
		<div style="line-height:22px; font-size: 12px; margin: 0 0 10px;">
			<?php echo $text_update_footer; ?>
		</div>
<?php echo $footer; ?>