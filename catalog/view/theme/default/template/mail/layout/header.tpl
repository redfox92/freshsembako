<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />

  <title><?php echo $title; ?></title>
</head>

<body>
  <div bgcolor="#FFFFFF" style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;width:100%!important;height:100%;font-size:14px;color:#000000;margin:0;padding:0">
    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;">
      <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0 0 20px;" bgcolor="transparent">
        <tr>
          <td style="text-align: center; padding: 15px 0; border-bottom: 3px solid #ffd446">
            <a href="<?php echo $url; ?>"><img src="<?php echo $logo; ?>"
                style="height: 64px; width:auto;" /></a>
          </td>
        </tr>
      </table>
    </div>
    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;">