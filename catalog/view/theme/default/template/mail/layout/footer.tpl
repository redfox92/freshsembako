    </div>
    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;">
      <div style="background-color: #FFFAE2; padding: 10px 10px; text-align: center; display: block; width: 100%;box-sizing: border-box;line-height: 0;">
        <?php if (!empty($facebook)) { ?>
          <a href="<?php echo $facebook; ?>" target="_blank" style="padding: 0 5px;"><img src="<?php echo $img_facebook;?>" style="height: 15px;"></a>
        <?php } ?>
        <?php if (!empty($instagram)) { ?>
          <a href="<?php echo $instagram; ?>" target="_blank" style="padding: 0 5px;"><img src="<?php echo $img_instagram;?>" style="height: 15px;"></a>
        <?php } ?>
        <?php if (!empty($line)) { ?>
          <a href="<?php echo $line; ?>" target="_blank" style="padding: 0 5px;"><img src="<?php echo $img_line;?>" style="height: 15px;"></a>
        <?php } ?>
        <?php if (!empty($youtube)) { ?>
          <a href="<?php echo $youtube; ?>" target="_blank" style="padding: 0 5px;"><img src="<?php echo $img_youtube;?>" style="height: 15px;"></a>
        <?php } ?>
      </div>
    </div>
  </div>
</body>

</html>