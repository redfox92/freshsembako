<?php echo $header; ?>
<table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0"
  bgcolor="transparent">
  <tr>
    <td style="margin:0;padding:0"></td>
    <td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0">
      <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;">
        <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0 0 20px;" bgcolor="transparent">
          <tr>
            <td style="padding: 15px 0 10px; line-height:22px; font-size: 12px">
              <?php echo $text_greeting; ?>
            </td>
          </tr>
          <?php if ($customer_id) { ?>
          <tr>
            <td style="line-height:22px; font-size: 12px"><?php echo $text_link; ?> 
              <a href="<?php echo $link ?>" style="font-weight: bold;font-style: italic; text-decoration: none; color: #fe6122">
                <?php echo $text_clickhere; ?>
              </a>
            </td>
          </tr>
          <?php } ?>
          <?php if ($link_confirm) { ?>
          <tr>
            <td style="line-height:22px; font-size: 12px"><?php echo $text_link_confirm; ?> 
              <a href="<?php echo $link_confirm; ?>" style="font-weight: bold;font-style: italic; text-decoration: none; color: #53d800">
                <?php echo $text_clickhere; ?>
              </a>
            </td>
          </tr>
          <?php } ?>
        </table>
        <table style="margin-bottom:30px; width: 100%;border-spacing:0;width:100%;background-color:transparent; border-collapse: collapse">
          <thead style="border-bottom: 1px solid #53d800">
            <tr>
              <td colspan="2" style="font-size: 13px; padding: 8px 0 ; font-weight: bold; color:#fe6122; text-transform: uppercase; border-bottom: 1px solid #53d800">
                <?php echo $text_order_id; ?> #<?php echo $invoice_no; ?>
              </td>
            </tr>
          </thead>
          <tbody style="border-bottom: 1px solid #53d800">
            <tr>
              <td style="width: 50%; padding: 4px 8px 4px 0; vertical-align: top">
                <div style="display:block; width: 100%; padding: 6px 0;">
                  <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;" bgcolor="transparent" ">
                    <tr>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 42%; padding: 2px
                    0; vertical-align: top; "><?php echo $text_date_added; ?></td>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 5%; padding: 2px
                    0; vertical-align: top; ">:</td>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 53%; padding: 2px
                    0; vertical-align: top; "><?php echo $date_added; ?></td>
                    </tr>
                    <tr>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 42%; padding: 2px
                    0; vertical-align: top; "><?php echo $text_payment_method; ?></td>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 5%; padding: 2px
                    0; vertical-align: top; ">:</td>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 53%; padding: 2px
                    0; vertical-align: top; "><?php echo $payment_method; ?></td>
                    </tr>
          <?php if ($shipping_method) { ?>
                    <tr>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 42%; padding: 2px
                    0; vertical-align: top; "><?php echo $text_shipping_method; ?></td>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 5%; padding: 2px
                    0; vertical-align: top; ">:</td>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 53%; padding: 2px
                    0; vertical-align: top; "><?php echo $shipping_method; ?></td>
                    </tr>
          <?php } ?>
                  </table>
                </div>
              </td>
              <td style="width: 50%; padding: 4px 0 4px 8px; border-left: 1px solid #53d800; vertical-align: top ">
                  <div style="display:block; width: 100%; padding: 6px 0; ">
                    <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;
                    " bgcolor="transparent ">
                    <tr>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 42%; padding: 2px
                    0; vertical-align: top; "><?php echo $text_email; ?></td>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 5%; padding: 2px
                    0; vertical-align: top; ">:</td>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 53%; padding: 2px
                    0; vertical-align: top; "><a href="mailto:<?php echo $email; ?>" style="color: black; text-decoration: none "><?php echo $email; ?></a></td>
                    </tr>
                    <tr>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 42%; padding: 2px
                    0; vertical-align: top; "><?php echo $text_telephone; ?></td>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 5%; padding: 2px
                    0; vertical-align: top; ">:</td>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 53%; padding: 2px
                    0; vertical-align: top; "><a href="tel:<?php echo $telephone; ?>" style="color: black; text-decoration: none "><?php echo $telephone; ?></a></td>
                    </tr>
                    <tr>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 42%; padding: 2px
                    0; vertical-align: top; "><?php echo $text_ip; ?></td>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 5%; padding: 2px
                    0; vertical-align: top; ">:</td>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 53%; padding: 2px
                    0; vertical-align: top; "><?php echo $ip; ?></td>
                    </tr>
                    <tr>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 42%; padding: 2px
                    0; vertical-align: top; "><?php echo $text_order_status; ?></td>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 5%; padding: 2px
                    0; vertical-align: top; ">:</td>
                      <td style="word-break: break-word; font-size: 12px; line-height: 22px; width: 53%; padding: 2px
                    0; vertical-align: top; font-style: italic; font-weight: bold "><?php echo $order_status; ?></td>
                    </tr>
                  </table>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
        <table style="margin-bottom: 30px; width: 100%;border-spacing:0;width:100%;background-color:transparent; border-collapse:
                    collapse ">
          <thead style="border-bottom: 1px solid #53d800 ">
              <tr>
                <td style="font-size: 13px; width:50%; padding: 8px 8px 8px 0 ; color:#fe6122; font-weight: bold;text-transform:
                    uppercase; "><?php echo $text_instruction; ?>
                </td>
                <td style="font-size: 13px; width:50%; padding: 8px 0 8px 8px ; color:#fe6122; font-weight: bold;text-transform:
                      uppercase; "><?php echo $text_shipping_address; ?>
                </td>
              </tr>
          </thead>
          <tbody style="border-bottom: 1px solid #53d800 ">
            <tr>
              <td style="padding: 0; vertical-align: top ">
                <div style="display:block; width: 100%; padding: 10px 0 ">
  <?php if ($comment) { ?>
                  <?php echo $comment; ?>
  <?php } ?>
                  <!-- <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;
                    " bgcolor="transparent ">
                    <tr>
                      <td colspan="2 " style="font-size: 12px; line-height: 22px; width: 100%; vertical-align: top; ">Bank
                        transfer instruction</td>
                    </tr>
                    <tr>
                      <td style="line-height: 0; padding: 8px 0; width: 60px; vertical-align: middle ">
                        <img src="https://upload.wikimedia.org/wikipedia/id/thumb/e/e0/BCA_logo.svg/1280px-BCA_logo.svg.png
                    " style="max-height: 80%; max-width: 70% ">
                      </td>
                      <td style="line-height: 22px; padding: 4px 0; vertical-align: middle; font-weight: bold; ">
                        <div>BANK BCA</div>
                        <div style="font-size: 12px "><span style="color: #939393
                    ">PT. Go Online Solusi - </span> <span style="font-weight: bold ">453 890
                          0099</span>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2 " style="font-size: 12px; line-height: 22px; width: 100%; vertical-align: top; ">Your
                        order will not ship until we receive payment.</td>
                    </tr>
                  </table> -->
                </div>
              </td>
        <?php if ($shipping_address) { ?>
              <td style="width: 50%; padding: 0 0 0 8px; border-left: 1px solid #53d800; vertical-align: top ">
                <div style="padding: 10px 0 ">
                  <?php echo $shipping_address; ?>
              </td>
        <?php } ?>
            </tr>
          </tbody>
        </table>

        <!-- <table style="margin-bottom: 30px; width: 100%;border-spacing:0;width:100%;background-color:transparent;border-collapse:
                    collapse ">
          <thead style="border-bottom: 1px solid #53d800 ">
            <tr>
              <td style="font-size: 13px; width:50%; padding: 8px 8px 8px 0 ; color:#fe6122; font-weight: bold;text-transform:
                    uppercase; "><?php //echo $text_payment_address; ?>
              </td>
              <td style="font-size: 13px; width:50%; padding: 8px 0 8px 8px ; color:#fe6122; font-weight: bold;text-transform:
                    uppercase; "><?php //echo $text_shipping_address; ?>
              </td>
            </tr>
          </thead>
          <tbody style="border-bottom: 1px solid #53d800; ">
            <tr>
              <td style="width: 50%; padding: 0 8px 0 0; vertical-align: top ">
                <div style="padding: 10px 0 ">
                  <?php //echo $payment_address; ?>
                </div>
              </td>
        <?php //if ($shipping_address) { ?>
              <td style="width: 50%; padding: 0 0 0 8px; border-left: 1px solid #53d800; vertical-align: top ">
                <div style="padding: 10px 0 ">
                  <?php //echo $shipping_address; ?>
              </td>
        <?php //} ?>
            </tr>
          </tbody>
        </table> -->
        <table style="margin-bottom: 30px; width: 100%;border-spacing:0;width:100%;background-color:transparent; border-collapse:
                    collapse; ">
          <thead style="border-bottom: 1px solid #53d800 ">
            <tr>
              <td style="width:40%; padding: 8px 5px 8px 0 ; font-weight: bold; color:#fe6122; text-transform: uppercase ">
                <?php echo $text_product; ?>
              </td>
              <td style="width:10%; padding: 8px 0 8px 5px ; font-weight: bold;color:#fe6122; text-transform: uppercase ">
                <?php echo $text_quantity; ?>
              </td>
              <td style="text-align:right; width:20%; padding: 8px 5px 8px 0 ; font-weight: bold;color:#fe6122; text-transform: uppercase; ">
                <?php echo $text_price; ?>
              </td>
              <td style="width:30%; padding: 8px 0 8px 5px ; font-weight: bold;color:#fe6122; text-transform: uppercase;text-align: right ">
                <?php echo $text_total; ?>
              </td>
            </tr>
          </thead>
          <tbody style="border-bottom: 1px solid #53d800 ">
            <!-- <tr><td colspan="4 " style="padding: 5px 0 "></td></tr> -->
            <!-- ini cuma buat spasi aja, di taro di paling awal + akhir tbody  -->
            <tr><td colspan="4 " style="padding: 5px 0 "></td></tr>
      <?php foreach ($products as $product) { ?>
            <tr>
              <td style="width:40%; padding: 0 5px 0 0 ; vertical-align: top ">
                <div style="display:block; width: 100%; ">
                  <table>
                    <tr>
                      <td style="padding-right: 10px;vertical-align: top; text-align: center ">
                        <img src="<?php echo $product['thumb']; ?>" style="max-width: 60px; max-height: 45px ">
                      </td>
                      <td style="vertical-align: top ">
                        <div style="margin-bottom: 5px ">
                          <a href="<?php echo $product['href']; ?>" style="color: black; text-decoration: none; font-size:12px; line-height: 22px; "><?php echo $product['name']; ?></a>
                        </div>
                        <div style="color: #666666; font-size: 12px; ">
                          <?php foreach ($product['option'] as $option) { ?>
                          <?php echo $option['value']; ?> <?php echo $option['name']; ?>
                          <?php } ?>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
              </td>
              <td style="width:15%; padding: 8px 0 8px 5px ; vertical-align: top;font-size: 12px; ">
                <?php echo $product['quantity']; ?>
              </td>
              <td style="text-align:right; width:20%; padding: 8px 5px 8px 0 ; vertical-align: top ;font-size: 12px; ">
                <?php echo $product['price']; ?>
              </td>
              <td style="width:25%; padding: 8px 0 8px 5px ; vertical-align: top; text-align: right;font-size: 12px; ">
                <?php echo $product['total']; ?>
              </td>
            </tr>
      <?php } ?>
            <tr><td colspan="4 " style="padding: 5px 0 "></td></tr>
          </tbody>
          <tfoot>
            <!-- untuk tfoot -->
            <!-- row terakhir = color, padding sama font-sizenya beda + ada border-topnya di 2 td terakhir -->
            <!-- <tr><td colspan="4 " style="padding: 5px 0 "></td></tr> di taro di awal + sblm 1 row terakhir -->
            <tr><td colspan="4 " style="padding: 5px 0 "></td></tr>
      <?php foreach ($totals as $total) { ?>
            <?php if ($total['code'] == 'total') { ?>
            <tr><td colspan="4 " style="padding: 5px 0 "></td></tr>
            <?php } ?>
            <tr>
              <?php if ($total['code'] != 'total') { ?>
              <td colspan="3" style="text-align:right; width:20%; padding: 0 5px 0 0 ; vertical-align: top; line-height: 22px;font-size: 12px;">
                <?php echo $total['title']; ?>
              </td>
              <td style="width:25%; padding: 0 0 0 5px ; vertical-align: top; line-height: 22px;font-size: 12px; text-align: right;">
                <?php echo $total['text']; ?>
              </td>
              <?php } else { ?>
              <td colspan="3" style="text-align:right; width:20%; padding: 10px 5px 0 0 ;text-transform: uppercase; font-size: 14px; border-top:
                    1px solid #53d800; color:#fe6122; font-weight: bold; "><?php echo $total['title']; ?>
              </td>
              <td style="width:25%; padding: 10px 0 0 5px ;color: #53d800; border-top: 1px solid #53d800; text-align:
                    right; font-size: 14px; font-weight: bold; "><?php echo $total['text']; ?>
              </td>
              <?php } ?>
            </tr>
      <?php } ?>
            </tfoot>
        </table>
      </div>
    </td>
    <td style="margin:0;padding:0 "></td>
  </tr>
</table>
<?php echo $footer; ?>