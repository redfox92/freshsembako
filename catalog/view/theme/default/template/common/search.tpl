

<!-- SEARCH -->

<div class="search-box">

  <form class="search-form" method="get">

    <input type="hidden" name="route" value="product/search">

    <input type="text" class="search-input search-list" name="search" placeholder="<?php echo $text_search; ?>" value="" autocomplete="off">

    <div class="search-result"></div>

    <button type="button" class="search-btn expand-btn">

      <i class="fa fa-search"></i>

    </button>

    <button type="submit" class="search-btn action-btn">

      <i class="fa fa-search"></i>

    </button>

  </form>

</div>

<!-- END OF SEARCH -->