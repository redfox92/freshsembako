<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">

<head>
<!-- Gositus | www.Gositus.com | www.facebook.com/Gositus | http://twitter.com/Gositus -->
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta property="og:image" content="<?php echo isset($og_image) ? $og_image : $logo ?>"/>
<base href="<?php echo HTTP_SERVER; ?>" />

<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-3.3.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<link href="catalog/view/theme/default/stylesheet/style.min.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/style.custom.css" rel="stylesheet">

<?php if(! empty($favicon)){ ?>
  <?php foreach ($favicon as $ico) { ?>
    <link rel="<?php echo $ico['rel']; ?>" href="<?php echo $ico['src']; ?>" <?php if(! empty($ico['sizes'])) echo "sizes=\"{$ico['sizes']}\"" ?>>
  <?php } ?>
<?php } ?>

<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<script src="catalog/view/javascript/main.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
 <!-- HEADER -->
  <div class="container">
    <header>
      <div class="header-brand">
        <a href="<?php echo $home; ?>">
          <img src="<?php echo $logo; ?>">
        </a>
      </div>
      <div class="header-menu hidden-xs">
        <div class="header-list">
          <ul class="hl-left">
            <li>
              <a href="<?php echo $home; ?>" class="btn-line <?php echo $route == 'common/home' ? 'active' : ''; ?>"><?php echo $text_home; ?></a>
            </li>
            <li>
              <a href="<?php echo $category; ?>" class="btn-line <?php echo ($route == 'product/category' && $category_id != 10) ? 'active' : ''; ?>"><?php echo $text_shop; ?></a>
            </li>
            <?php if (!empty($grosir)) { ?>
            <li>
              <a class="btn-line <?php echo ($route == 'product/category' && $category_id == 10) ? 'active' : ''; ?>" href="<?php echo $grosir; ?>"><?php echo $text_grosir; ?></a>
            </li>
            <?php } ?>
            <li>
              <?php echo $search; ?>
            </li>
          </ul>
          <ul class="hl-right text-right">

            <?php if ($logged) { ?>
            <li class="account-li">
                <a href="javascript:;" class="hvr-green link-account"><i class="fa fa-user-circle-o"></i> <?php echo $text_logged; ?></a>
                <div class="account-content">
                  <div><a class="hvr-green" href="<?php echo $account; ?>"><?php echo $text_account; ?></a></div>
                  <div><a class="hvr-green" href="<?php echo $address; ?>"><?php echo $text_address; ?></a></div>
                  <div><a class="hvr-green" href="<?php echo $order; ?>"><?php echo $text_order; ?></a></div>
                  <div><a class="hvr-green" href="<?php echo $payment_confirmation; ?>"><?php echo $text_payment_confirmation; ?></a></div>
                  <div><a class="hvr-green" href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></div>
                  <div><a class="hvr-green" href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></div>
                </div>
            </li>
            <?php } else { ?>

            <li>
              <a href="javascript:;" class="btn-slide" data-toggle="modal" data-target="#register-modal"><?php echo $text_register; ?></a>
            </li>
            <li>
              <a href="javascript:;" class="btn-line green" data-toggle="modal" data-target="#login-modal"><?php echo $text_login; ?></a>
            </li>
            <?php } ?>
            <li>

              <!-- CART -->

              <div class="cart">
                <a href="javascript:;" class="cart-menu">
                  <img src="image/assets/home/basket.png">
                  <!-- cart dot di munculin kalo cartnya ada isi aja, jd biar user notice klo di cartnya ada isi -->
                  <!-- <div class="cart-dot"></div> -->
                </a>
              </div>

              <!-- END OF CART -->
            </li>
        <?php if (!empty($reward_points)) { ?>
            <li>
              <a href="<?php echo $reward ?>"><div class="menu-point">Poin: <span class="bold"><?php echo $reward_points; ?></span></div></a>
            </li>
        <?php } ?>
          </ul>
        </div>
      </div>
    </header>
  </div>
  
  <div class="wrap-cart">
  <?php echo $cart; ?>
  </div>

  <div class="mobile-menu visible-xs">
    <div class="container">
      <div class="mobile-content">
        <div class="mobile-toggle">
          <div class="mt-icon">
            <div class="toggle-line"></div>
            <div class="toggle-line"></div>
            <div class="toggle-line"></div>
          </div>
          <div class="mt-text"><?php echo $text_menu; ?></div>
        </div>
        <div class="mobile-feature">
          <div class="mobile-search">
            <i class="fa fa-search"></i>
          </div>
          <div class="mobile-cart">
            <!-- CART -->

            <div class="cart">
              <a href="javascript:;" class="cart-menu">
                <img src="image/assets/home/basket.png">
                <!-- cart dot di munculin kalo cartnya ada isi aja, jd biar user notice klo di cartnya ada isi -->
                <!-- <div class="cart-dot"></div> -->
              </a>
            </div>

            <!-- END OF CART -->
          </div>
        </div>
      </div>

      <?php echo $search; ?>
    </div>
    <div class="mobile-menu-x">
      <div class="mobile-menu-overlay"></div>
      <div class="mobile-menu-content">
        <div class="mobile-menu-title">
          <div class="mmt-title"><?php echo $text_menu; ?></div>
          <div class="mmt-close">
            <a href="#" class="mobile-menu-close">
                <img src="image/assets/home/cancel.png">
            </a>
          </div>
        </div>
        <div class="mobile-menu-list">
          <ul>
            <li><a class="btn-line <?php echo $route == 'common/home' ? 'active' : ''; ?>" href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
            <li><a class="btn-line <?php echo ($route == 'product/category' && $category_id != 10) ? 'active' : ''; ?>" href="<?php echo $category; ?>"><?php echo $text_shop; ?></a></li>
          </ul>
          <ul class="mml-bottom">
            <?php if ($logged) { ?>
            <li><a class="btn-line link-account" href="javascript:;"><i class="fa fa-user-circle-o"></i> <?php echo $text_logged; ?></a></li>
            <li><a class="btn-line" href="<?php echo $account; ?>" ><?php echo $text_account; ?></a></li>
            <li><a class="btn-line" href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
            <li><a class="btn-line" href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a class="btn-line" href="<?php echo $payment_confirmation; ?>"><?php echo $text_payment_confirmation; ?></a></li>
            <li><a class="btn-line" href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
            <li><a class="btn-line" href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a class="btn-slide" href="javascript:;" data-toggle="modal" data-target="#register-modal"><?php echo $text_register_modal; ?></a></li>
            <li><a class="text-green hvr-orange" href="javascript:;" data-toggle="modal" data-target="#login-modal"><?php echo $text_customer_area; ?></a></li>
            <?php } ?>
            <?php if (!empty($grosir)) { ?>
            <li><a class="btn-line <?php echo ($route == 'product/category' && $category_id == 10) ? 'active' : ''; ?>" href="<?php echo $grosir; ?>"><?php echo $text_grosir; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php if (!empty($reward_points)) { ?>
        <a href="<?php echo $reward; ?>"><div class="menu-point"><span class="bold"><?php echo $reward_points; ?></span> Poin</div></a>
        <?php } ?>
        <div class="mobile-info">
          <!-- <div class="mi-box">
            <div class="mi-title"><?php echo $text_contact; ?></div>
            <div class="mi-list">
              <a href="tel:<?php echo $telephone; ?>" class="hvr-orange"><i class="fa fa-phone"></i> <?php echo $telephone; ?></a>
              <a href="mailto:<?php echo $email; ?>" class="hvr-orange"><i class="fa fa-envelope"></i><?php echo $email; ?></a>
            </div>
          </div> -->
          <div class="mi-box">
            <div class="mi-title"><?php echo $text_follow; ?></div>
            <div class="mi-list social-media">
                <?php if ($facebook) { ?>
              <a target="_blank" href="<?php echo $facebook; ?>" class="hvr-orange"><i class="fa fa-facebook"></i></a>
                <?php } ?>
                <?php if ($instagram) { ?>
              <a target="_blank" href="<?php echo $instagram; ?>" class="hvr-orange"><i class="fa fa-instagram"></i></a>
                <?php } ?>
                <?php if ($youtube) { ?>
              <a target="_blank" href="<?php echo $youtube; ?>" class="hvr-orange"><i class="fa fa-youtube"></i></a>
                <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function() {
      
  $('.mobile-toggle').on('click', function () {
    $('.mobile-menu-x').addClass('active');
    $('body').addClass('lock');
  });

  $('.mobile-menu-close, .mobile-menu-overlay').on('click', function () {
    $('.mobile-menu-x').removeClass('active');
    $('body').removeClass('lock');
  });

  $('.mobile-search').on('click', function () {
    $('.mobile-menu .search-box').toggleClass('active');
    $('.search-search').empty();
    $('.search-input').val('');
  });
    });
  </script>
  <!-- login/regis/forgot -->

  <div id="login-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <button type="button" class="modal-close" data-dismiss="modal">
          <img src="image/assets/home/cancel.png">
        </button>
        <div class="modal-body">
          <div class="modal-title"><?php echo $text_customer_area; ?></div>
          <div>
            <form id="form-login" action="<?php echo $login; ?>" method="post">
              <input type="hidden" name="mode" value="popup">
              <div class="field-box wmargin">
                <div class="input-box">
                  <div class="input-label"><?php echo $entry_email; ?></div>
                  <input class="input-field-line" type="text" name="email">
                </div>
              </div>
              <div class="field-box wmargin">
                <div class="input-box">
                  <div class="input-label"><?php echo $entry_password; ?></div>
                  <input class="input-field-line" type="password" name="password">
                </div>
              </div>
              <div class="field-box wmargin lp04">
                <?php echo $text_forgot_password; ?>
              </div>
              <div class="field-box wmargin text-center">
                <button class="btn-slide green w100px" type="submit"><?php echo $button_login; ?></button>
              </div>
              <div class="field-box wmargin text-center lp04">
                <?php echo $text_account_register_modal; ?>
              </div>
            </form>
            <script type="text/javascript">
              $(document).ready(function() {
                $('#form-login').submit(function( event ) {
                  $('#form-login .alert.alert-danger').remove();
                  $.ajax({
                      url: $(event.target).attr('action'),
                      type: $(event.target).attr('method'),
                      dataType: 'json',
                      data: $(event.target).serialize(),
                      success: function(response) {
                          if (response.error_warning) {
                              $('#form-login').prepend('<div class="alert alert-danger">' + response.error_warning + '</div>');
                          }

                          if (response.redirect) {
                              location = response.redirect;
                          }
                      }
                  });
                  event.preventDefault();
                });
              });
            </script>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="register-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <button type="button" class="modal-close" data-dismiss="modal">
          <img src="image/assets/home/cancel.png">
        </button>
        <div class="modal-body">
          <div class="modal-title"><?php echo $text_register_modal; ?></div>
          <div>
            <form id="form-register" action="<?php echo $register ?>" method="post">
              <input type="hidden" name="mode" value="popup">
              <input type="hidden" name="company" value="">
              <input type="hidden" name="fax" value="">
              <div class="field-box wmargin">
                <div class="input-box">
                  <div class="input-label"><?php echo $entry_fullname; ?></div>
                  <input class="input-field-line" type="text" name="fullname" autocomplete="off">
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="field-box wmargin mb20">
                    <div class="input-box">
                      <div class="input-label"><?php echo $entry_email; ?></div>
                      <input class="input-field-line" type="text" name="email" autocomplete="off">
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="field-box wmargin mb20">
                    <div class="input-box">
                      <div class="input-label"><?php echo $entry_telephone; ?></div>
                      <input class="input-field-line" type="text" name="telephone" autocomplete="off">
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="field-box wmargin mb20">
                    <div class="input-box">
                      <div class="input-label"><?php echo $entry_password; ?></div>
                      <input class="input-field-line" type="password" name="password" autocomplete="off">
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="field-box wmargin mb20">
                    <div class="input-box">
                      <div class="input-label"><?php echo $entry_confirm; ?></div>
                      <input class="input-field-line" type="password" name="confirm" autocomplete="off">
                    </div>
                  </div>
                </div>
              </div>
              <!-- <div class="field-box wmargin">
                <div class="check-box">
                  <label class="check-label lp04" for="receive-promo">Receive daily deals and promo via email
                    <input type="checkbox" class="check-field" value="promo" id="receive-promo">
                    <span class="check-mark"></span>
                  </label>
                </div>
              </div> -->
              <div class="field-box wmargin text-center">
                <button class="btn-slide green"><?php echo $text_register; ?></button>
              </div>
              <div class="field-box wmargin text-center lp04">
                <?php echo $text_account_already_modal; ?>
              </div>
            </form>
            <script type="text/javascript">
              $(document).ready(function() {
                $('#form-register').submit(function( event ) {
                    $('#form-register .alert.alert-danger, #form-register .text-danger').remove();
                    $.ajax({
                        url: $(event.target).attr('action'),
                        type: $(event.target).attr('method'),
                        dataType: 'json',
                        data: $(event.target).serialize(),
                        success: function(response) {
                            if (response.error_fullname) {
                                $('#form-register input[name=fullname]').parents('.field-box').append('<div class="text-danger">' + response.error_fullname + '</div>');
                            }
                            if (response.error_email) {
                                $('#form-register input[name=email]').parents('.field-box').append('<div class="text-danger">' + response.error_email + '</div>');
                            }
                            if (response.error_telephone) {
                                $('#form-register input[name=telephone]').parents('.field-box').append('<div class="text-danger">' + response.error_telephone + '</div>');
                            }
                            if (response.error_password) {
                                $('#form-register input[name=password]').parents('.field-box').append('<div class="text-danger">' + response.error_password + '</div>');
                            }
                            if (response.error_confirm) {
                                $('#form-register input[name=confirm]').parents('.field-box').append('<div class="text-danger">' + response.error_confirm + '</div>');
                            }
                            if (response.error_warning) {
                                $('#form-register').prepend('<div class="alert alert-danger">' + response.error_warning + '</div>');
                            }

                            if (response.redirect) {
                                location = response.redirect;
                            }
                        }
                    });
                    event.preventDefault();
                });
              });
            </script>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="forgot-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <button type="button" class="modal-close" data-dismiss="modal">
          <img src="image/assets/home/cancel.png">
        </button>
        <div class="modal-body">
          <div class="modal-title"><?php echo $text_forgotten; ?></div>
          <div>
            <form id="form-forgotten" action="<?php echo $forgotten; ?>" method="post">
              <input type="hidden" name="mode" value="popup">
              <div class="field-box wmargin">
                <div class="input-box">
                  <div class="input-label"><?php echo $entry_email; ?></div>
                  <input class="input-field-line" type="text" name="email" autocomplete="off">
                </div>
              </div>
              <div class="field-box wmargin text-center">
                <button class="btn-slide green w100px"><?php echo $button_submit; ?></button>
              </div>
            </form>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#form-forgotten').submit(function( event ) {
                        $('#form-forgotten .alert.alert-danger,#form-forgotten .alert.alert-success , #form-forgotten .text-danger').remove();
                        $.ajax({
                            url: $(event.target).attr('action'),
                            type: $(event.target).attr('method'),
                            dataType: 'json',
                            data: $(event.target).serialize(),
                            success: function(response) {
                                if (response.error_warning) {
                                    $('#form-forgotten').prepend('<div class="alert alert-danger">' + response.error_warning + '</div>');
                                }

                                if (response.success) {
                                    $('#form-forgotten').prepend('<div class="alert alert-success">' + response.success + '</div>');
                                    $('#form-forgotten').find('input[name=email]').val('');
                                }
                            }
                        });
                        event.preventDefault();
                    });
                });
            </script>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="notes-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <button type="button" class="modal-close" data-dismiss="modal">
          <img src="image/assets/home/cancel.png">
        </button>
        <div class="modal-body">
          <div class="modal-title">Informasi</div>
          <div class="notes-content text-center">
            Pesanan yang dibayarkan <b>sebelum</b> pukul 16.00 WIB akan diproses pada <b>H+1.</b><br>
            Pesanan yang dibayarkan <b>lebih dari</b> pukul 16.00 WIB akan diproses pada <b>H+2.</b>

            <div class="wrap confirm" style="margin-top: 20px;">
              <a href="javascript:;" class="btn-slide" data-dismiss="modal" style="float: left;">Batal</a>
              <a href="javascript:;" class="btn-slide btn-next" style="float: right;">Lanjut</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END OF HEADER -->