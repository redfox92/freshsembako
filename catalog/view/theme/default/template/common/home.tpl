<?php echo $header; ?>

<?php echo $content_top; ?>
<div class="container">
  <div class="main-value">
    <div class="mv-title">Getting your groceries online is now easy with Fresh Sembako</div>
    <div class="mv-list">
      <div class="mv-item">
        <div class="mvi-img">
          <img src="image/assets/home/value-1.png">
        </div>
        <div class="mvi-name">Trusted</div>
      </div>
      <div class="mv-item">
        <div class="mvi-img">
          <img src="image/assets/home/value-2.png">
        </div>
        <div class="mvi-name">All in One</div>
      </div>
      <div class="mv-item">
        <div class="mvi-img">
          <img src="image/assets/home/value-3.png">
        </div>
        <div class="mvi-name">Fast Delivery</div>
      </div>
      <div class="mv-item">
        <div class="mvi-img">
          <img src="image/assets/home/value-4.png">
        </div>
        <div class="mvi-name">Daily Deals</div>
      </div>
    </div>
  </div>
</div>


<div id="welcome-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="modal-close" data-dismiss="modal">
        <img src="image/assets/home/cancel.png">
      </button>
      <div class="modal-body">
        <div class="modal-title">Informasi</div>
        <div class="notes-content text-center">
          Dapatkan berbagai keuntungan dengan menjadi Pelanggan di Fresh Sembako.<br><br>
          Untuk setiap transaksi yang berhasil, Anda berhak mendapatkan point yang dapat digunakan untuk transaksi berikutnya.<br>
          (1 point = Rp. 1)
          <div class="wrap confirm" style="margin-top: 20px;">
            <a href="javascript:;" class="btn-slide" data-dismiss="modal">Close</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    if (!checkCookie('welcome')) {
      <?php if (!$logged) { ?>
      $('#welcome-modal').modal('show');
      <?php } ?>
    }
  });
</script>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>