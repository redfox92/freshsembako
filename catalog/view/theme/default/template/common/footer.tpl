<footer>
  <div class="top-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-2 col-xs-6">
          <div class="footer-box">
            <div class="fb-title"><?php echo $text_quick_links; ?></div>
            <div class="fb-list">
              <ul>
                <li>
                  <a href="<?php echo $home; ?>" class="btn-line <?php echo $route == 'common/home' ? 'active' : ''; ?>"><?php echo $text_home; ?></a>
                </li>
                <li>
                  <a href="<?php echo $category; ?>" class="btn-line <?php echo ($route == 'product/category' && $category_id != 10) ? 'active' : ''; ?>"><?php echo $text_shop; ?></a>
                </li>
                <li>
                  <a href="<?php echo $manufacturer; ?>" class="btn-line <?php echo $route == 'product/manufacturer' ? 'active' : ''; ?>"><?php echo $text_manufacturer; ?></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
          <div class="footer-box">
            <div class="fb-title"><?php echo $text_information; ?></div>
            <div class="fb-list">
              <ul>
                <?php foreach ($informations as $information) { ?>
                <li><a href="<?php echo $information['href']; ?>" class="btn-line <?php echo ($route == 'information/information' && $information['id'] == $information_id) ? 'active' : ''; ?>"><?php echo $information['title']; ?></a></li>
                <?php } ?>
                <!-- <li>
                  <a href="#" class="btn-line">How to Order</a>
                </li>
                <li>
                  <a href="#" class="btn-line">Shipping</a>
                </li>
                <li>
                  <a href="#" class="btn-line">Terms and Conditions</a>
                </li>
                <li>
                  <a href="#" class="btn-line">Payment</a>
                </li>
                <li>
                  <a href="#" class="btn-line">FAQ</a>
                </li> -->
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <div class="footer-box">
            <div class="fb-title"><?php echo $text_contact; ?></div>
            <div class="fb-list">
              <ul>
                <li>
                  <a href="tel:<?php echo $telephone; ?>" class="hvr-orange">
                    <i class="fa fa-phone"></i><?php echo $telephone; ?></a>
                </li>
                <li>
                  <a href="mailto:<?php echo $email; ?>" class="hvr-orange">
                    <i class="fa fa-envelope"></i><?php echo $email; ?></a>
                </li>
              </ul>
            </div>
          </div>
          <div class="footer-box">
            <div class="fb-title"><?php echo $text_follow; ?></div>
            <div class="fb-list">
              <ul>
                <?php if ($facebook) { ?>
                <li class="social-li">
                  <a target="_blank" href="<?php echo $facebook; ?>" class="fb-icon-link">
                    <i class="fa fa-facebook"></i>
                  </a>
                </li>
                <?php } ?>
                <?php if ($instagram) { ?>
                <li class="social-li">
                  <a target="_blank" href="<?php echo $instagram; ?>" class="fb-icon-link">
                    <i class="fa fa-instagram"></i>
                  </a>
                </li>
                <?php } ?>
                <?php if ($youtube) { ?>
                <li class="social-li">
                  <a target="_blank" href="<?php echo $youtube  ; ?>" class="fb-icon-link">
                    <i class="fa fa-youtube "></i>
                  </a>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
          <div class="tfr-right">
            <?php if ($payment_images) { ?>
            <div class="footer-box">
              <div class="fb-title"><?php echo $text_payment_method; ?></div>
              <div class="fb-list">
                <ul>
                  <?php foreach ($payment_images as $image) { ?>
                  <li class="img-li">
                    <img src="<?php echo $image['image']; ?>" title="<?php echo $image['title']; ?>" alt="<?php echo $image['title']; ?>">
                  </li>
                  <?php } ?>
                </ul>
              </div>
            </div>
            <?php } ?>
            <?php if ($shipping_images) { ?>
            <div class="footer-box">
              <div class="fb-title"><?php echo $text_shipping_method; ?></div>
              <div class="fb-list">
                <ul>
                  <?php foreach ($shipping_images as $image) { ?>
                  <li class="img-li">
                    <img src="<?php echo $image['image']; ?>">
                  </li>
                  <?php } ?>
                </ul>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="bottom-footer">
    <div class="container">
      <div class="bf-item"><?php echo $powered; ?></div>
      <div class="bf-item">
        <a id="copyright-gositus" target="_blank" href="http://www.gositus.com" title="Jasa Pembuatan Website" style="color: #fff;margin-right: 0;font-size: 9px;top: 0;">
          <span>Gositus</span>
        </a>
      </div>
    </div>
  </div>
</footer>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129802339-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-129802339-1');
</script>

</body></html>