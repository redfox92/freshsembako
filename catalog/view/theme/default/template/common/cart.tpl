<!-- part of cart -->
<div class="cart-dropdown" id="cart">
  <div class="cart-dropdown-overlay"></div>
  <div class="cart-dropdown-content">
    <a href="javascript:;" class="cart-close hvr-orange"><?php echo $button_close; ?></a>
    <div>
      <div class="cart-title"><?php echo $text_cart; ?>
        <sup>
          <div class="ct-qty">
            <span><?php echo count($products); ?></span>
          </div>
        </sup>
      </div>
      <div class="cart-list">
      <?php foreach ($products as $product) { ?>
        <div class="cart-box cart-id-<?php echo $product['cart_id']; ?>">
          <div class="cb-img">
            <a href="<?php echo $product['href']; ?>" class="cb-img-link">
              <img src="<?php echo $product['thumb']; ?>">
            </a>
          </div>
          <div class="cb-info">
            <div class="cb-name">
              <a href="<?php echo $product['href']; ?>" class="hvr-orange"><?php echo $product['name']; ?></a>
            </div>
            <div class="cb-option">
              <?php if ($product['option']) { ?>
              <?php foreach ($product['option'] as $option) { ?>
              <?php echo $option['value']; ?> <?php echo $option['name']; ?>
              <?php } ?>
              <?php } ?>
            </div>
            <div class="cb-price"><?php echo $product['price']; ?></div>
          </div>
          <div class="cb-detail">
            <div class="cb-qty">
              <div class="num-stepper">
                <div class="ns-step ns-step-down <?php echo $product['ns_down']; ?>" data-cart_id="<?php echo $product['cart_id']; ?>">
                  <i class="fa fa-minus"></i>
                </div>
                <div class="ns-value"><?php echo $product['quantity']; ?></div>
                <div class="ns-step ns-step-up <?php echo $product['ns_up']; ?>" data-cart_id="<?php echo $product['cart_id']; ?>">
                  <i class="fa fa-plus"></i>
                </div>
                <input type="hidden" class="ns-qty" value="<?php echo $product['quantity']; ?>" min="<?php echo $product['minimum']; ?>" max="<?php echo $product['maximum']; ?>">
              </div>
            </div>
            <div class="cb-totalprice"><?php echo $product['total']; ?></div>
          </div>
        </div>
      <?php } ?>
      </div>
      <!-- cart-arrow dimnculin kalo itemnya lebih dari 4 -->
      <!-- <div class="cart-arrow">
        <i class="fa fa-angle-double-down"></i>
      </div> -->
      <?php foreach ($totals as $total) { ?>
        <?php if ($total['code'] == 'sub_total') { ?>
          <div class="cart-total">  
            <div class="ct-text"><?php echo $total['title']; ?></div>
            <div class="ct-total"><?php echo $total['text']; ?></div>
          </div>
        <?php } ?>
      <?php } ?>
      <div class="cart-desc">
        <?php echo $text_cart_desc; ?>
      </div>
      <div class="cart-button">
        <a href="<?php echo $home; ?>" class="btn-continue btn-slide btn-cart"><?php echo $button_shopping; ?></a>
        <a href="<?php echo $cart; ?>" class="btn-slide green btn-checkout"><?php echo $text_cart; ?></a>
      </div>
    </div>
  </div>
</div>
<!-- end of part of cart -->

<script type="text/javascript">
function open_cart() {
  $('body').addClass('lock').css('padding-right', getScrollbarWidth()+'px');
  $('.cart-dropdown').addClass('active');
}

function close_cart() {
  $('body').removeClass('lock').css('padding-right', '0');
  $('.cart-dropdown').removeClass('active');
}

function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = "scroll";

    // add innerdiv
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);        

    var widthWithScroll = inner.offsetWidth;

    // remove divs
    outer.parentNode.removeChild(outer);

    return widthNoScroll - widthWithScroll;
}

var count_products = <?php echo count($products); ?>;

$(document).ready(function() {

  // $('.btn-checkout').on('click', function(event) {
  //   event.preventDefault();
  //   $('#notes-modal').find('.notes-content').find('.btn-next').attr('href', $(this).attr('href'));
  //   $('#notes-modal').modal('show');
  // });

  if (count_products > 0 && $('.cart .cart-menu .cart-dot').length == 0) {
    $('.cart .cart-menu').each(function(index, el) {
      $(this).append('<div class="cart-dot"></div>');
    });
  }

  // Cart
  $('.cart-menu').on('click', function () {
    open_cart();
  });

  $('.cart-close, .cart-dropdown-overlay, .btn-continue').on('click', function () {
    close_cart();
  });

  $('#cart .ns-step-down').on('click', function(){
    if(!$(this).hasClass('disabled')){
      var min = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('min'));
      var max = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('max'));

      var value = parseInt($(this).parents('.num-stepper').find('.ns-value').text()) - 1;

      $(this).parents('.num-stepper').find('.ns-step').removeClass('disabled')

      if (value == max) {
        $(this).parents('.num-stepper').find('.ns-step-up').addClass('disabled');
      }
      if (value == min) {
        $(this).parents('.num-stepper').find('.ns-step-down').addClass('disabled');
      }

      $(this).parents('.num-stepper').find('.ns-value').text(value);
      $(this).parents('.num-stepper').find('.ns-qty').val(value);

      var cart_id = $(this).attr('data-cart_id');
      var new_value  = $(this).siblings('.ns-qty').val();
      
      if (new_value == 0) {
        cart.remove(cart_id);
      } else {
        cart.update(cart_id, new_value);
      }
    }
  });

  $('#cart .ns-step-up').on('click', function(){
    if(!$(this).hasClass('disabled')){
      var min = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('min'));
      var max = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('max'));

      var value = parseInt($(this).parents('.num-stepper').find('.ns-value').text()) + 1;

      $(this).parents('.num-stepper').find('.ns-step').removeClass('disabled')

      if (value == max) {
        $(this).parents('.num-stepper').find('.ns-step-up').addClass('disabled');
      }
      if (value == min) {
        $(this).parents('.num-stepper').find('.ns-step-down').addClass('disabled');
      }

      $(this).parents('.num-stepper').find('.ns-value').text(value);
      $(this).parents('.num-stepper').find('.ns-qty').val(value);

      var cart_id = $(this).attr('data-cart_id');
      var new_value  = $(this).siblings('.ns-qty').val();

      cart.update(cart_id, new_value);
    }
  });
});
</script>