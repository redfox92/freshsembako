<?php echo $header; ?>
<div class="account-page">
  <div class="container">
    <div class="account-content">
    	<div class="field-container">
      	<div class="color-title"><?php echo $heading_title; ?></div>
      	<div class="field-desc mb20"><?php echo $text_error; ?></div>
      	<div class="field-box wmargin text-right">
          <a href="<?php echo $continue; ?>" class="btn-slide green" type="button"><?php echo $button_continue; ?></a>
        </div>
      </div>
    </div>
  </div>
</div>    	
<?php echo $footer; ?>