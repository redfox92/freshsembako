<?php echo $header; ?>
<!-- ACCOUNT PAGE -->
<div class="account-page">
	<div class="container">
	  <div class="row">
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <?php echo $column_left; ?>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <div class="account-content">
            <div class="field-container">
              <div class="table-box">
                <div class="table-head">
                  <div class="table-col col-return-id"><?php echo $column_return_id; ?></div>
                  <div class="table-col col-date hidden-xs"><?php echo $column_date_added; ?></div>
                  <div class="table-col col-return-status"><?php echo $column_status; ?></div>
                  <div class="table-col col-return-order-id"><?php echo $column_order_id; ?></div>
                  <div class="table-col col-return-action">Action</div>
                </div>
                <div class="table-body">
						      <?php if ($returns) { ?>
			            <?php foreach ($returns as $return) { ?>
                  <div class="table-row">
                    <div class="table-col col-return-id">#<?php echo $return['return_id']; ?></div>
                    <div class="table-col col-date hidden-xs"><?php echo $return['date_added']; ?></div>
                    <div class="table-col col-return-status">
                      <div class="visible-xs mb5"><?php echo $return['date_added']; ?></div>
                      <?php echo $return['status']; ?>
                    </div>
                    <div class="table-col col-return-order-id"><?php echo $return['order_id']; ?></div>
                    <div class="table-col col-return-action">
                      <a href="<?php echo $return['href']; ?>" class="btn-slide green"><i class="fa fa-eye"></i></a>
                    </div>
                  </div>
			            <?php } ?>
						      <?php } else { ?>
    							<div class="table-row">
                    <div class="table-col col-empty"><?php echo $text_empty; ?></div>
                  </div>
						      <?php } ?>

                </div>
              </div>
            </div>
            <div class="pagination-box text-center">
              <?php if ($pagination): ?>
              	<?php echo $pagination; ?>
              <?php endif ?>
            </div>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>