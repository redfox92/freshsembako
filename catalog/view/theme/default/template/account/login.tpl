<?php echo $header; ?>
<!-- ACCOUNT PAGE -->

<div class="account-page">
  <div class="container">
    <div class="account-content">
      <div class="color-title"><span class="orange">Login</span></div>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
      <?php } ?>
      <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
      <?php } ?>
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="field-container mb20">
            <div class="field-title"><?php echo $text_new_customer; ?></div>
            <div class="field-desc mb20"><?php echo $text_register_account; ?></div>
            <div class="field-box wmargin"><a class="btn-slide" href="<?php echo $register; ?>"><?php echo $button_continue; ?></a></div>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="field-container">
            <div class="field-title"><?php echo $text_returning_customer; ?></div>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
              <div class="field-box wmargin">
                <div class="input-box <?php echo $email ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $entry_email; ?></div>
                  <input class="input-field-line" type="text" name="email" value="<?php echo $email; ?>">
                </div>
              </div>
              <div class="field-box wmargin">
                <div class="input-box <?php echo $password ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $entry_password; ?></div>
                  <input class="input-field-line" type="password" name="password" value="<?php echo $password; ?>">
                </div>
              </div>
              <div class="field-box wmargin">
                <?php echo $text_forgotten; ?>
                <a class="text-green hvr-orange" href="<?php echo $forgotten; ?>" data-toggle="modal" data-target="#forgot-modal">Click
                  here
                </a>
              </div>
              <div class="field-box wmargin">
                <button class="btn-slide green" type="submit"><?php echo $button_login; ?></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>