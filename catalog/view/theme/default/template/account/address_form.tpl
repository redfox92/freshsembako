<?php echo $header; ?>
<!-- ACCOUNT PAGE -->
<div class="account-page">
	<div class="container">
	  <div class="row">
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <?php echo $column_left; ?>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <div class="account-content">
	        <div class="field-container">
	          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	            <div class="field-box wmargin">
	              <div class="input-box <?php echo !empty($fullname) ? 'filled' : ''; ?>">
	                <div class="input-label"><?php echo $entry_fullname; ?></div>
	                <input class="input-field-line" type="text" name="fullname" value="<?php echo $fullname; ?>">
	              </div>
	              <?php if ($error_fullname) { ?>
	              <div class="text-danger"><?php echo $error_fullname; ?></div>
	              <?php } ?>
	            </div>
	            <div class="field-box wmargin">
	              <div class="input-box <?php echo !empty($address_1) ? 'filled' : ''; ?>">
	                <div class="input-label"><?php echo $entry_address_1; ?></div>
	                <input class="input-field-line" type="text" name="address_1" value="<?php echo $address_1; ?>">
	              </div>
	              <?php if ($error_address_1) { ?>
	              <div class="text-danger"><?php echo $error_address_1; ?></div>
	              <?php } ?>
	            </div>
	            
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="field-box mb20">
                      <div class="select-box">
                        <div class="select-label"><?php echo $entry_country; ?></div>
                        <div class="select-box-chevron">
                          <select class="select-field-line" name="country_id">
                            <option value="" selected disabled><?php echo $entry_country; ?></option>
			                <?php foreach ($countries as $country) { ?>
			                <option value="<?php echo $country['country_id']; ?>" <?php echo ($country['country_id'] == $country_id) ? 'selected' : ''; ?>><?php echo $country['name']; ?></option>
			                <?php } ?>
                          </select>
                        </div>
                      </div>
                      <?php if ($error_country) { ?>
		              <div class="text-danger"><?php echo $error_country; ?></div>
		              <?php } ?>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="field-box mb20">
                      <div class="select-box">
                        <div class="select-label"><?php echo $entry_zone; ?></div>
                        <div class="select-box-chevron">
                          <select class="select-field-line" name="zone_id">
                            <option value="" selected disabled><?php echo $entry_zone; ?></option>
                          </select>
                        </div>
                      </div>
                      <?php if ($error_zone) { ?>
		              <div class="text-danger"><?php echo $error_zone; ?></div>
		              <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="field-box mb20">
                      <div class="select-box">
                        <div class="select-label"><?php echo $entry_city; ?></div>
                        <div class="select-box-chevron">
                          <select class="select-field-line" name="city_id">
                            <option value="" selected disabled><?php echo $entry_city; ?></option>
                          </select>
                        </div>
                      </div>
                      <?php if ($error_city) { ?>
		              <div class="text-danger"><?php echo $error_city; ?></div>
		              <?php } ?>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="field-box mb20">
                      <div class="input-box <?php echo !empty($postcode) ? 'filled' : ''; ?>">
                        <div class="input-label"><?php echo $entry_postcode; ?></div>
                        <input class="input-field-line" type="text" name="postcode" value="<?php echo $postcode; ?>">
                      </div>
                      <?php if ($error_postcode) { ?>
		              <div class="text-danger"><?php echo $error_postcode; ?></div>
		              <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="field-box wmargin">
                  <div class="radio-title mb10"><?php echo $entry_default; ?></div>
                  <div class="radio-box">
                    <label class="radio-label" for="radio-default-yes"><?php echo $text_yes; ?>
                      <input type="radio" class="radio-field" value="1" name="default" id="radio-default-yes" <?php echo $default ? 'checked' : ''; ?>>
                      <span class="radio-mark"></span>
                    </label>
                  </div>
                  <div class="radio-box">
                    <label class="radio-label" for="radio-default-no"><?php echo $text_no; ?>
                      <input type="radio" class="radio-field" value="0" name="default" id="radio-default-no" <?php echo !$default ? 'checked' : ''; ?>>
                      <span class="radio-mark"></span>
                    </label>
                  </div>
                </div>
	            <div class="row">
	              <div class="col-md-6 col-sm-6 col-xs-4">
	                <div class="field-box wmargin">
	                  <a href="<?php echo $back; ?>" class="btn-slide" type="button"><?php echo $button_back; ?></a>
	                </div>
	              </div>
	              <div class="col-md-6 col-sm-6 col-xs-8">
	                <div class="field-box wmargin text-right">
	                  <button class="btn-slide green" type="submit"><?php echo $button_continue; ?></button>
	                </div>
	              </div>
	            </div>
	          </form>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('select[name=\'country_id\']').trigger('change');
	});
</script>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/account/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value="" selected disabled><?php echo $entry_zone; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
			  		}

			  		html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected disabled><?php echo $entry_zone; ?></option>';
			}

			$('select[name=\'zone_id\']').html(html);
			$('select[name=\'zone_id\']').trigger('change');
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');

/********** zone ************/
$('select[name=\'zone_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/account/zone&zone_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('select[name=\'zone_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      
      html = '<option value="" selected disabled><?php echo $entry_city; ?></option>';

      if (json['city'] && json['city'] != '') {
        for (i = 0; i < json['city'].length; i++) {
          html += '<option value="' + json['city'][i]['city_id'] + '"';

          if (json['city'][i]['city_id'] == '<?php echo $city_id; ?>') {
            html += ' selected="selected"';
            }

            html += '>' + json['city'][i]['name'] + '</option>';
        }
      } else {
        html += '<option value="0" selected disabled><?php echo $entry_city; ?></option>';
      }
      $('select[name=\'city_id\']').html(html);
	  $('select[name=\'city_id\']').trigger('change');
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

// $('select[name=\'city_id\']').on('change', function() {
// 	$('.tes').find('.id').text($(this).val());
// });


//$('select[name=\'zone_id\']').trigger('change');
//--></script>
<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>