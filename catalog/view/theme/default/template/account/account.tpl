<?php echo $header; ?>
<!-- ACCOUNT PAGE -->
<div class="account-page">
	<div class="container">
	  <div class="row">
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <?php echo $column_left; ?>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <!-- kalo mau mnculin error warning disini yaa -->
	      <!-- <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
	        error message
	      </div> -->
	      <?php if ($success) { ?>
		  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
		  <?php } ?>
	      <div class="account-content">
	        <div class="field-container">
	          <form>
	            <div class="field-box wmargin">
	              <div class="input-box filled">
	                <div class="input-label"><?php echo $entry_fullname; ?></div>
	                <input class="input-field-line" type="text" value="<?php echo $customer_info['fullname']; ?>" disabled>
	              </div>
	            </div>
	            <div class="field-box wmargin">
	              <div class="input-box filled">
	                <div class="input-label"><?php echo $entry_email; ?></div>
	                <input class="input-field-line" type="text" value="<?php echo $customer_info['email']; ?>" disabled>
	              </div>
	            </div>
	            <div class="field-box wmargin">
	              <div class="input-box filled">
	                <div class="input-label"><?php echo $entry_telephone; ?></div>
	                <input class="input-field-line" type="text" value="<?php echo $customer_info['telephone'] ?>" disabled>
	              </div>
	            </div>
	            <div class="row">
	              <!-- <div class="col-md-6 col-sm-6 col-xs-4">
	                <div class="field-box wmargin">
	                  <button class="btn-slide" id="btn-cancel-edit" type="button" style="display: none">Cancel</button>
	                </div>
	              </div> -->
	              <div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="field-box wmargin text-right">
	                  <a href="<?php echo $edit; ?>" class="btn-slide green" id="btn-edit-account" type="button"><?php echo $button_edit_account; ?></a>
	                </div>
	              </div>
	            </div>
	          </form>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>