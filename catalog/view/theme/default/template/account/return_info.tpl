<?php echo $header; ?>
<!-- ACCOUNT PAGE -->
<div class="account-page">
	<div class="container">
	  <div class="row">
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <?php echo $column_left; ?>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <div class="account-content">
            <div class="field-box wmargin">
              <div class="field-title"><?php echo $text_return_detail; ?></div>
            </div>
            <div class="field-container">
              <div class="table-box">
                <div class="table-body nopadding noborder">
                  <div class="table-row">
                    <div class="table-col col-order-left">
                      <span class="bold"><?php echo $text_return_id; ?></span> #<?php echo $return_id; ?>
                    </div>
                    <div class="table-col col-order-right">
                      <span class="bold"><?php echo $text_order_id; ?></span> #<?php echo $order_id; ?>
                    </div>
                  </div>
                  <div class="table-row">
                    <div class="table-col col-order-left">
                      <span class="bold"><?php echo $text_date_added; ?></span> <?php echo $date_added; ?>
                    </div>
                    <div class="table-col col-order-right">
                      <span class="bold"><?php echo $text_date_ordered; ?></span> <?php echo $date_ordered; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="field-box wmargin">
              <div class="field-title"><?php echo $text_product; ?></div>
            </div>
            <div class="field-container">
              <div class="table-box">
                <div class="table-head">
                  <div class="table-col col-name"><?php echo $column_product; ?></div>
                  <div class="table-col col-qty hidden-sm hidden-xs"><?php echo $column_quantity; ?></div>
                </div>
                <div class="table-body">
                  <div class="table-row">
                    <div class="table-col col-name">
                      <a class="hvr-orange" href="#"><?php echo $product; ?></a>
                      <div class="visible-xs visible-sm text-grey"><?php echo $quantity; ?> x 250gr</div>
                    </div>
                    <div class="table-col col-qty hidden-xs hidden-sm">
                      <?php echo $quantity; ?> x 250gr
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="field-box wmargin">
              <div class="field-title"><?php echo $text_reason; ?></div>
            </div>
            <div class="field-container">
              <div class="table-box">
                <div class="table-head">
                  <div class="table-col col-return-reason"><?php echo $column_reason; ?></div>
                  <div class="table-col col-return-opened"><?php echo $column_opened; ?></div>
                  <div class="table-col col-return-info-action"><?php echo $column_action; ?></div>
                </div>
                <div class="table-body">
                  <div class="table-row">
                    <div class="table-col col-return-reason"><?php echo $reason; ?></div>
                    <div class="table-col col-return-opened"><?php echo $opened; ?></div>
                    <div class="table-col col-return-info-action"><?php echo $action; ?></div>
                  </div>
                  <!-- <div class="table-row">
                      <div class="table-col col-empty">no data</div>
                    </div> -->
                </div>
              </div>
            </div>
            <?php if ($comment) { ?>
            <div class="field-container">
              <div class="table-box">
                <div class="table-head">
                  <div class="table-col col-full"><?php echo $text_comment; ?></div>
                </div>
                <div class="table-body">
                  <div class="table-row">
                    <div class="table-col col-full"><?php echo $comment; ?></div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
            <div class="field-box wmargin">
              <div class="field-title"><?php echo $text_history; ?></div>
            </div>
            <div class="field-container">
              <div class="table-box">
                <div class="table-head">
                  <div class="table-col col-date hidden-xs"><?php echo $column_date_added; ?></div>
                  <div class="table-col col-order-status xs-pl0"><?php echo $column_status; ?></div>
                  <div class="table-col col-order-comment"><?php echo $column_comment; ?></div>
                </div>
                <div class="table-body">
            			<?php if ($histories) { ?>
            			<?php foreach ($histories as $history) { ?>
                  <div class="table-row">
                    <div class="table-col col-date hidden-xs"><?php echo $history['date_added']; ?></div>
                    <div class="table-col col-order-status xs-pl0">
                      <div class="visible-xs mb5"><?php echo $history['date_added']; ?></div>
                      <?php echo $history['status']; ?>
                    </div>
                    <div class="table-col col-order-comment"><?php echo $history['comment']; ?></div>
                  </div>
            			<?php } ?>
            			<?php } else { ?>
            			<div class="table-row">
                    <div class="table-col col-empty"><?php echo $text_no_results; ?></div>
                  </div>
            			<?php } ?>
                </div>
              </div>
            </div>
            <div class="field-box wmargin text-right">
              <!-- balik ke page order list -->
              <a class="btn-slide" href="<?php echo $continue; ?>"><?php echo $button_continue; ?></a>
            </div>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>