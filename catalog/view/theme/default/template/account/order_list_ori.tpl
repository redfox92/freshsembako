<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php if ($orders) { ?>
      <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-right"><?php echo $column_order_id; ?></td>
              <td class="text-left"><?php echo $column_customer; ?></td>
              <td class="text-right"><?php echo $column_product; ?></td>
              <td class="text-left"><?php echo $column_status; ?></td>
              <td class="text-right"><?php echo $column_total; ?></td>
              <td class="text-left"><?php echo $column_date_added; ?></td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($orders as $order) { ?>
            <tr>
              <td class="text-right">#<?php echo $order['order_id']; ?></td>
              <td class="text-left"><?php echo $order['name']; ?></td>
              <td class="text-right"><?php echo $order['products']; ?></td>
              <td class="text-left"><?php echo $order['status']; ?></td>
              <td class="text-right"><?php echo $order['total']; ?></td>
              <td class="text-left"><?php echo $order['date_added']; ?></td>
              <td class="text-right">
                <?php if($order['resi']) { ?>
                <button type="button" class="btn btn-primary tracking" data-toggle="tooltip" title="<?php echo $button_track; ?>" data-resi="<?php echo $order['resi']; ?>"><i class="fa fa-send"></i></button>
                <?php } ?>
                <a href="<?php echo $order['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

<!-- Modal -->
<div class="modal fade modal-custom" id="modalTracking" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-send"></i> <?php echo $text_tracking; ?></h4>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $(document).on('click', '.tracking', function(event) {
      event.preventDefault();
      
      $.ajax({
        url: '<?php echo $tracking_url; ?>',
        type: 'POST',
        dataType: 'json',
        data: {resi: $(this).data('resi')},
        beforeSend: function() {
          // add loading
          var html = '<div class="loading-full"><div class="loading-full-content text-center"><i class="fa fa-circle-o-notch fa-spin fa-3x"></i></div></div>';
          $('body').append(html);
        },
        complete: function(data) {
          // init
          var data = data.responseJSON;

          // remove loading
          $('.loading-full').remove();
          $('#modalTracking .modal-body').html('');
          
          if (data['status'] == false) {
            $('#modalTracking .modal-body').append(data['error']);
          }
          else {
            var cnote = data.cnote;
            var detail = data.detail;
            var manifest = data.manifest;
            var runsheet = data.runsheet;
            var mergeArr = manifest.concat(runsheet);
            var html = '';

            // sort by date
            mergeArr.sort(function(a,b){
            dateA = (typeof a.manifest_date === 'undefined') ? a.mrsheet_date : a.manifest_date;
            dateB = (typeof b.manifest_date === 'undefined') ? b.mrsheet_date : b.manifest_date;
              return new Date(dateB) - new Date(dateA);
            });

            if (typeof mergeArr) {
              html += '<p>No Resi: '+ cnote['cnote_no'] +'<br />Status: <b>'+ cnote['pod_status'] +'</b></p>';
              html += '<table class="table table-striped">'
              html += '<thead>';
              html += '<tr><td>Date</td><td>City</td><td>Information</td></tr>';
              html += '</thead>';
              for (var i = 0; i < mergeArr.length; i++) {
                var date = (typeof mergeArr[i]['manifest_date'] === 'undefined') ? mergeArr[i]['mrsheet_date'] : mergeArr[i]['manifest_date'];
                var ket = (typeof mergeArr[i]['keterangan'] === 'undefined') ? mergeArr[i]['pod_status'] : mergeArr[i]['keterangan'];

                html += '<tr><td>'+ date +'</td><td>'+ mergeArr[i]['city_name'] +'</td><td>'+ ket +'<td/></tr>'
              }
              html += '</table>'         
            }   
          }

          $('#modalTracking .modal-body').append(html);
          $('#modalTracking').modal();
        }
      });

    });
  });
</script>
<?php echo $footer; ?>
