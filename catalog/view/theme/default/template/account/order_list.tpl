<?php echo $header; ?>
<!-- ACCOUNT PAGE -->
<div class="account-page">
	<div class="container">
	  <div class="row">
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <?php echo $column_left; ?>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <!-- kalo mau mnculin error warning disini yaa -->
	      <!-- <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
	        error message
	      </div> -->
		  <?php if ($error_warning) { ?>
		  <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
		  <?php } ?>
	      <div class="account-content">
	        <div class="field-container">
				<div class="table-box">
					<div class="table-head">
					  <div class="table-col col-order-list-no hidden-xs"><?php echo $column_invoice_no; ?></div>
					  <div class="table-col col-order-list-date hidden-xs hidden-sm"><?php echo $column_date_added; ?></div>
					  <div class="table-col col-order-list-status hidden-xs"><?php echo $column_status; ?></div>
					  <div class="table-col col-order-list-total hidden-xs"><?php echo $column_total; ?></div>
					  <div class="table-col col-visible-xs col-order-mobile-detail xs-pl0">Detail</div>
					  <div class="table-col col-order-action">
					    Action
					  </div>
					</div>
					<div class="table-body">
						<?php if ($orders) { ?>
			            <?php foreach ($orders as $order) { ?>
						<div class="table-row">
							<div class="table-col col-order-list-no hidden-xs">#<?php echo $order['invoice_no']; ?></div>
							<div class="table-col col-order-list-date hidden-sm xs-pl0">
                      			<div class="visible-xs bold">#<?php echo $order['invoice_no']; ?></div>
								<div><?php echo $order['date_added']; ?></div>
								<div class="visible-xs"><?php echo $order['status']; ?></div>
								<div class="visible-xs"><?php echo $order['total']; ?></div>
							</div>
							<div class="table-col col-order-list-status hidden-xs">
                      			<div class="visible-sm text-grey"><?php echo $order['date_added']; ?></div>
								<?php echo $order['status']; ?>
							</div>
							<div class="table-col col-order-list-total hidden-xs"><?php echo $order['total']; ?></div>
							<div class="table-col col-order-action">
								<a href="<?php echo $order['view']; ?>" class="btn-slide green"><i class="fa fa-eye"></i></a>
							</div>
						</div>			            	
			            <?php } ?>
						<?php } else { ?>
							<div class="table-row">
								<div class="table-col col-empty"><?php echo $text_empty; ?></div>
							</div>
						<?php } ?>
					</div>
				</div>
	        </div>
	        <?php if ($pagination): ?>
            <div class="pagination-box text-center">
            	<?php echo $pagination; ?>
            </div>
	        <?php endif ?>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>