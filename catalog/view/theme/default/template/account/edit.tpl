<?php echo $header; ?>
<!-- ACCOUNT PAGE -->
<div class="account-page">
	<div class="container">
	  <div class="row">
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <?php echo $column_left; ?>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <!-- kalo mau mnculin error warning disini yaa -->
	      <!-- <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
	        error message
	      </div> -->

		  <?php if ($error_warning) { ?>
		  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
		  <?php } ?>
	      <div class="account-content">
	        <div class="field-container">
	          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	            <div class="field-box wmargin">
	              <div class="input-box <?php echo !empty($fullname) ? 'filled' : ''; ?>">
	                <div class="input-label"><?php echo $entry_fullname; ?></div>
	                <input class="input-field-line" type="text" name="fullname" value="<?php echo $fullname; ?>">
	              </div>
	              <?php if ($error_fullname) { ?>
	              <div class="text-danger"><?php echo $error_fullname; ?></div>
	              <?php } ?>
	            </div>
	            <div class="field-box wmargin">
	              <div class="input-box <?php echo !empty($email) ? 'filled' : ''; ?>">
	                <div class="input-label"><?php echo $entry_email; ?></div>
	                <input class="input-field-line" type="text" name="email" value="<?php echo $email; ?>">
	              </div>
	              <?php if ($error_email) { ?>
	              <div class="text-danger"><?php echo $error_email; ?></div>
	              <?php } ?>
	            </div>
	            <div class="field-box wmargin">
	              <div class="input-box <?php echo !empty($telephone) ? 'filled' : ''; ?>">
	                <div class="input-label"><?php echo $entry_telephone; ?></div>
	                <input class="input-field-line" type="text" name="telephone" value="<?php echo $telephone ?>">
	              </div>
	              <?php if ($error_telephone) { ?>
	              <div class="text-danger"><?php echo $error_telephone; ?></div>
	              <?php } ?>
	            </div>
	            <div class="row">
	              <div class="col-md-6 col-sm-6 col-xs-4">
	                <div class="field-box wmargin">
	                  <a href="<?php echo $back; ?>" class="btn-slide" id="btn-cancel-edit" type="button"><?php echo $button_back; ?></a>
	                </div>
	              </div>
	              <div class="col-md-6 col-sm-6 col-xs-8">
	                <div class="field-box wmargin text-right">
	                  <button class="btn-slide green" id="btn-edit-account" type="submit"><?php echo $button_continue; ?></button>
	                </div>
	              </div>
	            </div>
	          </form>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>