<?php echo $header; ?>

<div class="payment-confirmation-page">
    <div class="container">
        <ul class="breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
          <?php } ?>
        </ul>

        <div class="row">
            <?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6 col-md-6 col-xs-12'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9 col-md-9 col-xs-12'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-12 col-md-12 col-xs-12'; ?>
            <?php } ?>

            <div class="<?php echo $class; ?> payment-confirmation-content" id="content">
                <?php echo $content_top; ?>

                <div class="red-title">
                    <div class="rt-title"><?php echo $heading_title; ?></div>
                </div>

                <form class="form-horizontal" method="post" action="<?php echo $action; ?>" enctype="multipart/form-data" >
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 pc-left">
                            <div class="title-line-through">
                                <div class="tlt-title"><?php echo $text_bank; ?></div>
                                <div class="tlt-line"><div class="line"></div></div>
                            </div>

                            <div class="field-box">
                                <div class="label-field"><?php echo $text_bank; ?></div>
                                <input type="text" name="user_bank" value="<?php echo $user_bank; ?>" placeholder="<?php echo $text_bank; ?>" id="user_bank" class="input-field" />
                                <?php if ($error_user_bank) { ?>
                                <div class="text-error"><?php echo $error_user_bank; ?></div>
                                <?php } ?>
                            </div>

                            <div class="field-box">
                                <div class="label-field"><?php echo $text_user_account; ?></div>
                                <input type="text" name="user_account" value="<?php echo $user_account; ?>" placeholder="<?php echo $text_user_account; ?>" id="user_account" class="input-field" />
                                <?php if ($error_user_account) { ?>
                                <div class="text-error"><?php echo $error_user_account; ?></div>
                                <?php } ?>
                            </div>

                            <div class="field-box">
                                <div class="label-field"><?php echo $text_user_name; ?></div>
                                <input type="text" name="user_name" value="<?php echo $user_name; ?>" placeholder="<?php echo $ph_atasnama; ?>" id="user_name" class="input-field" />
                                <?php if ($error_user_name) { ?>
                                <div class="text-error"><?php echo $error_user_name; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 pc-right">
                            <div class="title-line-through">
                                <div class="tlt-title"><?php echo $text_payment; ?></div>
                                <div class="tlt-line"><div class="line"></div></div>
                            </div>

                            <div class="field-box">
                                <div class="label-field"><?php echo $text_payment_order; ?></div>
                                <input type="hidden" name="payment_order" value="<?php echo $payment_order; ?>">

                                <div class="select-box-caret grey">
                                    <select <?php echo ($payment_order) ? 'disabled' : ''; ?> name="payment_order" id="payment_order" class="select-field grey" onchange="$('#payment_total').val($(this).find('option:selected').attr('total'))">
                                        <option value=""><?php echo $text_select_order; ?></option>
                                        <?php foreach($orders as $item){ ?>
                                        <option <?php if($payment_order==$item['order_id']) echo 'selected="selected"' ?> total="<?php echo $item['total']; ?>" value="<?php echo $item['order_id']; ?>"><?php echo $item['label']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <?php if ($error_payment_order) { ?>
                                <div class="text-error"><?php echo $error_payment_order; ?></div>
                                <?php } ?>
                            </div>

                            <div class="field-box">
                                <div class="label-field"><?php echo $text_payment_bank; ?></div>

                                <div class="select-box-caret grey">
                                    <select name="payment_bank" id="payment_bank" class="select-field grey">
                                        <option value=""><?php echo $text_select_bank; ?></option>
                                        <?php foreach($payment_banks as $item){ ?>
                                            <option <?php if($payment_bank==$item['code']) echo 'selected="selected"' ?> value="<?php echo $item['code']; ?>"><?php echo str_replace('Transfer via','',$item['title']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <?php if ($error_payment_bank) { ?>
                                <div class="text-error"><?php echo $error_payment_bank; ?></div>
                                <?php } ?>
                            </div>

                            <div class="field-box">
                                <div class="label-field"><?php echo $text_payment_total; ?></div>
                                <input type="text" name="payment_total" value="<?php echo $payment_total; ?>" placeholder="<?php echo $ph_jumlah_pembayaran; ?>" id="payment_total" class="input-field" readonly />
                                <?php if ($error_payment_total) { ?>
                                <div class="text-error"><?php echo $error_payment_total; ?></div>
                                <?php } ?>
                            </div>

                            <div class="field-box">
                                <div class="label-field"><?php echo $text_payment_date; ?></div>
                                <input type="text" readonly name="payment_date" value="<?php echo $payment_date; ?>" placeholder="<?php echo $ph_tgl_pembayaran; ?>" id="payment_date" class="input-field" />
                                <?php if ($error_payment_date) { ?>
                                <div class="text-error"><?php echo $error_payment_date; ?></div>
                                <?php } ?>
                            </div>

                            <div class="field-box">
                                <div class="label-field"><?php echo $text_payment_memo; ?></div>
                                <textarea name="payment_memo" value="<?php echo $payment_memo; ?>" placeholder="<?php echo $ph_keterangan; ?>" id="payment_memo" class="textarea-field" rows="3"/></textarea>
                                <?php if ($error_payment_memo) { ?>
                                <div class="text-error"><?php echo $error_payment_memo; ?></div>
                                <?php } ?>
                            </div>

                            <div class="field-box">
                                <div class="label-field"><?php echo $attachment_text_file; ?></div>
                                <input type="file" name="file" id="attachment_file" class="input-field" />
                                <p>Allowed type file: <strong>jpg, png</strong>. Max file size: <strong>2MB</strong></p>
                                <?php if ($error_attachment_file) { ?>
                                <div class="text-error"><?php echo $error_attachment_file; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="row mt20">
                        <div class="col-sm-6 col-md-6 col-xs-6 text-left"><a href="<?php echo $back; ?>"><button type="button" class="btn-slide"><?php echo $button_back; ?></button></a></div>

                        <div class="col-sm-6 col-md-6 col-xs-6 text-right"><button type="submit" value="<?php echo $button_continue; ?>" class="btn-slide">Update</button></div>
                    </div>
                </form>

                <?php echo $content_bottom; ?>
            </div>

            <?php echo $column_right; ?>
        </div>
    </div>
</div>

<?php echo $footer; ?>

<script type="text/javascript">
    $(document).ready(function() {
       $('#payment_date').datepicker({
            changeYear: true,
            yearRange: '-100:+0',
            dateFormat: 'dd-M-yy',
            maxDate: '0',
        });

       var payment_order = "<?php echo $payment_order; ?>";

       if (payment_order) {
        $('#payment_order').trigger('change');
       }
    });
</script>