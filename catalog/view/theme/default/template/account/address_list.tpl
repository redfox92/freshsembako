<?php echo $header; ?>
<!-- ACCOUNT PAGE -->
<div class="account-page">
	<div class="container">
	  <div class="row">
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <?php echo $column_left; ?>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <!-- kalo mau mnculin error warning disini yaa -->
	      <!-- <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
	        error message
	      </div> -->
		  <?php if ($success) { ?>
		  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
		  <?php } ?>
		  <?php if ($error_warning) { ?>
		  <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
		  <?php } ?>
	      <div class="account-content">
	        <div class="field-container">
	          <div class="table-box">
                <div class="table-head">
                  <div class="table-col col-address">
                    <?php echo $text_address_book; ?>
                  </div>
                  <div class="table-col col-address-action">
                    Action
                  </div>
                </div>
                <div class="table-body">
      			<?php if ($addresses) { ?>
         		<?php foreach ($addresses as $result) { ?>
                  <div class="table-row">
                    <div class="table-col col-address">
                      <?php echo $result['address']; ?>
                    </div>
                    <div class="table-col col-address-action">
                      <a href="<?php echo $result['update']; ?>" class="btn-slide green"><i class="fa fa-pencil"></i></a>
                      <a href="<?php echo $result['delete']; ?>" class="btn-slide"><i class="fa fa-trash"></i></a>
                    </div>
                  </div>
          		<?php } ?>
      			<?php } else { ?>
      				<div class="table-row">
	                    <div class="table-col col-empty"><?php echo $text_empty; ?></div>
	                </div>
      			<?php } ?>
                </div>
              </div>
              <!-- <div class="pagination-box text-center">
                <ul class="pagination">
                  <li><a class="text-first" href="http://lab.gositus.com/gemilang_new/order-history?page={page}">&lt;&lt;</a></li>
                  <li><a class="text-prev" href="http://lab.gositus.com/gemilang_new/order-history?page=3">PREV</a></li>
                  <li><a class="1" href="http://lab.gositus.com/gemilang_new/order-history?page={page}">1</a></li>
                  <li class="active"><span>2</span></li>
                  <li><a class="2" href="http://lab.gositus.com/gemilang_new/order-history?page=5">3</a></li>
                  <li><a class="text-next" href="http://lab.gositus.com/gemilang_new/order-history?page=5">NEXT</a></li>
                  <li><a class="text-last" href="http://lab.gositus.com/gemilang_new/order-history?page=8">&gt;&gt;</a></li>
                </ul>
              </div> -->

	            <div class="row">
	              <div class="col-md-6 col-sm-6 col-xs-4">
	                <div class="field-box wmargin">
	                  <a href="<?php echo $back; ?>" class="btn-slide" id="btn-cancel-edit" type="button"><?php echo $button_back; ?></a>
	                </div>
	              </div>
	              <div class="col-md-6 col-sm-6 col-xs-8">
		              <div class="field-box wmargin text-right">
		                <a class="btn-slide green" href="<?php echo $add; ?>"><?php echo $button_new_address; ?></a>
		              </div>
	              </div>
	            </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>