<?php echo $header; ?>
<!-- ACCOUNT PAGE -->

<div class="account-page">
  <div class="container">
    <div class="account-content">
		  <?php if ($success) { ?>
		  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
		  <?php } ?>
		  <?php if ($error_warning) { ?>
		  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
		  <?php } ?>
      <div class="field-container">
        <div class="color-title"><span class="orange"><?php echo $heading_title[0]; ?></span> <span class="green"><?php echo $heading_title[1]; ?> <?php echo $heading_title[2]; ?></span></div>
        <div class="field-desc mb20"><?php echo $text_email; ?></div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
          <div class="field-box wmargin">
            <div class="input-box">
              <div class="input-label"><?php echo $entry_email; ?></div>
              <input class="input-field-line" type="text" name="email" value="<?php echo $email; ?>">
            </div>
          </div>
          <div class="field-box wmargin">
            <button class="btn-slide green" type="submit"><?php echo $button_continue; ?></button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>