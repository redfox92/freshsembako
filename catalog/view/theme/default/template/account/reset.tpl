<?php echo $header; ?>
<!-- ACCOUNT PAGE -->

<div class="account-page">
  <div class="container">
    <div class="account-content">
      <div class="field-container">
        <div class="color-title"><span class="orange"><?php echo $heading_title[0]; ?></span> <span class="green"><?php echo $heading_title[1]; ?></span></div>
        <!-- <div class="field-desc mb20">Enter the new password you want to use.</div> -->
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
          <div class="field-box wmargin">
            <div class="input-box <?php echo $password ? 'filled' : ''; ?>">
              <div class="input-label"><?php echo $entry_password; ?></div>
              <input class="input-field-line" type="password" name="password" value="<?php echo $password; ?>">
            </div>
            <?php if ($error_password) { ?>
            <div class="text-danger"><?php echo $error_password; ?></div>
            <?php } ?>
          </div>
          <div class="field-box wmargin">
            <div class="input-box <?php echo $confirm ? 'filled' : ''; ?>">
              <div class="input-label"><?php echo $entry_confirm; ?></div>
              <input class="input-field-line" type="password" name="confirm" value="<?php echo $confirm; ?>">
            </div>
            <?php if ($error_confirm) { ?>
            <div class="text-danger"><?php echo $error_confirm; ?></div>
            <?php } ?>
          </div>
          <div class="field-box wmargin">
            <button class="btn-slide green" type="submit"><?php echo $button_continue; ?></button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>