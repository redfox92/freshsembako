<?php echo $header; ?>
<!-- ACCOUNT PAGE -->
<div class="account-page">
	<div class="container">
	  <div class="row">
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <?php echo $column_left; ?>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <!-- kalo mau mnculin error warning disini yaa -->
	      <!-- <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
	        error message
	      </div> -->
	      <div class="account-content">
          <form method="post" action="<?php echo $action; ?>" enctype="multipart/form-data">
            <div class="field-container">
              <div class="field-title"><?php echo $text_bank; ?></div>
              <div class="field-box wmargin">
                <div class="input-box <?php echo $user_bank ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $text_bank; ?></div>
                  <input class="input-field-line" type="text" name="user_bank" value="<?php echo $user_bank; ?>">
                </div>
                <?php if ($error_user_bank) { ?>
                <div class="text-error"><?php echo $error_user_bank; ?></div>
                <?php } ?>
              </div>
              <div class="field-box wmargin">
                <div class="input-box <?php echo $user_account ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $text_user_account; ?></div>
                  <input class="input-field-line" type="text" name="user_account" value="<?php echo $user_account; ?>">
                </div>
                <?php if ($error_user_account) { ?>
                <div class="text-error"><?php echo $error_user_account; ?></div>
                <?php } ?>
              </div>
              <div class="field-box wmargin">
                <div class="input-box <?php echo $user_name ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $text_user_name; ?></div>
                  <input class="input-field-line" type="text" name="user_name" value="<?php echo $user_name; ?>">
                </div>
                <?php if ($error_user_name) { ?>
                <div class="text-error"><?php echo $error_user_name; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="field-container">
              <div class="field-title"><?php echo $text_payment; ?></div>
              <div class="field-box wmargin">
                <div class="select-box">
                  <div class="select-label <?php echo $payment_order ? 'filled' : ''; ?>"><?php echo $text_payment_invoice; ?></div>
                  <div class="select-box-chevron">
                    <input type="hidden" name="payment_order" value="<?php echo $payment_order; ?>">
                    <select class="select-field-line <?php echo $payment_order ? 'filled' : ''; ?>" name="payment_order" <?php echo $from ? 'disabled' : ''; ?>>
                      <option value="" selected disabled><?php echo $text_payment_invoice; ?></option>
                      <?php foreach($orders as $item){ ?>
                      <option total="<?php echo $item['total']; ?>" value="<?php echo $item['order_id']; ?>" <?php echo ($payment_order==$item['order_id']) ? 'selected' : ''; ?>><?php echo $item['label']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <?php if ($error_payment_order) { ?>
                  <div class="text-error"><?php echo $error_payment_order; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="field-box wmargin">
                <div class="select-box">
                  <div class="select-label <?php echo $payment_bank ? 'filled' : ''; ?>"><?php echo $text_payment_bank; ?></div>
                  <div class="select-box-chevron">
                    <select class="select-field-line <?php echo $payment_bank ? 'filled' : ''; ?>" name="payment_bank" <?php echo ($from) ? 'readonly' : ''; ?>>
                      <option value="" selected disabled><?php echo $text_payment_bank; ?></option>
                      <?php foreach($payment_banks as $item){ ?>
                          <option <?php echo ($payment_bank==$item['code']) ? 'selected=' : ''; ?> value="<?php echo $item['code']; ?>"><?php echo str_replace('Transfer via','',$item['title']); ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <?php if ($error_payment_bank) { ?>
                  <div class="text-error"><?php echo $error_payment_bank; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="field-box mb20">
                    <div class="input-box <?php echo $payment_total ? 'filled' : ''; ?>">
                      <div class="input-label"><?php echo $text_payment_total; ?></div>
                      <input class="input-field-line" type="text" name="payment_total" value="<?php echo $payment_total; ?>" readonly>
                    </div>
                    <?php if ($error_payment_total) { ?>
                    <div class="text-error"><?php echo $error_payment_total; ?></div>
                    <?php } ?>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="field-box mb20">
                    <div class="input-box <?php echo $payment_date ? 'filled' : ''; ?>">
                      <div class="input-label"><?php echo $text_payment_date; ?></div>
                      <input class="input-field-line" type="text" readonly id="payment-date" name="payment_date" value="<?php echo $payment_date; ?>">
                    </div>
                    <?php if ($error_payment_date) { ?>
                    <div class="text-error"><?php echo $error_payment_date; ?></div>
                    <?php } ?>
                  </div>
                </div>
              </div>

              <div class="field-box wmargin">
                <div class="textarea-box <?php echo $payment_memo ? 'filled' : ''; ?>">
                  <div class="textarea-label"><?php echo $text_payment_memo; ?></div>
                  <textarea class="textarea-field-line" name="payment_memo"><?php echo $payment_memo; ?></textarea>
                </div>
                <?php if ($error_payment_memo) { ?>
                <div class="text-error"><?php echo $error_payment_memo; ?></div>
                <?php } ?>
              </div>
              <div class="field-box wmargin">
                <div class="input-box filled">
                  <div class="input-label w100"><?php echo $attachment_text_file; ?></div>
                  <input class="input-field-line file-field" type="file" name="file">
                </div>
                <?php if ($error_attachment_file) { ?>
                <div class="text-error"><?php echo $error_attachment_file; ?></div>
                <?php } ?>
              </div>
              <div class="field-box wmargin text-right">
                <button class="btn-slide" type="submit"><?php echo $button_continue; ?></button>
              </div>
            </div>
          </form>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function () {
  var today = new Date();
  var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Des'];
  $("#payment-date").val(today.getDate() + ' ' + month[today.getMonth()] + ' ' + today.getFullYear());

  $("#payment-date").datepicker({
    dateFormat: 'd M yy',
    monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
    setDate: new Date(),
    yearRange: '-10:+0',
    maxDate: new Date(),
    changeYear: true,
    showButtonPanel: true,
  });

  $('select[name=payment_order]').on('change', function(event) {
    $('input[name=payment_total]').val($(this).find('option:selected').attr('total'));
    $('input[name=payment_total]').focus();
  });
});
</script>
<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>