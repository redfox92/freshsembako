<?php echo $header; ?>
<div class="account-page">
	<div class="container">
	  <div class="row">
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <?php echo $column_left; ?>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <!-- kalo mau mnculin error warning disini yaa -->
	      <!-- <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
	        error message
	      </div> -->
	      <?php if ($success) { ?>
		  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
		  <?php } ?>
	      <div class="account-content">
	        <div class="field-container">
	          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div class="field-box wmargin">
                  <div class="input-box">
                    <div class="input-label"><?php echo $entry_password; ?></div>
                    <input class="input-field-line" type="password" name="password" value="<?php echo $password; ?>">
                  </div>
                  <?php if ($error_password) { ?>
	              <div class="text-danger"><?php echo $error_password; ?></div>
	              <?php } ?>
                </div>
                <div class="field-box wmargin">
                  <div class="input-box">
                    <div class="input-label"><?php echo $entry_confirm; ?></div>
                    <input class="input-field-line" type="password" name="confirm" value="<?php echo $confirm; ?>">
                  </div>
                  <?php if ($error_confirm) { ?>
	              <div class="text-danger"><?php echo $error_confirm; ?></div>
	              <?php } ?>
                </div>
	            <div class="row">
	              <div class="col-md-6 col-sm-6 col-xs-4">
	                <div class="field-box wmargin">
	                  <a href="<?php echo $back; ?>" class="btn-slide" id="btn-cancel-edit" type="button"><?php echo $button_back; ?></a>
	                </div>
	              </div>
	              <div class="col-md-6 col-sm-6 col-xs-8">
	                <div class="field-box wmargin text-right">
	                  <button class="btn-slide green" id="btn-edit-account" type="submit"><?php echo $button_continue; ?></button>
	                </div>
	              </div>
	          	</div>
              </form>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<?php echo $footer; ?>