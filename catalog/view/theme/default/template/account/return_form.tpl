<?php echo $header; ?>
<!-- ACCOUNT PAGE -->
<div class="account-page">
	<div class="container">
	  <div class="row">
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <?php echo $column_left; ?>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">

		  <?php if ($error_warning) { ?>
		  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
		  <?php } ?>
	      <div class="account-content">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <div class="field-container nomargin">
              <div class="field-box wmargin">
                <div class="field-title"><?php echo $text_order; ?></div>
              </div>
              <div class="field-box wmargin">
                <div class="input-box <?php echo $fullname ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $entry_fullname ?></div>
                  <input class="input-field-line" type="text" name="fullname" value="<?php echo $fullname ?>">
                </div>
                <?php if ($error_fullname) { ?>
	              <div class="text-danger"><?php echo $error_fullname; ?></div>
	              <?php } ?>
              </div>
              <div class="field-box wmargin">
                <div class="input-box <?php echo $email ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $entry_email; ?></div>
                  <input class="input-field-line" type="text" name="email" value="<?php echo $email; ?>">
                </div>
                <?php if ($error_email) { ?>
	              <div class="text-danger"><?php echo $error_email; ?></div>
	              <?php } ?>
              </div>
              <div class="field-box wmargin">
                <div class="input-box <?php echo $telephone ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $entry_telephone; ?></div>
                  <input class="input-field-line" type="text" name="telephone" value="<?php echo $telephone; ?>">
                </div>
                <?php if ($error_telephone) { ?>
	              <div class="text-danger"><?php echo $error_telephone; ?></div>
	              <?php } ?>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="field-box mb20">
                    <div class="input-box <?php echo $order_id ? 'filled' : ''; ?>">
                      <div class="input-label"><?php echo $entry_order_id; ?></div>
                      <input class="input-field-line" type="text" name="order_id" value="<?php echo $order_id; ?>">
                    </div>
                    <?php if ($error_order_id) { ?>
			              <div class="text-danger"><?php echo $error_order_id; ?></div>
			              <?php } ?>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="field-box mb20">
                    <div class="input-box <?php echo $date_ordered ? 'filled' : ''; ?>">
                      <div class="input-label"><?php echo $entry_date_ordered; ?></div>
                      <input class="input-field-line" type="text" id="order-date" name="date_ordered" value="<?php echo $date_ordered; ?>" readonly>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div class="field-container">
              <div class="field-box wmargin">
                <div class="field-title"><?php echo $text_product; ?></div>
              </div>
              <div class="field-box wmargin">
                <div class="input-box <?php echo $product ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $entry_product; ?></div>
                  <input class="input-field-line" type="text" name="product" value="<?php echo $product; ?>">
                </div>
                <?php if ($error_product) { ?>
	              <div class="text-danger"><?php echo $error_product; ?></div>
	              <?php } ?>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="field-box mb20">
                    <div class="input-box <?php echo $model ? 'filled' : ''; ?>">
                      <div class="input-label"><?php echo $entry_model; ?></div>
                      <input class="input-field-line" type="text" name="model" value="<?php echo $model; ?>">
                    </div>
                    <?php if ($error_model) { ?>
			              <div class="text-danger"><?php echo $error_model; ?></div>
			              <?php } ?>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="field-box mb20">
                    <div class="input-box <?php echo $quantity ? 'filled' : ''; ?>">
                      <div class="input-label"><?php echo $entry_quantity; ?></div>
                      <input class="input-field-line" type="text" name="quantity" value="<?php echo $quantity; ?>">
                    </div>
                  </div>
                </div>
              </div>
              <div class="field-box wmargin">
                <div class="radio-title mb10"><?php echo $entry_reason; ?></div>
	              <?php foreach ($return_reasons as $key => $return_reason) { ?>
	                <div class="radio-box">
	                  <label class="radio-label" for="reason-<?php echo $key ?>"><?php echo $return_reason['name']; ?>
	                    <input type="radio" class="radio-field" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" id="reason-<?php echo $key ?>" <?php echo ($return_reason['return_reason_id'] == $return_reason_id) ? 'checked' : ''; ?>>
	                    <span class="radio-mark"></span>
	                  </label>
	                </div>
	              <?php  } ?>
	              <?php if ($error_reason) { ?>
	              <div class="text-danger"><?php echo $error_reason; ?></div>
	              <?php } ?>
              </div>

              <div class="field-box wmargin">
                <div class="radio-title mb10"><?php echo $entry_opened; ?></div>
                <div class="radio-box">
                  <label class="radio-label" for="opened-1"><?php echo $text_yes; ?>
                    <input type="radio" class="radio-field" name="opened" value="1" id="opened-1" <?php echo $opened ? 'checked' : ''; ?>>
                    <span class="radio-mark"></span>
                  </label>
                </div>
                <div class="radio-box">
                  <label class="radio-label" for="opened-2"><?php echo $text_no; ?>
                    <input type="radio" class="radio-field" name="opened" value="0" id="opened-2" <?php echo !$opened ? 'checked' : ''; ?>>
                    <span class="radio-mark"></span>
                  </label>
                </div>
              </div>
              <div class="field-box wmargin">
                <div class="textarea-box <?php echo $comment ? 'filled' : ''; ?>">
                  <div class="textarea-label"><?php echo $entry_fault_detail; ?></div>
                  <textarea class="textarea-field-line" rows="3" name="comment"><?php echo $comment; ?></textarea>
                </div>
              </div>
              <div class="field-box wmargin text-right">
                <button class="btn-slide green" type="submit"><?php echo $button_submit; ?></button>
              </div>
            </div>
          </form>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#order-date").datepicker({
      dateFormat: 'd M yy',
      setDate: new Date(),
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      setDate: new Date(),
      yearRange: '-10:+0',
      maxDate: new Date(),
      changeYear: true,
      showButtonPanel: true,
    });
	});
</script>
<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>