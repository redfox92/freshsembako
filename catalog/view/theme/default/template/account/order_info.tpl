<?php echo $header; ?>
<!-- ACCOUNT PAGE -->
<div class="account-page">
	<div class="container">
	  <div class="row">
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <?php echo $column_left; ?>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <!-- kalo mau mnculin error warning disini yaa -->
	      <!-- <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
	        error message
	      </div> -->

		  <?php if ($success) { ?>
		  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
		    <button type="button" class="close" data-dismiss="alert">&times;</button>
		  </div>
		  <?php } ?>
		  <?php if ($error_warning) { ?>
		  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		    <button type="button" class="close" data-dismiss="alert">&times;</button>
		  </div>
		  <?php } ?>
		  <?php if ($error_warning) { ?>
		  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
		  <?php } ?>
	      <div class="account-content">
            <div class="field-box wmargin">
              <div class="field-title"><?php echo $heading_title; ?></div>
            </div>

            <div class="field-container">
              <div class="table-box">
                <div class="table-body nopadding noborder">
                  <div class="table-row">
                    <div class="table-col col-order-left">
                      <span class="bold"><?php echo $text_order_id; ?></span> #<?php echo $invoice_no; ?>
                    </div>
                    <div class="table-col col-order-right">
                      <span class="bold"><?php echo $text_date_added; ?></span> <?php echo $date_added; ?>
                    </div>
                  </div>
                  <div class="table-row">
                    <?php if ($payment_method) { ?>
                    <div class="table-col col-order-left">
                      <span class="bold"><?php echo $text_payment_method; ?></span> <?php echo $payment_method; ?>
                    </div>
                    <?php } ?>
                    <?php if ($shipping_method) { ?>
                    <div class="table-col col-order-right">
                      <span class="bold"><?php echo $text_shipping_method; ?></span> <?php echo $shipping_method; ?>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="field-container">
              <div class="table-box">
                <div class="table-head">
                  <?php if ($shipping_address) { ?>
                  <div class="table-col col-payment"><?php echo $text_shipping_address; ?></div>
                  <?php } ?>
                  <div class="table-col col-shipping"><?php echo $text_comment; ?></div>
                </div>
                <div class="table-body">
                  <div class="table-row">
                    <?php if ($shipping_address) { ?>
                    <div class="table-col col-payment"><?php echo $shipping_address; ?></div>
                    <?php } ?>
                    <?php if ($comment) { ?>
                    <div class="table-col col-shipping"><?php echo $comment; ?></div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="field-container">
              <div class="table-box">
                <div class="table-head">
                  <div class="table-col col-order-name"><?php echo $column_name; ?></div>
                  <div class="table-col col-order-qty hidden-sm hidden-xs"><?php echo $column_quantity; ?></div>
                  <div class="table-col col-order-price hidden-xs"><?php echo $column_price; ?></div>
                  <div class="table-col col-order-total"><?php echo $column_total; ?></div>
                  <div class="table-col col-order-info-action">Action</div>
                </div>
                <div class="table-body">
                  <?php foreach ($products as $product) { ?>
                  <div class="table-row">
                    <div class="table-col col-order-name">
                      <a class="hvr-orange" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                      <?php if ($product['option']) { ?>
                      <div class="col-option">
                        <?php foreach ($product['option'] as $option) { ?>
                          <?php echo $option['value']; ?> <?php echo $option['name']; ?>
                        <?php } ?>
                      </div>
                      <?php } ?>
                      <div class="visible-xs visible-sm text-grey"><?php echo $product['quantity']; ?></div>
                      <div class="visible-xs text-grey"><?php echo $product['total']; ?></div>
                    </div>
                    <div class="table-col col-order-qty hidden-sm hidden-xs"><?php echo $product['quantity']; ?></div>
                    <div class="table-col col-order-price hidden-xs"><?php echo $product['price']; ?></div>
                    <div class="table-col col-order-total"><?php echo $product['total']; ?></div>
                    <div class="table-col col-order-info-action">
                    <?php if ($product['reorder']) { ?>
                      <a href="<?php echo $product['reorder']; ?>" data-toggle="tooltip" title="<?php echo $button_reorder; ?>" class="btn-slide green"><i class="fa fa-shopping-basket "></i></a>
                    <?php } ?>
                    <?php if ($product['return']) { ?>
                      <a href="<?php echo $product['return']; ?>" data-toggle="tooltip" title="<?php echo $button_return; ?>" class="btn-slide "><i class="fa fa-reply "></i></a>
                    <?php } ?>
                    </div>
                  </div>
                  <?php } ?>
                  <?php foreach ($vouchers as $voucher) { ?>
                  <div class="table-row">
                    <div class="table-col col-order-name"><?php echo $voucher['description']; ?>
                      <div class="visible-xs text-grey"><?php echo $voucher['amount']; ?></div>
                    </div>
                    <div class="table-col col-order-qty hidden-sm hidden-xs">1</div>
                    <div class="table-col col-order-price hidden-xs"><?php echo $voucher['amount']; ?></div>
                    <div class="table-col col-order-total"><?php echo $voucher['amount']; ?></div>
                    <div class="table-col col-order-info-action"></div>
                  </div>
                  <?php } ?>
                </div>
                <div class="table-foot">
                <?php foreach ($totals as $total) { ?>
                  <div class="table-row">
                    <div class="table-col table-foot-label col-order-subtotal"><?php echo $total['title']; ?></div>
                    <div class="table-col col-order-subtotal-nominal"><?php echo $total['text']; ?></div>
                  </div>
                <?php } ?>
                </div>
              </div>
            </div>
            <?php if ($histories) { ?>
            <div class="field-box wmargin">
              <div class="field-title"><?php echo $text_history; ?></div>
            </div>
            <div class="field-container">
              <div class="table-box">
                <div class="table-head">
                  <div class="table-col col-date hidden-xs"><?php echo $column_date_added; ?></div>
                  <div class="table-col col-order-status xs-pl0"><?php echo $column_status; ?></div>
                  <div class="table-col col-order-comment"><?php echo $column_comment; ?></div>
                </div>
                <div class="table-body">
                <?php if ($histories) { ?>
                <?php foreach ($histories as $history) { ?>
                  <div class="table-row">
                    <div class="table-col col-date hidden-xs"><?php echo $history['date_added']; ?></div>
                    <div class="table-col col-order-status xs-pl0">
                      <div class="visible-xs mb5"><?php echo $history['date_added']; ?></div>
                      <?php echo $history['status']; ?>
                    </div>
                    <div class="table-col col-order-comment"><?php echo $history['comment']; ?></div>
                  </div>
                <?php } ?>
                <?php } else { ?>
                  <div class="table-row">
                    <div class="table-col col-empty"><?php echo $text_no_results; ?></div>
                  </div>
                <?php } ?>
                </div>
              </div>
            </div>
            <?php } ?>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>