<?php echo $header; ?>
<!-- ACCOUNT PAGE -->
<div class="account-page">
	<div class="container">
	  <div class="row">
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <?php echo $column_left; ?>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <div class="account-content">
	      	<div class="field-box wmargin">
            <div class="account-info-point"><?php echo $text_total; ?></div>
          </div>
	        <div class="field-container">
	        	<div class="table-box">
              <div class="table-head">
                <div class="table-col col-date hidden-xs"><?php echo $column_date_added; ?></div>
                <div class="table-col col-point-desc xs-pl0"><?php echo $column_description; ?></div>
                <div class="table-col col-point"><?php echo $column_points; ?></div>
              </div>
              <div class="table-body">
            		<?php if ($rewards) { ?>
            		<?php foreach ($rewards  as $reward) { ?>
                <div class="table-row">
                  <div class="table-col col-date hidden-xs"><?php echo $reward['date_added']; ?></div>
                  <div class="table-col col-point-desc xs-pl0">
                    <div class="visible-xs mb5 text-grey"><?php echo $reward['date_added']; ?></div>
                    <?php if ($reward['order_id']) { ?>
                    <a class="hvr-orange" href="<?php echo $reward['href']; ?>"><?php echo $reward['description']; ?></a></div>
                		<?php } else { ?>
              			<?php echo $reward['description']; ?>
  	                <?php } ?>
                  <div class="table-col col-point"><?php echo $reward['points']; ?></div>
                </div>
            		<?php } ?>
            		<?php } else { ?>
	            	<div class="table-row">
	                <div class="table-col col-empty"><?php echo $text_empty; ?></div>
	              </div>
            		<?php } ?>
              </div>
            </div>
	        </div>
	        <?php if ($pagination): ?>
          <div class="pagination-box text-center">
            <?php echo $pagination; ?>
          </div>
	        <?php endif ?>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>