<?php echo $header; ?>
<!-- ACCOUNT PAGE -->

<div class="account-page">
  <div class="container">
    <div class="account-content">
      <div class="color-title"><span class="orange"><?php echo $heading_title[0]; ?></span> <span class="green"><?php echo $heading_title[1]; ?></span></div>
      <div class="field-desc mb20"><?php echo $text_account_already; ?></div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="customer_group_id" class="radio-field" value="1" checked="checked">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="account-content-left">
              <div class="field-title"><?php echo $text_your_details; ?></div>
              <div class="field-box wmargin">
                <div class="input-box <?php echo $fullname ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $entry_fullname; ?></div>
                  <input class="input-field-line" type="text" name="fullname" value="<?php echo $fullname; ?>">
                </div>
                <?php if ($error_fullname) { ?>
                <div class="text-danger"><?php echo $error_fullname; ?></div>
                <?php } ?>
              </div>
              <div class="field-box wmargin">
                <div class="input-box <?php echo $email ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $entry_email; ?></div>
                  <input class="input-field-line" type="text" name="email" value="<?php echo $email; ?>">
                </div>
                <?php if ($error_email) { ?>
                <div class="text-danger"><?php echo $error_email; ?></div>
                <?php } ?>
              </div>
              <div class="field-box wmargin">
                <div class="input-box <?php echo $telephone ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $entry_telephone; ?></div>
                  <input class="input-field-line" type="text" name="telephone" value="<?php echo $telephone; ?>">
                </div>
                <?php if ($error_telephone) { ?>
                <div class="text-danger"><?php echo $error_telephone; ?></div>
                <?php } ?>
              </div>
              <div class="field-box wmargin">
                <div class="input-box <?php echo $password ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $entry_password; ?></div>
                  <input class="input-field-line" type="password" name="password" value="<?php echo $password; ?>">
                </div>
                <?php if ($error_password) { ?>
                <div class="text-danger"><?php echo $error_password; ?></div>
                <?php } ?>
              </div>
              <div class="field-box wmargin">
                <div class="input-box <?php echo $confirm ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $entry_confirm; ?></div>
                  <input class="input-field-line" type="password" name="confirm" value="<?php echo $confirm; ?>">
                </div>
                <?php if ($error_confirm) { ?>
                <div class="text-danger"><?php echo $error_confirm; ?></div>
                <?php } ?>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="account-content-right">
              <div class="field-title"><?php echo $text_your_address; ?></div>
              <div class="field-box wmargin">
                <div class="input-box <?php echo $address_1 ? 'filled' : ''; ?>">
                  <div class="input-label"><?php echo $entry_address_1; ?></div>
                  <input class="input-field-line" type="text" name="address_1" value="<?php echo $address_1; ?>">
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="field-box mb20">
                    <div class="select-box">
                      <div class="select-label <?php echo $country_id ? 'filled' : ''; ?>"><?php echo $entry_country; ?></div>
                      <div class="select-box-chevron">
                        <select class="select-field-line <?php echo $country_id ? 'filled' : ''; ?>" name="country_id">
                          <option value="" selected disabled><?php echo $entry_country; ?></option>
                          <?php foreach ($countries as $country) { ?>
                          <option value="<?php echo $country['country_id']; ?>" <?php echo ($country['country_id'] == $country_id) ? 'selected' : ''; ?>><?php echo $country['name']; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <?php if ($error_country) { ?>
                    <div class="text-danger"><?php echo $error_country; ?></div>
                    <?php } ?>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="field-box mb20">
                    <div class="select-box">
                      <div class="select-label"><?php echo $entry_zone; ?></div>
                      <div class="select-box-chevron">
                        <select class="select-field-line" name="zone_id">
                          <option value="" selected disabled><?php echo $entry_zone; ?></option>
                        </select>
                      </div>
                    </div>
                    <?php if ($error_zone) { ?>
                    <div class="text-danger"><?php echo $error_zone; ?></div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="row mb10">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="field-box mb20">
                    <div class="select-box">
                      <div class="select-label"><?php echo $entry_city; ?></div>
                      <div class="select-box-chevron">
                        <select class="select-field-line" name="city_id">
                          <option value="" selected disabled><?php echo $entry_city; ?></option>
                        </select>
                      </div>
                    </div>
                    <?php if ($error_city) { ?>
                    <div class="text-danger"><?php echo $error_city; ?></div>
                    <?php } ?>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="field-box mb20">
                    <div class="input-box <?php echo $postcode ? 'filled' : ''; ?>">
                      <div class="input-label"><?php echo $entry_postcode; ?></div>
                      <input class="input-field-line" type="text" name="postcode" value="<?php echo $postcode; ?>">
                    </div>
                    <?php if ($error_postcode) { ?>
                    <div class="text-danger"><?php echo $error_postcode; ?></div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                  <!-- <div class="field-box mb20">
                    <div id="recaptcha1" class="g-recaptcha" style="transform:scale(0.95);-webkit-transform:scale(0.95);transform-origin:0 0;-webkit-transform-origin:0
                                0;"></div>
                    <input type="hidden" class="hiddenRecaptcha " name="hiddenRecaptcha1 " id="hiddenRecaptcha1" value="">
                  </div> -->
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12 ">
                  <div class="field-box text-right">
                    <button class="btn-slide green" type="submit"><?php echo $button_continue; ?></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>


    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('select[name=\'country_id\']').trigger('change');
  });
</script>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/account/country&country_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      if (json['postcode_required'] == '1') {
        $('input[name=\'postcode\']').parent().parent().addClass('required');
      } else {
        $('input[name=\'postcode\']').parent().parent().removeClass('required');
      }

      html = '<option value="" selected disabled><?php echo $entry_zone; ?></option>';

      if (json['zone'] && json['zone'] != '') {
        for (i = 0; i < json['zone'].length; i++) {
          html += '<option value="' + json['zone'][i]['zone_id'] + '"';

          if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['zone'][i]['name'] + '</option>';
        }
      } else {
        html += '<option value="0" selected disabled><?php echo $entry_zone; ?></option>';
      }

      $('select[name=\'zone_id\']').html(html);
      $('select[name=\'zone_id\']').trigger('change');
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

/********** zone ************/
$('select[name=\'zone_id\']').on('change', function() {
  var zone_id = this.value;
  
  // if zone_id exist
  if (zone_id == '') {
    <?php if(isset($zone_id) && $zone_id != '') { ?>
      zone_id = <?php echo $zone_id; ?>;
    <?php } ?>
  }

  $.ajax({
    url: 'index.php?route=account/account/zone&zone_id=' + zone_id,
    dataType: 'json',
    beforeSend: function() {
      $('select[name=\'zone_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      html = '<option value="" selected disabled><?php echo $entry_city; ?></option>';

      if (json['city'] && json['city'] != '') {
        for (i = 0; i < json['city'].length; i++) {
          html += '<option value="' + json['city'][i]['city_id'] + '"';

          if (json['city'][i]['city_id'] == '<?php echo $city_id; ?>') {
            html += ' selected="selected"';
            }

            html += '>' + json['city'][i]['name'] + '</option>';
        }
      } else {
        html += '<option value="0" selected disabled><?php echo $entry_city; ?></option>';
      }

      $('select[name=\'city_id\']').html(html);
      $('select[name=\'city_id\']').trigger('change');
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});


//--></script>
<!-- END OF ACCOUNT PAGE -->
<?php echo $footer; ?>