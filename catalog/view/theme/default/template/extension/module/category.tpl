<!-- <div class="list-group">
  <?php foreach ($categories as $category) { ?>
  <?php if ($category['category_id'] == $category_id) { ?>
  <a href="<?php echo $category['href']; ?>" class="list-group-item active"><?php echo $category['name']; ?></a>
  <?php if ($category['children']) { ?>
  <?php foreach ($category['children'] as $child) { ?>
  <?php if ($child['category_id'] == $child_id) { ?>
  <a href="<?php echo $child['href']; ?>" class="list-group-item active">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
  <?php } else { ?>
  <a href="<?php echo $child['href']; ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
  <?php } ?>
  <?php } ?>
  <?php } ?>
  <?php } else { ?>
  <a href="<?php echo $category['href']; ?>" class="list-group-item"><?php echo $category['name']; ?></a>
  <?php } ?>
  <?php } ?>
</div> -->



<!-- CATEGORY LIST -->

<div class="category-list">
  <div class="container">
    <div class="cl-title"><?php echo $text_lets_shopping[0]; ?>
      <span class="bold"><?php echo $text_lets_shopping[1]; ?></span>
    </div>
  </div>
  <?php if ($category_id != 10) { ?>
  <div class="cl-slider-container">
    <div class="container">
      <div class="cl-slider owl-theme owl-carousel">
        <?php foreach ($categories as $category) { ?>
        <div class="item">
          <a class="link-line <?php echo $category['category_id'] == $category_id ? ' active' : ''; ?>" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
  <div class="visible-xs">
    <div class="container">
      <div class="select-box-caret">
        <select class="select-field radius" onchange="change_category($(this));">
          <?php foreach ($categories as $category) { ?>
          <option data-href="<?php echo $category['href']; ?>" <?php echo $category['category_id'] == $category_id ? 'selected' : ''; ?>><?php echo $category['name']; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
  </div>
  <?php } ?>
</div>

<script type="text/javascript">
  function change_category(select) {
    location = select.find('option:selected').attr('data-href');
  }
  $(document).ready(function () {
    $('.cl-slider').owlCarousel({
      margin: 20,
      loop: false,
      dots: false,
      autoWidth: true,
      nav: true,
      navText: ["<i class='owl-btn owl-btn-prev fa fa-angle-left'></i>", "<i class='owl-btn owl-btn-next fa fa-angle-right'></i>"],
      responsive:
        {
          0: {
            items: 1,
            autoWidth: false,
          },
          768: {
            items: 8,
          },
          950: {
            items: 8,
          },
          1025: {
            items: 8,
          }
        }
    })
  });
</script>
<!-- END OF CATEGORY LIST -->