<?php if(!empty($banners[0]) && !empty($banners[1])) { ?>
<div class="mini-banner">

  <div class="mb-item">

    <a href="<?php echo ($banners[0]['link']) ? $banners[0]['link'] : 'javascript:;'; ?>" class="mb-link">

      <img src="<?php echo $banners[0]['image']; ?>">

    </a>

  </div>

  <div class="mb-item mb-right">

    <a href="<?php echo ($banners[1]['link']) ? $banners[1]['link'] : 'javascript:;'; ?>" class="mb-link">

      <img src="<?php echo $banners[1]['image']; ?>">

    </a>

  </div>

</div>
<?php } ?>
