<div class="main-product">

<?php if (!empty($categories)) { ?>

  <div class="container">

  <?php foreach ($categories as $category_id => $category) { ?>

    <div class="mp-box">

      <!-- <div class="mp-box-title">Pembeli produk ini juga membeli</div> -->

      <div class="row">

<!--         <div class="col-md-3 col-sm-3 col-xs-6">

          <div class="mp-banner-container">

            <div class="mp-banner" style="background-image: url(<?php echo $category['banner']; ?>)">

              <div class="mp-banner-overlay red">

                <div class="mpb-title"><?php echo $category['name']; ?></div>

                <div class="mpb-desc">

                  <?php echo strip_tags($category['desc']); ?>

                </div>

                <div class="mpb-btn">

                  <a href="<?php echo $category['href']; ?>" class="btn-slide green"><?php echo $button_view_all; ?></a>

                </div>

              </div>

            </div>

          </div>

        </div> -->

        <div class="col-md-12 col-sm-12 col-xs-12">

          <div class="mp-slider-container">

            <div class="mp-slider owl-theme owl-carousel">

            <?php foreach ($category['products'] as $product) { ?>

              <div class="item">

                <div class="product-container big-product <?php echo (!$product['quantity']) ? 'out-of-stock' : ''; ?>">

                  <a href="<?php echo $product['href']; ?>" class="product-link">

                    <div class="product-box <?php echo ($product['discount']) ? 'special-price' : ''; ?>">

                      <div class="product-img <?php echo ($product['noimage']) ? 'no-image' : ''; ?>">

                        <img src="<?php echo $product['thumb']; ?>">

                        <?php if (!$product['quantity']) { ?>

                          <div class="stock-info">

                            <div>Stok Habis</div>

                          </div>

                        <?php } ?>

                      </div>

                      <div class="product-name"><?php echo $product['name']; ?></div>

                      <?php if ($product['special']) { ?>

                      <div class="product-price-wline"><?php echo $product['price']; ?></div>

                      <div class="product-price"><?php echo $product['special']; ?></div>

                      <?php } else { ?>

                      <div class="product-price"><?php echo $product['price']; ?></div>

                      <?php } ?>

                      <!-- <div class="product-weight"><?php echo $product['caption']; ?></div> -->

                    </div>

                  </a>
                  <?php if (!empty($product['label'])) { ?>
                  <div class="product-label"><?php echo $product['label']; ?></div>
                  <?php } ?>
                  <?php if (!empty($product['label_2'])) { ?>
                  <div class="product-label-2"><?php echo $product['label_2']; ?></div>
                  <?php } ?>

                  <?php if ($product['discount']) { ?>

                  <div class="product-label"><?php echo $product['discount']; ?></div>

                  <?php } ?>

                  <div class="product-btn">

                    <a class="btn-slide green <?php echo (!$product['quantity']) ? 'disabled' : ''; ?>" href="javascript:;" <?php if ($product['quantity']) { ?>onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"<?php } ?>>

                      <img src="image/assets/home/basket.png">

                      <span><?php echo $button_cart; ?></span>

                    </a>

                  </div>

                </div>

              </div>

            <?php } ?>

            </div>

          </div>

        </div>

      </div>

    </div>

  <?php } ?>

  </div>

<?php } ?>

</div>



<script>

  $(document).ready(function () {

    $('.mp-slider').owlCarousel({

      loop: false,

      dots: false,

      nav: true,

      navText: ["<i class='owl-btn owl-btn-prev fa fa-angle-left'></i>", "<i class='owl-btn owl-btn-next fa fa-angle-right'></i>"],

      autoplay: true,

      autoplayTimeout: 5500,

      responsive: {

        0: {

          items: 1,

          margin: 0,

        },

        768: {

          items: 3,

          margin: 7,

        },

        995: {

          items: 3,

          margin: 10,

        },

        1025: {

          items: 4,

          margin: 15,

        }

      }

    });



    if ($(window).width() < 768) {

      var url = $('.mmc-banner-link').attr('href');

      $('.mmc-banner-link-mobile').attr('href', url);

    }

  });

</script>
