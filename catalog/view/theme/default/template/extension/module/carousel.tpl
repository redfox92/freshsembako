<div class="main-banner ms-nav-parent">
    <div class="main-slider owl-theme owl-carousel">
    <?php foreach ($banners as $key => $banner) { ?>
        <div class="item" data-item="0<?php echo $key + 1; ?>">
            <a href="<?php echo ($banner['link']) ? $banner['link'] : 'javascript:;'; ?>">
              <img src="<?php echo $banner['image']; ?>" />
            </a>
        </div>
    <?php }?>
    </div>
    <div class="ms-index">
        <div class="msi-item">
            <span class="msi-active">01</span>
            <span>0<?php echo count($banners); ?></span>
        </div>
        <div class="msi-item">
            <div class="msi-line"></div>
        </div>
    </div>
    <?php if (count($banners) > 1): ?>
    <div class="ms-nav">
        <div class="msn-item">
            <div class="msn-left">
                <div class='outer-shadow'></div>
                <div class='inner-shadow'></div>
                <div class='hold left'>
                    <div class='fill'></div>
                </div>
                <div class='hold right'>
                    <div class='fill'></div>
                </div>
                <i class="fa fa-arrow-left"></i>
            </div>
        </div>
        <div class="msn-item">
            <div class="msn-right load">
                <div class='outer-shadow'></div>
                <div class='inner-shadow'></div>
                <div class='hold left'>
                    <div class='fill'></div>
                </div>
                <div class='hold right'>
                    <div class='fill'></div>
                </div>
                <i class="fa fa-arrow-right"></i>
            </div>
        </div>
    </div>
    <?php endif ?>
</div>

<script type="text/javascript">
    $(document).ready(function () {
      $('.main-slider').owlCarousel({
        loop: ($('.main-slider .item').length <= 1) ? false : true,
        dots: false,
        nav: false,
        items: 1,
        autoplay: true,
        autoplayTimeout: 5500,
      });

      $('.owl-carousel').on('translated.owl.carousel', function (event) {
        $(this).parents('.ms-nav-parent').find('.msn-right').addClass('load');

        var idx = $(this).find('.owl-item.active .item').data('item');
        $('.msi-active').text(idx);
      })

      $('.owl-carousel').on('translate.owl.carousel', function (event) {
        $(this).parents('.ms-nav-parent').find('.msn-right').removeClass('load');
      })

      $('body').delegate('.msn-right', 'click', function(){
        $(this).parents('.ms-nav-parent').find('.owl-carousel').trigger('stop.owl.autoplay').trigger('play.owl.autoplay');
        $(this).parents('.ms-nav-parent').find('.owl-carousel').trigger('next.owl.carousel', [300]);
      });

      $('body').delegate('.msn-left', 'click', function(){
        $(this).parents('.ms-nav-parent').find('.owl-carousel').trigger('stop.owl.autoplay').trigger('play.owl.autoplay');
        $(this).parents('.ms-nav-parent').find('.owl-carousel').trigger('prev.owl.carousel', [300]);
      });
    });
</script>
