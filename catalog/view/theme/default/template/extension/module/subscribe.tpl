<div class="main-subscribe" style="background-image: url(image/assets/home/sayur-background.jpg)">
  <div class="main-subscribe-overlay">
    <div class="container">
      <div class="ms-title"><?php echo $text_title; ?></div>
      <div class="ms-desc"><?php echo $text_description; ?></div>
      <div>
        <form id="form-subscribe" action="<?php echo $action; ?>" method="post">
          <div class="field-box ms-input">
            <input type="text" class="input-field white radius" name="email" placeholder="<?php echo $entry_email; ?>" autocomplete="off">
            <div class="field-error"><!-- isi error --></div>
            <!-- class field-error cma di pake di dalem field-box, dan ini khusus tooltip, kalo mau nampilin teks error biasa, pake class text-error -->
          </div>
          <div class="ms-btn">
            <!-- <a class="btn-slide white"> -->
              <button type="submit" class="btn-slide white"><?php echo $button_subscribe; ?></button>
            <!-- </a> -->
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#form-subscribe').submit(function( event ) {
            $('#form-subscribe').find('.field-error').removeClass('show');
            $.ajax({
                url: $(event.target).attr('action'),
                type: $(event.target).attr('method'),
                dataType: 'json',
                data: $(event.target).serialize(),
                success: function(response) {
                    if (response.error_email) {
                        $('#form-subscribe .field-box.ms-input .field-error').text(response.error_email).addClass('show');
                    }

                    if (response.success) {
                        $('#form-subscribe .field-box.ms-input .field-error').text(response.success).addClass('show');
                        $('#form-subscribe .field-box.ms-input .input-field').val('');  
                    }
                    setTimeout(function() {
                      $('#form-subscribe .field-box.ms-input .field-error').removeClass('show');
                    }, 5000);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
            event.preventDefault();
        });
    });
</script>