<div class="main-mini-category">

<div class="mmc-content">

  <div class="container">
    <?php if(!empty($banner['image'])) { ?>
    <div class="mmc-banner">
      <!-- attr href di class mmc-banner-link-mobile jgn diubah -->
      <a href="javascript:;" class="mmc-banner-link-mobile">
        <img src="<?php echo $banner['image']; ?>">
      </a>
      <?php if (!empty($banner['link'])) { ?>
      <div class="mmc-banner-btn">
        <a class="btn-slide green mmc-banner-link" href="<?php echo ($banner['link']) ? $banner['link'] : 'javascript:;'; ?>"><?php echo $button_cart; ?></a>
      </div>
      <?php } ?>
    </div>
    <?php } ?>

    <?php if ($categories) { ?>

    <div class="mmc-slider-container">

      <div class="mmc-slider">

        <?php foreach ($categories as $category) { ?>

        <div class="item">

          <a class="mmc-link" href="<?php echo $category['href']; ?>">

            <div class="mmc-box" style="background-image: url(<?php echo $category['image']; ?>)">

              <div class="mmc-name orange"><?php echo $category['name']; ?></div>

            </div>

          </a>

        </div>

        <?php } ?>

      </div>

    </div>

    <?php } ?>

  </div>

</div>

</div>



<script>

$(document).ready(function () {

  // $('.mmc-slider').owlCarousel({

  //   loop: false,

  //   dots: true,

  //   nav: false,

  //   margin: 20,

  //   items: 4,

  //   autoplay: true,

  //   autoplayTimeout: 3500,

  //   responsive: {

  //     0: {

  //       items: 2,

  //       margin: 5,

  //     },

  //     430: {

  //       items: 3,

  //       margin: 10,

  //     },

  //     1024: {

  //       items: 4

  //     }

  //   }

  // });



  if ($(window).width() < 768) {

    var url = $('.mmc-banner-link').attr('href');

    $('.mmc-banner-link-mobile').attr('href', url);

  }

});

</script>