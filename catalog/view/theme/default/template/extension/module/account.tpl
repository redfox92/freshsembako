<div class="account-left-side">
  <div class="account-side-menu-button"><button class="btn-slide white-wborder w100">Account</button></div>
  <div class="account-side-menu">
    <div class="ap-menu-title">
      <div class="apm-title">Menu</div>
      <div class="apm-close">
        <a href="#" class="apm-close-btn">
          <img src="image/assets/home/cancel.png">
        </a>
      </div>
    </div>
    <?php if ($account) { ?>
    <div class="ap-side-menu">
      <a class="btn-line <?php echo ($url == $account || $url == $edit) ? 'active' : ''; ?>" href="<?php echo $account ?>" id="account-account"><?php echo $text_account; ?></a>
    </div>
    <?php } ?>
    <?php if ($password) { ?>
    <div class="ap-side-menu">
      <a class="btn-line <?php echo ($url == $password) ? 'active' : ''; ?>" href="<?php echo $password ?>" id="account-password"><?php echo $text_password; ?></a>
    </div>
    <?php } ?>
    <?php if ($address) { ?>
    <div class="ap-side-menu">
      <a class="btn-line <?php echo ($url == $address || $route == 'account/address/add' || $route == 'account/address/edit') ? 'active' : ''; ?>" href="<?php echo $address ?>" id="account-address"><?php echo $text_address; ?></a>
    </div>
    <?php } ?>
    <?php if ($order) { ?>
    <div class="ap-side-menu">
      <a class="btn-line <?php echo ($url == $order || $route == 'account/order/info') ? 'active' : ''; ?>" href="<?php echo $order ?>" id="account-order"><?php echo $text_order; ?></a>
    </div>
    <?php } ?>
    <?php if ($payment_confirmation) { ?>
    <div class="ap-side-menu">
      <a class="btn-line <?php echo ($url == $payment_confirmation) ? 'active' : ''; ?>" href="<?php echo $payment_confirmation ?>" id="account-pconfirm"><?php echo $text_payment_confirmation; ?></a>
    </div>
    <?php } ?>
    <?php if ($reward) { ?>
    <div class="ap-side-menu">
      <a class="btn-line <?php echo ($url == $reward) ? 'active' : ''; ?>" href="<?php echo $reward ?>" id="account-point"><?php echo $text_reward; ?></a>
    </div>
    <?php } ?>
    <?php if ($return) { ?>
    <div class="ap-side-menu">
      <a class="btn-line <?php echo ($url == $return || $route == 'account/return/info' || $route == 'account/return/add') ? 'active' : ''; ?>" href="<?php echo $return ?>" id="account-returns"><?php echo $text_return; ?></a>
    </div>
    <?php } ?>
  </div>
</div>