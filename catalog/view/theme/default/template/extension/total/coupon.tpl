<div class="field-box wmargin input-box-radius" id="cart-promo" style="<?php echo ($coupon) ? 'display: none;' : ''; ?>">
  <form action="#">
    <input name="coupon" type="text" class="input-field radius" placeholder="<?php echo $entry_coupon; ?>" value="<?php echo $coupon; ?>">
    <button class="btn-slide" id="btn-apply-promo" type="button"><?php echo $button_coupon; ?></button>
  </form>
</div>
<div class="box-error">
	
</div>

<script type="text/javascript"><!--
$('#btn-apply-promo').on('click', function() {
	$.ajax({
		url: 'index.php?route=extension/total/coupon/coupon',
		type: 'post',
		data: 'coupon=' + encodeURIComponent($('input[name=\'coupon\']').val()),
		dataType: 'json',
		beforeSend: function() {
			$('#btn-apply-promo').button('loading');
		},
		complete: function() {
			$('#btn-apply-promo').button('reset');
		},
		success: function(json) {
			$('.box-error .alert').remove();

			if (json['error']) {
				$('#btn-apply-promo').parents('.field-box').siblings('.box-error').append('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}

			if (json['redirect']) {
				location = json['redirect'];
			}
		}
	});
});
//--></script>