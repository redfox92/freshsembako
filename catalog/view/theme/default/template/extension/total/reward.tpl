<div class="field-box wmargin">
	<div class="check-box">
	  <label class="check-label lp04" for="input-reward" style="font-size: 12px; color: #53d800;"><?php echo $heading_title; ?>
	    <input type="checkbox" class="check-field" value="<?php echo $points_total; ?>" name="reward" id="input-reward" <?php echo $confirm ? 'checked' : ''; ?>>
	    <span class="check-mark"></span>
	  </label>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#input-reward').on('change', function(event) {
			event.preventDefault();
			
			if ($(this).prop('checked')) {
				$.ajax({
					url: 'index.php?route=extension/total/reward/reward',
					type: 'post',
					data: 'reward=' + encodeURIComponent($('input[name=\'reward\']').val()),
					dataType: 'json',
					success: function(json) {
						$('.alert').remove();

						if (json['error']) {
							$('.box-error').append('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>' + json['error'] + '</div>');

							$('html, body').animate({ scrollTop: 0 }, 'slow');
						}

						if (json['redirect']) {
							location = json['redirect'];
						}
					}
				});
			} else {
				$.ajax({
					url: 'index.php?route=extension/total/reward/remove',
					type: 'post',
					dataType: 'json',
					success: function(json) {
						$('.alert').remove();

						if (json['error']) {
							$('.box-error').append('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>' + json['error'] + '</div>');

							$('html, body').animate({ scrollTop: 0 }, 'slow');
						}

						if (json['redirect']) {
							location = json['redirect'];
						}
					}
				});
			}

		});
	});
</script>