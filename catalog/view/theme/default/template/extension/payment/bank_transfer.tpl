<div class="field-box wmargin">
	<div class="field-title"><?php echo $text_instruction; ?></div>
	<div class="field-desc mb10"><?php echo $text_description; ?></div>
	<div class="yellow-box text-center">
		<!-- <p><?php //echo $bank; ?></p>
		<p><?php //echo $text_payment; ?></p> -->
		<p><?php echo $text_selection; ?></p>
		<p><strong><?php echo $bank['name']; ?></strong></p>
		<p><img src="<?php echo $bank['image']; ?>" style="width: 150px;" /></p>
		<p><?php echo $bank['term']; ?></p>
		<p><?php echo $text_confirm; ?></p>
	</div>
	<div class="field-desc mb10"><?php echo $text_payment; ?></div>
</div>
<div class="field-box wmargin text-right">
	<button id="button-confirm" data-loading-text="<?php echo $text_loading; ?>" class="btn-slide green"><?php echo $button_confirm; ?></button>
</div>
<script type="text/javascript"><!--
$('#button-confirm').on('click', function() {
	$.ajax({
		type: 'get',
		url: 'index.php?route=extension/payment/bank_transfer/confirm',
		cache: false,
		beforeSend: function() {
			$('#button-confirm').button('loading');
		},
		complete: function() {
			$('#button-confirm').button('reset');
		},
		success: function() {
			location = '<?php echo $continue; ?>';
		}
	});
});
//--></script>
