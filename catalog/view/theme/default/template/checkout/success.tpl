<?php echo $header; ?>

<div class="account-page checkout-success">
  <div class="container">
    <div class="account-content">
      <div class="field-container">

        <div class="color-title"><?php echo $heading_title; ?></div>
        <div class="row">
          <div class="col-md-6">
            <div class="well text-center">
              <p>No Invoice: <span class="bold">#<?php echo $order['invoice_no']; ?></span></p>
              <div class="payment-method">
                <p><?php echo $order['payment_method']['name']; ?></p>
                <img src="<?php echo $order['payment_method']['image'] ?>">
                <p><?php echo $order['payment_method']['user_name']; ?> - <input type="text" id="user_account" readonly value="<?php echo $order['payment_method']['user_account']; ?>"></p>
                <a href="javascript:;" class="btn-slide green" id="btn-copy">Salin</a>
              </div>
            </div>
          </div>
        </div>
      	<div class="field-desc mb20">
          <?php foreach ($text_message as $message) {
            echo $message;
          } ?>
        </div>
      	<div class="field-box wmargin text-right">
          <a href="<?php echo $continue; ?>" class="btn-slide green" type="button"><?php echo $button_continue; ?></a>
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
  function resizable (el, factor) {
    var int = Number(factor) || 7.7;
    function resize() {el.style.width = ((el.value.length+1) * int) + 'px'}
    var e = 'keyup,keypress,focus,blur,change'.split(',');
    for (var i in e) el.addEventListener(e[i],resize,false);
    resize();
  }
  $(document).ready(function() {
    resizable(document.getElementById('user_account'),7.5);
    $('#btn-copy').on('click', function(event) {
      event.preventDefault();
      
      /* Get the text field */
      var copyText = document.getElementById("user_account");

      /* Select the text field */
      copyText.select();

      /* Copy the text inside the text field */
      document.execCommand("copy");
    });
  });
</script>
<?php echo $footer; ?>