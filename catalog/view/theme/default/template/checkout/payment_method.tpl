<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<div class="field-container">
<?php if ($payment_methods) { ?>
<?php foreach ($payment_methods as $key => $payment_method) { ?>
  <div class="field-box wmargin">
    <div class="radio-box">
      <label class="radio-label" for="radio-payment-<?php echo $key; ?>">
        <div class="radio-label-part">
          <div class="radio-label-logo">
            <img src="<?php echo $payment_method['image']; ?>" />
          </div>
          <div class="radio-label-desc">
            <div class="radio-label-desc-name"><?php echo $payment_method['title']; ?></div>
            <div class="field-desc">
              <?php echo $payment_method['desc']; ?>
              <!-- PT. Go Online Solusi -
              <span class="bold">122 123 2213</span> -->
            </div>
          </div>
        </div>
        <input type="radio" class="radio-field" value="<?php echo $payment_method['code']; ?>" name="payment_method" id="radio-payment-<?php echo $key; ?>" <?php if ($payment_method['code'] == $code || !$code) { $code = $payment_method['code']; ?> checked <?php } ?>>
        <span class="radio-mark middle"></span>
      </label>
    </div>
  </div>
<?php } ?>
<?php } ?>
  <div class="field-box wmargin">
    <div class="textarea-box <?php echo !empty($comment) ? 'filled' : ''; ?>">
      <div class="textarea-label"><?php echo $text_comments; ?></div>
      <textarea class="textarea-field-line" name="comment" rows="4"><?php echo $comment; ?></textarea>
    </div>
  </div>
<?php if ($text_agree) { ?>
  <div class="field-box wmargin">
    <div class="check-box">
      <label class="check-label lp04" for="agree-payment"><?php echo $text_agree; ?>
        <input type="checkbox" class="check-field" value="1" name="agree" id="agree-payment" <?php echo $agree ? 'checked' : ''; ?>>
        <span class="check-mark"></span>
      </label>
    </div>
  </div>
<?php } ?>
  <div class="field-box wmargin text-right">
    <button class="btn-slide btn-back" data-loading-text="<?php echo $text_loading; ?>" style="float: left;"><?php echo $button_back; ?></button>
    <button class="btn-slide green" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" style="float: right;"><?php echo $button_continue; ?></button>
  </div>
</div>