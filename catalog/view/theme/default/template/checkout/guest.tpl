<!-- guest.tpl -->
<div id="guest-content">
    <div class="field-container">
      <div class="field-title"><?php echo $text_your_details; ?></div>
      <input type="hidden" name="customer_group_id" value="default" />
      <div class="field-box wmargin">
        <div class="input-box">
          <div class="input-label"><?php echo $entry_fullname; ?></div>
          <input id="input-payment-fullname" class="input-field-line" type="text" name="fullname" value="">
        </div>
      </div>
      <div class="field-box wmargin">
        <div class="input-box">
          <div class="input-label"><?php echo $entry_email; ?></div>
          <input id="input-payment-email" class="input-field-line" type="text" name="email" value="">
        </div>
      </div>
      <div class="field-box wmargin">
        <div class="input-box">
          <div class="input-label"><?php echo $entry_telephone; ?></div>
          <input id="input-payment-telephone" class="input-field-line" type="text" name="telephone" value="">
        </div>
      </div>
    </div>
    <div class="field-container">
      <div class="field-title"><?php echo $text_your_address; ?></div>
      <input type="hidden" name="company" value="" />
      <input type="hidden" name="address_2" value="" />
      <div class="field-box wmargin">
        <div class="input-box">
          <div class="input-label"><?php echo $entry_address_1; ?></div>
          <input id="input-payment-address-1" class="input-field-line" type="text" name="address_1" value="">
        </div>
      </div>
      <!-- <div class="field-box wmargin">
        <div class="input-box">
          <div class="input-label">Phone</div>
          <input class="input-field-line" type="text">
        </div>
      </div> -->
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="field-box mb20">
            <div class="select-box">
              <div class="select-label"><?php echo $entry_country; ?></div>
              <div class="select-box-chevron">
                <select id="input-payment-country" class="select-field-line" name="country_id">
                  <option value="" selected disabled><?php echo $entry_country; ?></option>
                  <?php foreach ($countries as $country) { ?>
                  <option value="<?php echo $country['country_id']; ?>" <?php echo ($country['country_id'] == $country_id) ? 'selected' : ''; ?>><?php echo $country['name']; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="field-box mb20">
            <div class="select-box">
              <div class="select-label"><?php echo $entry_zone; ?></div>
              <div class="select-box-chevron">
                <select id="input-payment-zone" class="select-field-line" name="zone_id">
                  <option value="" selected disabled><?php echo $entry_zone; ?></option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="field-box mb20">
            <div class="select-box">
              <div class="select-label"><?php echo $entry_city; ?></div>
              <div class="select-box-chevron">
                <select id="input-payment-city" class="select-field-line" name="city_id">
                  <option value="" selected disabled><?php echo $entry_city; ?></option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="field-box mb20">
            <div class="input-box">
              <div class="input-label"><?php echo $entry_postcode; ?></div>
              <input id="input-payment-postcode" class="input-field-line" type="text" name="postcode" value="">
            </div>
          </div>
        </div>
      </div>
        <?php if ($shipping_required) { ?>
          <div class="field-box wmargin hidden">
            <div class="check-box">
              <label class="check-label lp04" for="shipping-required"><?php echo $entry_shipping; ?>
                <input type="checkbox" class="check-field" value="1" name="shipping_address" checked id="shipping-required" <?php echo $shipping_address ? 'checked' : ''; ?>>
                <span class="check-mark"></span>
              </label>
            </div>
          </div>
        <?php } ?>
      <div class="field-box wmargin text-right">
        <button class="btn-slide btn-back" data-loading-text="<?php echo $text_loading; ?>" style="float: left;"><?php echo $button_back; ?></button>
        <button id="button-guest" data-loading-text="<?php echo $text_loading; ?>" class="btn-slide green"><?php echo $button_continue; ?></button>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#guest-content select[name=\'country_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('#guest-content select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      if (json['postcode_required'] == '1') {
        $('#guest-content input[name=\'postcode\']').parent().parent().addClass('required');
      } else {
        $('#guest-content input[name=\'postcode\']').parent().parent().removeClass('required');
      }

      html = '<option value="" disabled><?php echo $entry_zone; ?></option>';

      if (json['zone'] && json['zone'] != '') {
        for (i = 0; i < json['zone'].length; i++) {
          html += '<option value="' + json['zone'][i]['zone_id'] + '"';

          if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['zone'][i]['name'] + '</option>';
        }
      } else {
        html += '<option value="0" selected="selected" disabled><?php echo $entry_zone; ?></option>';
      }

      $('#guest-content select[name=\'zone_id\']').html(html);
      $('#guest-content select[name=\'zone_id\']').trigger('change');
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('#guest-content select[name=\'country_id\']').trigger('change');

/********** zone ***********/
$('#guest-content select[name=\'zone_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=checkout/checkout/zone&zone_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('#guest-content select[name=\'zone_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      
      html = '<option value="" disabled selected><?php echo $entry_city; ?></option>';

      if (json['city'] && json['city'] != '') {
        for (i = 0; i < json['city'].length; i++) {
          html += '<option value="' + json['city'][i]['city_id'] + '"';

          if (json['city'][i]['city_id'] == '<?php echo $city_id; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['city'][i]['name'] + '</option>';
        }
      } else {
        html += '<option value="0" selected="selected" disabled><?php echo $entry_city; ?></option>';
      }

      $('#guest-content select[name=\'city_id\']').html(html);
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

//--></script>
<!-- end of guest.tpl -->