<!-- register.tpl -->
<div id="register-content">
    <div class="field-container">
      <div class="field-title"><?php echo $text_your_details; ?></div>
      <input type="hidden" name="customer_group_id" value="default" />
      <div class="field-box wmargin">
        <div class="input-box">
          <div class="input-label"><?php echo $entry_fullname; ?></div>
          <input class="input-field-line" type="text" id="input-payment-fullname" name="fullname" value="">
        </div>
      </div>
      <div class="field-box wmargin">
        <div class="input-box">
          <div class="input-label"><?php echo $entry_email; ?></div>
          <input class="input-field-line" type="text" id="input-payment-email" name="email" value="">
        </div>
      </div>
      <div class="field-box wmargin">
        <div class="input-box">
          <div class="input-label"><?php echo $entry_telephone; ?></div>
          <input class="input-field-line" type="text" id="input-payment-telephone" name="telephone" value="">
        </div>
      </div>
    </div>
    <div class="field-container">
      <div class="field-title"><?php echo $text_your_password; ?></div>
      <div class="field-box wmargin">
        <div class="input-box">
          <div class="input-label"><?php echo $entry_password; ?></div>
          <input class="input-field-line" type="password" id="input-payment-password" name="password" value="">
        </div>
      </div>
      <div class="field-box wmargin">
        <div class="input-box">
          <div class="input-label"><?php echo $entry_confirm; ?></div>
          <input class="input-field-line" type="password" id="input-payment-confirm" name="confirm" value="">
        </div>
      </div>
    </div>
    <div class="field-container">
      <div class="field-title"><?php echo $text_your_address; ?></div>
      <!-- ini kalo addressny mau pake textarea, tp prefer input sih, lebih rapi, karena desainnya cma line, jd ngegantung klo textarea -->
      <!-- <div class="field-box wmargin">
        <div class="textarea-box">
          <div class="textarea-label short">Address</div>
          <textarea class="textarea-field-line" rows="2"></textarea>
        </div>
      </div> -->
      <div class="field-box wmargin">
        <div class="input-box">
          <div class="input-label"><?php echo $entry_address_1; ?></div>
          <input class="input-field-line" type="text" id="input-payment-address-1" name="address_1" value="">
        </div>
      </div>
      <!-- <div class="field-box wmargin">
        <div class="input-box">
          <div class="input-label">Phone</div>
          <input class="input-field-line" type="text">
        </div>
      </div> -->
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="field-box mb20">
            <div class="select-box">
              <div class="select-label"><?php echo $entry_country; ?></div>
              <div class="select-box-chevron">
                <select class="select-field-line" name="country_id" id="input-payment-country">
                  <option value="" selected disabled><?php echo $entry_country; ?></option>
                  <?php foreach ($countries as $country) { ?>
                  <option value="<?php echo $country['country_id']; ?>" <?php echo ($country['country_id'] == $country_id) ? 'selected' : ''; ?>><?php echo $country['name']; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="field-box mb20">
            <div class="select-box">
              <div class="select-label"><?php echo $entry_zone; ?></div>
              <div class="select-box-chevron">
                <select class="select-field-line" name="zone_id" id="input-payment-zone">
                  <option value="" selected disabled><?php echo $entry_zone; ?></option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="field-box mb20">
            <div class="select-box">
              <div class="select-label"><?php echo $entry_city; ?></div>
              <div class="select-box-chevron">
                <select class="select-field-line" name="city_id" id="input-payment-city">
                  <option value="" selected disabled><?php echo $entry_city; ?></option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="field-box mb20">
            <div class="input-box">
              <div class="input-label"><?php echo $entry_postcode; ?></div>
              <input class="input-field-line" type="text" name="postcode" value="<?php echo $postcode; ?>">
            </div>
          </div>
        </div>
      </div>
<?php if ($shipping_required) { ?>
      <div class="field-box wmargin hidden">
        <div class="check-box">
          <label class="check-label lp04" for="same_address">
            <?php echo $entry_shipping; ?>
            <input type="checkbox" class="check-field" name="shipping_address" value="1" id="same_address" checked />
            <span class="check-mark"></span>
          </label>
        </div>
      </div>
<?php } ?>
<?php if ($text_agree) { ?>
      <div class="field-box wmargin">
        <div class="check-box">
          <label class="check-label lp04" for="agree">
            <?php echo $text_agree; ?>
            <!-- <a class="text-green hvr-orange">Privacy Policy </a> -->
            <input type="checkbox" class="check-field" name="agree" value="1" id="agree" />
            <span class="check-mark"></span>
          </label>
        </div>
      </div>
<?php } ?>
      <div class="field-box wmargin text-right">
        <button class="btn-slide btn-back" data-loading-text="<?php echo $text_loading; ?>" style="float: left;"><?php echo $button_back; ?></button>
        <button class="btn-slide green" id="button-register" data-loading-text="<?php echo $text_loading; ?>" style="float: right;"><?php echo $button_continue; ?></button>
      </div>
    </div>
</div>

<script type="text/javascript"><!--
$('#register-content select[name=\'country_id\']').on('change', function() {
    $.ajax({
        url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
        dataType: 'json',
        beforeSend: function() {
            $('#register-content select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
        },
        complete: function() {
            $('.fa-spin').remove();
        },
        success: function(json) {
            if (json['postcode_required'] == '1') {
                $('#register-content input[name=\'postcode\']').parent().addClass('required');
            } else {
                $('#register-content input[name=\'postcode\']').parent().removeClass('required');
            }

            html = '<option value="" selected disabled><?php echo $entry_zone; ?></option>';

            if (json['zone'] && json['zone'] != '') {
                for (i = 0; i < json['zone'].length; i++) {
                    html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                    if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                        html += ' selected="selected"';
                    }

                    html += '>' + json['zone'][i]['name'] + '</option>';
                }
            } else {
                html += '<option value="0" selected="selected" disabled><?php echo $entry_zone; ?></option>';
            }

            $('#register-content select[name=\'zone_id\']').html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

$('#register-content select[name=\'country_id\']').trigger('change');

/********** zone ***********/
$('#register-content select[name=\'zone_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=checkout/checkout/zone&zone_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('#register-content select[name=\'zone_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      html = '<option value="" selected disabled><?php echo $entry_city; ?></option>';

      if (json['city'] && json['city'] != '') {
        for (i = 0; i < json['city'].length; i++) {
          html += '<option value="' + json['city'][i]['city_id'] + '"';

          if (json['city'][i]['city_id'] == '<?php echo $city_id; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['city'][i]['name'] + '</option>';
        }
      } else {
        html += '<option value="0" selected="selected" disabled><?php echo $entry_city; ?></option>';
      }

      $('#register-content select[name=\'city_id\']').html(html);
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

//--></script>
<!-- end of register.tpl