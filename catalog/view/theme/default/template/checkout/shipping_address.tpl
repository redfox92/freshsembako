<!-- shipping-address.tpl -->
<div id="shipping-content">
  <?php if ($addresses) { ?>
    <div class="field-container nomargin">
      <div class="field-box mb5">
        <div class="radio-box">
          <label class="radio-label" for="shipping-address-existing"><?php echo $text_address_existing; ?>
            <input type="radio" class="radio-field" value="existing" name="shipping_address" id="shipping-address-existing" checked>
            <span class="radio-mark"></span>
          </label>
        </div>
      </div>
      <div class="field-box wmargin pl25 pt5 mb30" id="shipping-existing-address">
        <div class="select-box">
          <div class="select-label">Choose Address</div>
          <div class="select-box-chevron">
            <select class="select-field-line" name="address_id">
              <option value="" selected disabled>Choose Address</option>
              <?php foreach ($addresses as $address) { ?>
              <option value="<?php echo $address['address_id']; ?>" <?php echo ($address['address_id'] == $address_id) ? 'selected' : ''; ?>><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
    <div class="field-container nomargin">
      <div class="field-box mb10" style="display: <?php echo ($addresses ? 'block' : 'none'); ?>;">
        <div class="radio-box">
          <label class="radio-label" for="shipping-address-new"><?php echo $text_address_new; ?>
            <input type="radio" class="radio-field" value="new" name="shipping_address" id="shipping-address-new">
            <span class="radio-mark"></span>
          </label>
        </div>
      </div>
      <div class="field-container pl25" id="shipping-new-address" style="display: <?php echo ($addresses ? 'none' : 'block'); ?>;">
        <div class="field-box wmargin">
          <div class="input-box">
            <div class="input-label"><?php echo $entry_fullname; ?></div>
            <input class="input-field-line" type="text" name="fullname" id="input-shipping-fullname">
          </div>
        </div>
        <!-- <div class="field-box wmargin">
          <div class="input-box">
            <div class="input-label">Phone</div>
            <input class="input-field-line" type="text">
          </div>
        </div> -->
        <div class="field-box wmargin">
          <div class="input-box">
            <div class="input-label"><?php echo $entry_address_1; ?></div>
            <input class="input-field-line" type="text" name="address_1" value="" id="input-shipping-address-1">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="field-box mb20">
              <div class="select-box">
                <div class="select-label"><?php echo $entry_country; ?></div>
                <div class="select-box-chevron">
                  <select class="select-field-line" name="country_id" id="input-shipping-country">
                    <option value="" selected disabled><?php echo $entry_country; ?></option>
                      <?php foreach ($countries as $country) { ?>
                      <option value="<?php echo $country['country_id']; ?>" <?php echo ($country['country_id'] == $country_id) ? 'selected' : ''; ?>><?php echo $country['name']; ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="field-box mb20">
              <div class="select-box">
                <div class="select-label"><?php echo $entry_zone; ?></div>
                <div class="select-box-chevron">
                  <select class="select-field-line" name="zone_id" id="input-shipping-zone">
                    <option value="" selected disabled><?php echo $entry_zone; ?></option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="field-box mb20">
              <div class="select-box">
                <div class="select-label"><?php echo $entry_city; ?></div>
                <div class="select-box-chevron">
                  <select class="select-field-line" name="city_id" id="input-shipping-city">
                    <option value="" selected disabled><?php echo $entry_city; ?></option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="field-box mb20">
              <div class="input-box">
                <div class="input-label"><?php echo $entry_postcode; ?></div>
                <input class="input-field-line" type="text" name="postcode" id="input-shipping-postcode">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="field-box wmargin text-right">
      <button class="btn-slide green" id="button-shipping-address" data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_continue; ?></button>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('select[name=address_id]').trigger('change');

    $('input[name=\'shipping_address\']').on('change', function() {
        if (this.value == 'new') {
            $('#shipping-existing-address').slideUp();
            $('#shipping-new-address').slideDown();
        } else {
            $('#shipping-existing-address').slideDown();
            $('#shipping-new-address').slideUp();
        }
    });
});
</script>
<script type="text/javascript"><!--
$('#shipping-address select[name=\'country_id\']').on('change', function() {
    $.ajax({
        url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
        dataType: 'json',
        beforeSend: function() {
            $('#shipping-address select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
        },
        complete: function() {
            $('.fa-spin').remove();
        },
        success: function(json) {
            if (json['postcode_required'] == '1') {
                $('#shipping-address input[name=\'postcode\']').parent().parent().addClass('required');
            } else {
                $('#shipping-address input[name=\'postcode\']').parent().parent().removeClass('required');
            }

            html = '<option value="" selected disabled><?php echo $entry_zone; ?></option>';

            if (json['zone'] && json['zone'] != '') {
                for (i = 0; i < json['zone'].length; i++) {
                    html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                    if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                        html += ' selected="selected"';
                    }

                    html += '>' + json['zone'][i]['name'] + '</option>';
                }
            } else {
                html += '<option value="0" selected="selected" disabled><?php echo $entry_zone; ?></option>';
            }

            $('#shipping-address select[name=\'zone_id\']').html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

$('#shipping-address select[name=\'country_id\']').trigger('change');

/********** zone ***********/
$('#shipping-address select[name=\'zone_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=checkout/checkout/zone&zone_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('#shipping-address select[name=\'zone_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      html = '<option value="" selected disabled><?php echo $entry_city; ?></option>';

      if (json['city'] && json['city'] != '') {
        for (i = 0; i < json['city'].length; i++) {
          html += '<option value="' + json['city'][i]['city_id'] + '"';

          if (json['city'][i]['city_id'] == '<?php echo $city_id; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['city'][i]['name'] + '</option>';
        }
      } else {
        html += '<option value="0" selected="selected" disabled><?php echo $entry_city; ?></option>';
      }

      $('#shipping-address select[name=\'city_id\']').html(html);
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});
//--></script>
<!-- end of shipping-address.tpl -->