<!-- login.tpl -->
<div class="field-container">
  <div class="field-title"><?php echo $text_new_customer; ?></div>
  <div class="field-box wmargin">
    <div class="radio-box">
      <label class="radio-label" for="radio-new-register"><?php echo $text_register; ?>
        <input type="radio" class="radio-field" value="register" name="account" id="radio-new-register" <?php echo ($account == 'register') ? 'checked' : ''; ?>>
        <span class="radio-mark"></span>
      </label>
    </div>
    <?php if ($checkout_guest) { ?>
    <div class="radio-box">
      <label class="radio-label" for="radio-new-guest"><?php echo $text_guest; ?>
        <input type="radio" class="radio-field" value="guest" name="account" id="radio-new-guest" <?php echo ($account == 'guest') ? 'checked' : ''; ?>>
        <span class="radio-mark"></span>
      </label>
    </div>
    <?php } ?>
  </div>
  <div class="field-desc field-box wmargin">
    <?php echo $text_register_account; ?>
  </div>
  <div class="field-box wmargin text-right">
    <button class="btn-slide green" id="button-account" data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_continue; ?></button>
  </div>
</div>
<div class="field-container">
  <div class="field-title"><?php echo $text_returning_customer; ?></div>
  <div class="field-box wmargin">
    <div class="input-box">
      <div class="input-label"><?php echo $entry_email; ?></div>
      <input class="input-field-line" type="text" name="email" value="">
    </div>
  </div>
  <div class="field-box wmargin">
    <div class="input-box">
      <div class="input-label"><?php echo $entry_password; ?></div>
      <input class="input-field-line" type="password" name="password" value="">
    </div>
  </div>
  <div class="field-box wmargin lp04">
    <?php echo $text_forgotten; ?>
    <a class="text-green hvr-orange" href="<?php echo $forgotten; ?>"><?php echo $text_clickhere; ?></a>
  </div>
  <div class="field-box wmargin text-right">
    <button class="btn-slide green" id="button-login" data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_login; ?></button>
  </div>
</div>
<!-- end of login.tpl -->