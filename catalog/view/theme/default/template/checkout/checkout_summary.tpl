<div class="checkout-title">
  <span class="bold">Order Summary</span>
</div>
<div class="checkout-summary-list">
  <div class="cart-product-container">
  <?php foreach ($products as $product) { ?>
    <div class="cart-product-box" style="padding: 0;">
      <div class="cart-p-img">
        <a href="<?php echo $product['href']; ?>" target="_blank" class="hvr-img">
          <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
        </a>
      </div>
      <div class="cart-p-detail">
        <div class="cart-p-name">
          <a href="<?php echo $product['href']; ?>" class="hvr-orange">
            <?php echo $product['name']; ?>
          </a>
        </div>
          <?php if ($product['option']) { ?>
        <div class="cart-p-option">
          <?php foreach ($product['option'] as $option) { ?>
          <?php echo $option['value']; ?> <?php echo $option['name']; ?>
          <?php } ?>
        </div>
          <?php } ?>
        <div class="cart-p-info">
          <div class="cart-p-info-box cart-p-qty">
            Jumlah: <?php echo $product['quantity']; ?>
          </div>
          <div class="cart-p-info-box cart-p-price">
            <sup><?php echo $product['currency']; ?></sup> <?php echo $product['total']; ?>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
  </div>
  <hr>
  <?php foreach ($totals as $total): ?>
  <div class="checkout-s-info">
    <div class="checkout-s-label"><?php echo $total['title']; ?></div>
    <div class="checkout-s-nominal <?php echo $total['code'] == 'coupon' ? 'text-green' : ''; ?>">
      <?php if ($total['code'] != "total") { echo $total['text']; } else { ?>
        <sup><?php echo $total['currency']; ?></sup> <?php echo $total['value']; ?>
      <?php } ?>
    </div>
  </div>
  <?php endforeach ?>
</div>