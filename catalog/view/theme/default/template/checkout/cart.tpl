<?php echo $header; ?>
  <!-- CART PAGE -->
  <div class="cart-page">
    <div class="container">
      <div class="cl-title"><?php echo $heading_title[0]; ?>
        <span class="bold"><?php echo $heading_title[1]; ?></span>
      </div>

      <div class="box-error">
      <?php if ($attention) { ?>
      <div class="alert alert-info"><i class="fa fa-exclamation-circle"></i> <?php echo $attention; ?></div>
      <?php } ?>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?></div>
      <?php } ?>
      <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
      <?php } ?>
      </div>

      <div class="row row-eq-height mb20">
        <div class="col-md-8 col-sm-7 col-xs-12">
        <?php foreach ($products as $product): ?>
          <div class="cart-product-container">
            <div class="cart-product-box">
              <div class="cart-p-img">
                <a href="<?php echo $product['href']; ?>" class="hvr-img">
                  <img src="<?php echo $product['thumb']; ?>" />
                </a>
              </div>
              <div class="cart-p-detail" data-cart_id="<?php echo $product['cart_id']; ?>">
                <div class="cart-p-name">
                  <a href="<?php echo $product['href']; ?>" class="hvr-orange">
                    <?php echo $product['name']; ?>
                    <?php if (!$product['stock']) { ?>
                      <span class="text-danger">***</span>
                    <?php } ?>
                  </a>
                </div>
                  <?php if ($product['option']) { ?>
                <div class="cart-p-option">
                  <?php foreach ($product['option'] as $option) { ?>
                  <?php echo $option['value']; ?> <?php echo $option['name']; ?>
                  <?php } ?>
                </div>
                  <?php } ?>
                <div class="cart-p-info">
                  <div class="cart-p-info-box cart-p-price-label"><?php echo $product['price']; ?></div>
                  <?php if ($product['special']): ?>                    
                  <div class="cart-p-info-box cart-p-price-wline"><?php echo $product['price_normal']; ?></div>
                  <?php endif ?>
                </div>
                <div class="cart-p-info">
                  <div class="cart-p-info-box cart-p-qty">
                    <div class="num-stepper">
                      <div class="ns-step ns-step-down">
                        <i class="fa fa-minus"></i>
                      </div>
                      <div class="ns-value"><?php echo $product['quantity']; ?></div>
                      <div class="ns-step ns-step-up">
                        <i class="fa fa-plus"></i>
                      </div>
                      <input type="hidden" class="ns-qty" value="<?php echo $product['quantity']; ?>" min="<?php echo $product['minimum']; ?>" max="<?php echo $product['maximum']; ?>">
                    </div>
                  </div>
                  <div class="cart-p-info-box cart-p-price">
                    <sup><?php echo $product['currency']; ?></sup> <?php echo $product['total']; ?>
                  </div>
                </div>
              </div>
              <?php if ($product['special']): ?>  
              <div class="cart-p-label"><?php echo $product['discount']; ?></div>
              <?php endif ?>
            </div>
            <div class="cart-product-remove" onclick="cart.remove('<?php echo $product['cart_id']; ?>');">
              <img src="image/assets/home/cancel.png" />
            </div>
          </div>
        <?php endforeach ?>
        </div>
        <div class="col-md-4 col-sm-5 col-xs-12">
          <div class="cart-page-summary">
            <div class="cart-s-info-list">
              <?php foreach ($totals as $total): ?>
              <div class="cart-s-info">
                <div class="cart-s-label"><?php echo $total['title']; ?></div>
                <div class="cart-s-nominal <?php echo $total['code'] == 'coupon' ? 'text-green' : ''; ?>">
                <?php if ($total['code'] != "total") { echo $total['text']; } else { ?>
                  <sup><?php echo $total['currency']; ?></sup> <?php echo $total['value']; ?>
                <?php } ?>
                </div>
              </div>  
              <?php endforeach ?>
            </div>
            <!-- <div class="cart-s-note">
              <ul>
                <li>Free shipping for orders above IDR 200.000</li>
              </ul>
            </div> -->

            <?php foreach ($modules as $module): ?>
              <?php echo $module; ?>
            <?php endforeach ?>

            <div class="cart-note">
              Mohon dicek kembali barang belanjaan Anda. Apabila sudah sesuai, silahkan klik tombol "Pembayaran" dibawah ini.
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-5 col-xs-12 col-sm-offset-7 col-md-offset-8">
          <div class="cart-page-footer">
            <div class="field-box wmargin text-center">
              <a class="btn-slide green w100 btn-checkout" href="<?php echo $checkout; ?>"><?php echo $button_checkout; ?></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    $(document).ready(function () {

      $('.btn-checkout').on('click', function(event) {
        event.preventDefault();
        $('#notes-modal').find('.notes-content').find('.btn-next').attr('href', $(this).attr('href'));
        $('#notes-modal').modal('show');
      });

      $('.cart-page .ns-step-down').on('click', function(){
        if(!$(this).hasClass('disabled')){
          var min = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('min'));
          var max = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('max'));

          var value = parseInt($(this).parents('.num-stepper').find('.ns-value').text()) - 1;

          $(this).parents('.num-stepper').find('.ns-step').removeClass('disabled')

          if (value == max) {
            $(this).parents('.num-stepper').find('.ns-step-up').addClass('disabled');
          }
          if (value == min) {
            $(this).parents('.num-stepper').find('.ns-step-down').addClass('disabled');
          }

          $(this).parents('.num-stepper').find('.ns-value').text(value);
          $(this).parents('.num-stepper').find('.ns-qty').val(value);

          var cart_id = $(this).parents('.cart-p-detail').attr('data-cart_id');
          var new_value  = $(this).siblings('.ns-qty').val();
          if (new_value == 0) {
            $(this).parents('.cart-product-container').find('.cart-product-remove').trigger('click');
          } else {

            $.ajax({
              url: '<?php echo $action; ?>',
              type: 'POST',
              dataType: 'json',
              data: {
                key: cart_id,
                quantity: new_value
              },
              success: function(response){
                if (response.redirect) {
                  location = response.redirect;
                }
              }
            });
          }
        }
      });

      $('.cart-page .ns-step-up').on('click', function(){
        if(!$(this).hasClass('disabled')){
          var min = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('min'));
          var max = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('max'));

          var value = parseInt($(this).parents('.num-stepper').find('.ns-value').text()) + 1;

          $(this).parents('.num-stepper').find('.ns-step').removeClass('disabled')

          if (value == max) {
            $(this).parents('.num-stepper').find('.ns-step-up').addClass('disabled');
          }
          if (value == min) {
            $(this).parents('.num-stepper').find('.ns-step-down').addClass('disabled');
          }

          $(this).parents('.num-stepper').find('.ns-value').text(value);
          $(this).parents('.num-stepper').find('.ns-qty').val(value);

          var cart_id = $(this).parents('.cart-p-detail').attr('data-cart_id');
          var new_value  = $(this).siblings('.ns-qty').val();

          $.ajax({
            url: '<?php echo $action; ?>',
            type: 'POST',
            dataType: 'json',
            data: {
              key: cart_id,
              quantity: new_value
            },
            success: function(response){
              if (response.redirect) {
                location = response.redirect;
              }
            }
          });
        }
      });
    });
  </script>
  <!-- END OF CART PAGE -->
<?php echo $footer; ?>
