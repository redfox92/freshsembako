<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<div class="field-container">
<?php if ($shipping_methods) { ?>
  <div class="field-box wmargin">
    <?php echo $text_shipping_method; ?>
  </div>
<?php foreach ($shipping_methods as $key => $shipping_method) { ?>
<?php if (!$shipping_method['error']) { ?>
<?php foreach ($shipping_method['quote'] as $quote) { ?>
  <div class="field-box field-border wmargin shipping-radio <?php echo $shipping_method['disabled'] ? 'disabled' : ''; ?>">
    <div class="radio-box">
      <label class="radio-label w100" for="radio-shipping-<?php echo $key; ?>">
        <div class="radio-label-part">
          <span class="radio-label-name"><?php echo $shipping_method['title']; ?></span>
          <span class="radio-label-nominal"><?php echo $quote['text']; ?></span>
        </div>
        <input type="radio" class="radio-field" insurance="<?php echo $shipping_method['insurance']; ?>" value="<?php echo $quote['code']; ?>" <?php echo $shipping_method['disabled'] ? 'disabled' : ''; ?> name="shipping_method" id="radio-shipping-<?php echo $key; ?>" <?php if ($quote['code'] == $code) { $code = $quote['code']; ?>checked="checked"<?php } ?>>
        <span class="radio-mark"></span>
      </label>
      <div class="pl25 field-desc"><?php echo $quote['title']; ?></div>
    </div>
  </div>
<?php } ?>
<script type="text/javascript">
  $(document).ready(function() {
    $('.shipping-radio').on('click', function(event) {
      event.preventDefault();
      
      var name = $(this).find('.radio-field').attr('name');
      var id = $(this).find('.radio-field').attr('id');
      var insurance = $(this).find('.radio-field').attr('insurance');

      if (!$(this).find('.radio-field').prop('disabled')) {
        $('input[name' + name + ']').prop('checked', false);

        $('.shipping-radio').each(function(index, el) {
          if ($(this).find('input[name=' + name + ']').attr('id') == id) {
            $(this).find('input[id=' + id + ']').prop('checked', true);
          }
        });
      }

      if (insurance != 0 && !$(this).find('.radio-field').prop('disabled')) {
        $('.insurance-box').removeClass('hidden');
        $('.insurance-box').find('input[name=insurance]').val(insurance);
      } else {
        $('.insurance-box').addClass('hidden');
      }
    });
  });
</script>
<?php } else { ?>
  <!-- kalo mau taro error danger disini ya -->
  <div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
<?php } ?>
<?php } ?>
<?php } ?>

  <div class="insurance-box field-box wmargin hidden">
    <div class="check-box">
      <label class="check-label lp04" for="insurance">Asuransi - Jika harga barang diatas <?php echo $porter_insurance_min_subtotal; ?> lebih baik menggunakan asuransi untuk menghindari kerusakan barang atau kehilangan barang 
        <input type="checkbox" class="check-field" value="1" name="insurance" id="insurance">
        <span class="check-mark"></span>
      </label>
    </div>
  </div>

  <div class="field-box wmargin">
    <div class="textarea-box <?php echo !empty($comment) ? 'filled' : ''; ?>">
      <div class="textarea-label"><?php echo $text_comments; ?></div>
      <textarea class="textarea-field-line" name="comment" rows="4"><?php echo $comment; ?></textarea>
    </div>
  </div>
  <div class="field-box wmargin text-right">
    <button class="btn-slide btn-back" data-loading-text="<?php echo $text_loading; ?>" style="float: left;"><?php echo $button_back; ?></button>
    <button class="btn-slide green" id="button-shipping-method" data-loading-text="<?php echo $text_loading; ?>" style="float: right;"><?php echo $button_continue; ?></button>
  </div>
</div>
