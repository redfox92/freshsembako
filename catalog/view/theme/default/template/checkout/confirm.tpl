<?php if (!isset($redirect)) { ?>
<div class="field-container">
  <div class="table-box">
    <div class="table-head">
      <div class="table-col col-name"><?php echo $column_name; ?></div>
      <div class="table-col col-qty hidden-sm hidden-xs"><?php echo $column_quantity; ?></div>
      <div class="table-col col-price hidden-xs"><?php echo $column_price; ?></div>
      <div class="table-col col-total"><?php echo $column_total; ?></div>
    </div>
    <div class="table-body">
      <?php foreach ($products as $product) { ?>
      <div class="table-row">
        <div class="table-col col-name ellipses">
          <a href="<?php echo $product['href']; ?>" target="_blank" class="hvr-green"><?php echo $product['name']; ?></a>
          <div class="col-qty-xs visible-xs"><?php echo $product['price']; ?></div>
          <?php if ($product['option']) { ?>
          <div class="col-option">
            <?php foreach ($product['option'] as $option) { ?>
            <?php echo $option['value']; ?> <?php echo $option['name']; ?>
            <?php } ?>
          </div>
          <?php } ?>
          <div class="col-qty-xs visible-sm visible-xs"><?php echo $product['quantity']; ?> x <?php echo $product['weight']; ?></div>
        </div>
        <div class="table-col col-qty hidden-sm hidden-xs"><?php echo $product['quantity']; ?> x <?php echo $product['weight']; ?></div>
        <div class="table-col col-price hidden-xs"><?php echo $product['price']; ?></div>
        <div class="table-col col-total"><?php echo $product['total']; ?></div>
      </div>
      <?php } ?>
      <!-- utk yg voucher-->
      <?php foreach ($vouchers as $voucher) { ?>
      <div class="table-row">
        <div class="table-col col-name">
          <?php echo $voucher['description']; ?>
        </div>
        <div class="table-col col-qty hidden-sm hidden-xs">1</div>
        <div class="table-col col-price hidden-xs"><?php echo $voucher['amount']; ?></div>
        <div class="table-col col-total"><?php echo $voucher['amount']; ?></div>
      </div>
      <?php } ?>
    </div>
    <div class="table-foot">
      <?php foreach ($totals as $total) { ?>
      <div class="table-row">
        <div class="table-col table-foot-label col-subtotal">
          <?php echo $total['title']; ?>
        </div>
        <div class="table-col col-total"><?php echo $total['text']; ?></div>
      </div>
      <?php } ?>
    </div>
  </div>
<?php echo $payment; ?>
  <!-- klo ada notes -->
  <!-- <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> Warning: The payment gateway is in 'Sandbox Mode'. Your account will not be charged.</div> -->
</div>
<?php } else { ?>
<script type="text/javascript"><!--
location = '<?php echo $redirect; ?>';
//--></script>
<?php } ?>