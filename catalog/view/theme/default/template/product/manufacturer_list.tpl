<?php echo $header; ?>
<div class="brand-list">
  <div class="container">
      <div class="cl-title"><?php echo $heading_title; ?></div>
      <?php if ($categories) { ?>
      <div class="row">
      <?php foreach ($categories as $category) { ?>
        <div class="col-md-3 col-sm-4 col-xs-6">
          <h2><?php echo $category['name']; ?></h2>

          <?php if ($category['manufacturer']) { ?>
          <?php foreach (array_chunk($category['manufacturer'], 4) as $manufacturers) { ?>
              <ul>
            <?php foreach ($manufacturers as $manufacturer) { ?>
              <li><a href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a></li>
            <?php } ?>    
              </ul>
          <?php } ?>
          <?php } ?>
        </div>
      <?php } ?>
      </div>
      <?php } ?>
  </div>
</div>
<style type="text/css">
  .brand-list {
    padding: 20px 0 26px;
  }
  .brand-list .col-md-3:nth-child(4n+1) {
    clear: left;
  }
  .brand-list h2 {
    color: #fe6122;
  }
  .brand-list ul {
    list-style-type: none;
  }
  .brand-list ul li a {
    color: #53d800;
  }
  .brand-list ul li a:hover {
    color: #fe6122;
  }
@media (max-width: 992px) {
  .brand-list .col-sm-4:nth-child(3n+1) {
    clear: left;
  }
  .brand-list .col-md-3:nth-child(4n+1) {
    clear: inherit;
  }
}
@media (max-width: 767px) {
  .brand-list .col-xs-6:nth-child(2n+1) {
    clear: left;
  }
  .brand-list .col-sm-4:nth-child(3n+1) {
    clear: inherit;
  }
}
</style>
<?php echo $footer; ?>