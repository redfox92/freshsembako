<?php echo $header; ?>
<?php echo $column_left; ?>
<?php echo $content_top; ?>
<!-- PRODUCT DETAIL -->


<div class="product-detail">
  <div class="container">
    <div class="row row-eq-height">
      <div class="col-md-6 col-sm-6 col-xs-12 pd-slider-container ms-nav-parent">
        <div class="pd-slider-wrapper <?php echo ($count_image <= 1) ? 'single-item' : ''; ?>">
          <?php if ($thumb || $images) { ?>
          <div class="pd-slider owl-theme owl-carousel">
            <?php if ($thumb) { ?>
            <div class="item">
              <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>">
            </div>
            <?php } ?>
            <?php if ($images) { ?>
            <?php foreach ($images as $image) { ?>
            <div class="item">
              <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>">
            </div>
            <?php } ?>
            <?php } ?>


   <!--            <div class="item">
                <img src="image/assets/home/p2-1020x860.png">
              </div>
              <div class="item">
                <img src="image/assets/home/p3-1020x860.png">
              </div> -->
          </div>
          <?php } ?>
          <?php if ($count_image > 1): ?>

          <div class="ms-nav vertical">
            <div class="msn-item">
              <div class="msn-left">
                <div class='outer-shadow'>
                </div>
                <div class='inner-shadow'>
                </div>
                <div class='hold left'>
                  <div class='fill'></div>
                </div>
                <div class='hold right'>
                  <div class='fill'></div>
                </div>
                <i class="fa fa-arrow-left"></i>
              </div>
            </div>
            <div class="msn-item">
              <div class="msn-right load">
                <div class='outer-shadow'>
                </div>
                <div class='inner-shadow'>
                </div>
                <div class='hold left'>
                  <div class='fill'></div>
                </div>
                <div class='hold right'>
                  <div class='fill'></div>
                </div>
                <i class="fa fa-arrow-right"></i>
              </div>
            </div>
          </div>
          <?php endif ?>
        </div>
        <!-- <div class="pd-greyback"></div> -->
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 pd-detail">
        <div class="pd-box">
          <div class="pd-title">
            <div class="pd-ttext"><?php echo $heading_title; ?></div>
            <div class="pd-share">
              <i class="fa fa-share-alt"></i>
              <div class="pd-share-btn">
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
            </div>
            </div>
          </div>
          <!-- <div class="pd-unitprice"><?php echo $text_unitprice; ?></div>
          <?php if ($price) { ?>
          <div class="pd-price-box">
            <?php if (!$special) { ?>
            <div class="pd-price" currency="<?php echo $currency; ?>" price="<?php echo $price_unformat ? $price_unformat : ''; ?>">
            <sup><?php echo $currency; ?></sup> <?php echo $price; ?></div>
            <?php } else { ?>
            <div class="pd-price-wline"><?php echo $currency; ?> <?php echo $price; ?></div>
            <div class="pd-price">
              <sup><?php echo $currency; ?></sup> <?php echo $special ?></div>
            <?php } ?>
          </div>
          <?php } ?> -->
          <?php if ($options && $quantity != 0) { ?>
            <div class="pd-option">
              <div class="pd-option-title"><?php echo $text_option; ?></div>
            <?php foreach ($options as $option) { ?>
            <?php if ($option['type'] == 'checkbox') { ?>
              <div class="table-box">
                <div class="table-head">
                  <div class="table-row">
                    <div class="table-col col-detail-cb"></div>
                    <div class="table-col col-detail-option">Kemasan</div>
                    <div class="table-col col-detail-price hidden-sm hidden-xs">Harga</div>
                    <div class="table-col col-detail-stock hidden-xs"><!-- Stok --></div>
                    <div class="table-col col-detail-qty">Jumlah</div>
                  </div>
                </div>
                <div class="table-body">
                <?php foreach ($option['product_option_value'] as $idx => $option_value) { ?>
                  <div class="table-row">
                    <div class="table-col middle col-detail-cb">
                      <div class="check-box">
                        <label class="check-label lp04" for="option-<?php echo $option_value['product_option_value_id'] ?>">
                          <input type="checkbox" class="ns-qty check-field" name="option[<?php echo $option['product_option_id']; ?>][<?php echo $option_value['product_option_value_id']; ?>]" value="1" id="option-<?php echo $option_value['product_option_value_id'] ?>">
                          <span class="check-mark"></span>
                        </label>
                      </div>
                    </div>
                    <div class="table-col middle col-detail-option">
                      <?php echo $option_value['name']; ?> <?php echo $option['name']; ?>
                      <div class="text-grey visible-xs visible-sm"><?php echo $option_value['price']; ?></div>
                      <div class="text-grey visible-xs"><!-- Stok:  --><?php //echo $option_value['stock']; ?></div>
                    </div>
                    <div class="table-col middle col-detail-price price hidden-sm hidden-xs" currency="<?php echo $currency; ?>" price="<?php echo $option_value['price_unformat']; ?>"><?php echo $option_value['price']; ?></div>
                    <div class="table-col middle col-detail-stock stock hidden-xs" min="<?php echo $minimum; ?>" stock="<?php echo $option_value['stock']; ?>"><?php //echo $option_value['stock']; ?></div>
                    <div class="table-col middle col-detail-qty">
                      <div class="num-stepper">
                        <div class="ns-step ns-step-down disabled">
                          <i class="fa fa-minus"></i>
                        </div>
                        <div class="ns-value">1</div>
                        <div class="ns-step ns-step-up">
                          <i class="fa fa-plus"></i>
                        </div>
                        <!-- <input type="hidden" class="ns-qty" value="1" min="1" max="10"> -->
                      </div>
                    </div>
                  </div>
                <?php } ?>
                </div>
              </div>
            <?php } ?>
            <?php } ?>
            </div>
          <?php } ?>
          <?php if ($quantity != 0) { ?>


            <div class="pd-qty wo-stepper">
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
              <div class="pd-btn dv-large">
                <a href="javascript:;" class="button-cart btn-slide green">
                  <img src="image/assets/home/basket.png"><?php echo $button_cart; ?>
                </a>
              </div>
              <div class="pd-total">Total: <span class="pd-amount">0</span></div>
              <div class="pd-btn dv-medium">
                <a href="javascript:;" class="button-cart btn-slide green xs-big">
                  <img src="image/assets/home/basket.png"><?php echo $button_cart; ?>
                </a>
              </div>
            </div>
          <!-- <div class="pd-qty">
            <div class="num-stepper">
              <div class="ns-step ns-step-down disabled">
                <i class="fa fa-minus"></i>
              </div>
              <div class="ns-value">1</div>
              <div class="ns-step ns-step-up">
                <i class="fa fa-plus"></i>
              </div>
              <input type="hidden" class="ns-qty" name="quantity" value="1" min="<?php echo $minimum; ?>" max="<?php echo $quantity; ?>">
            </div> -->
            <!-- <div class="pd-btn">
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
              <a class="btn-slide green" href="javascript:;" id="button-cart">
                <img src="image/assets/home/basket.png"><?php echo $button_cart; ?>
              </a>
              <label class="total-product" style="margin-left: 60px;"><span>Total </span>Rp 0</label>
            </div>
          </div> -->
          <?php } else { ?>
          <div class="info-box">
            <i class="fa fa-exclamation-circle"></i>
            Stok Produk Habis

          </div>
          <?php } ?>


          <?php if ($description) { ?>
          <div class="pd-desc">
            <div class="pdd-title"><?php echo $tab_description; ?></div>
            <!-- Kalo bisa, konten deskripsi ini di limit dari backendnya (jmlh karakter), soalnya klo terlalu pnjg dskripsinya jd jelek (slider kanannya ngegantung sama deskripsinya) -->
            <div class="pdd-content">
              <?php echo $description ?>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>


    <div class="row row-eq-height">
      <div class="col-md-12">
        <div class="add-info">
          <div class="ai-box">
            <div class="ai-box-title">Pengiriman</div>
            <div class="ai-box-desc">
              Pesanan yang dibayarkan <b>sebelum</b> pukul 16.00 WIB akan diproses pada <b>H+1.</b><br>
              Pesanan yang dibayarkan <b>lebih dari</b> pukul 16.00 WIB akan diproses pada <b>H+2.</b>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  $(document).ready(function () {
    $($('.pd-option .table-box .table-row')[1]).find('input[type=checkbox]').trigger('click');
    updateTotal();

  $('.button-cart').on('click', function() {
    var $thisBtn = $(this);

    $.ajax({
      url: 'index.php?route=checkout/cart/add',
      type: 'post',
      data: $('.product-detail input[type=\'text\'], .product-detail input[type=\'hidden\'], .product-detail input[type=\'radio\']:checked, .product-detail input[type=\'checkbox\']:checked, .product-detail select, .product-detail textarea'),
      dataType: 'json',
      beforeSend: function() {
        // $('#button-cart').button('loading');
      },
      complete: function() {
        // $('#button-cart').button('reset');
      },
      success: function(json) {
        $('.alert, .text-danger').remove();
        $('.form-group').removeClass('has-error');

        if (json['error']) {
          if (json['error']['option']) {
            // console.log($(this));
            $thisBtn.parents('.pd-box').find('.pd-option').append('<div class="alert alert-danger">Harap centang salah satu Kemasan di atas.</div>');
            // for (i in json['error']['option']) {
            //   var element = $('#input-option' + i.replace('_', '-'));

            //   if (element.parent().hasClass('input-group')) {
            //     element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
            //   } else {
            //     element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
            //   }
            // }
          }

          if (json['error']['recurring']) {
            $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
          }

          // Highlight any found errors
          // $('.text-danger').parent().addClass('has-error');
        }

        if (json['success']) {
          // $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

          // $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');

          // $('html, body').animate({ scrollTop: 0 }, 'slow');

          // $('#cart > ul').load('index.php?route=common/cart/info ul li');

          $('.wrap-cart').load('index.php?route=common/cart/info', function(responseTxt, statusTxt, xhr){
            if(statusTxt == "success") {
              open_cart();
            }
          });
        }
      },
          error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
    });
  });

    $('.pd-share i').on('click', function () {
      $(this).parent('.pd-share').toggleClass('active');
    });

    $(document).on('click', function (event) {
      if (!$(event.target).closest('.pd-share').length) {
        if ($('.pd-share').hasClass('active')) {
          $('.pd-share').removeClass('active');
        }
      }
    });

    $('.pd-slider').owlCarousel({
      margin: 20,
      loop: ($('.pd-slider .item').length <= 1) ? false : true,
      dots: false,
      nav: false,
      items: 1,
      autoplay: true,
      autoplayTimeout: 5500,
      navText: ["<i class='owl-btn owl-btn-prev fa fa-angle-left'></i>", "<i class='owl-btn owl-btn-next fa fa-angle-right'></i>"],
    })
  });

  $('.pd-slider.owl-carousel').on('translated.owl.carousel', function (event) {
    $(this).parents('.ms-nav-parent').find('.msn-right').addClass('load');
  })

  $('.pd-slider.owl-carousel').on('translate.owl.carousel', function (event) {
    $(this).parents('.ms-nav-parent').find('.msn-right').removeClass('load');
  })

  $('.pd-slider-container').delegate('.msn-right', 'click', function(){
    $(this).parents('.ms-nav-parent').find('.pd-slider.owl-carousel').trigger('stop.owl.autoplay').trigger('play.owl.autoplay');
    $(this).parents('.ms-nav-parent').find('.pd-slider.owl-carousel').trigger('next.owl.carousel', [300]);
  });

  $('.pd-slider-container').delegate('.msn-left', 'click', function(){
    $(this).parents('.ms-nav-parent').find('.pd-slider.owl-carousel').trigger('stop.owl.autoplay').trigger('play.owl.autoplay');
    $(this).parents('.ms-nav-parent').find('.pd-slider.owl-carousel').trigger('prev.owl.carousel', [300]);
  });

</script>
<!-- END OF PRODUCT DETAIL -->

<!-- MAIN PRODUCT -->
<?php if ($products) { ?>
<div class="main-product">
<div class="container">
  <div class="mp-box">
    <div class="mp-box-title"><?php echo $text_related; ?></div>
    <div class="row">
      <!-- <div class="col-md-3 col-sm-3 col-xs-6">
        <div class="mp-banner-container">
          <div class="mp-banner" style="background-image: url(<?php //echo $category_related['banner']; ?>)">
            <div class="mp-banner-overlay red">
              <div class="mpb-title"><?php //echo $category_related['name']; ?></div>
              <div class="mpb-desc">
                  <?php //echo strip_tags($category_related['desc']); ?>
              </div>
              <div class="mpb-btn">
                <a href="<?php //echo $category_related['href']; ?>" class="btn-slide green"><?php //echo $button_view_all; ?></a>
              </div>
            </div>
          </div>
        </div>
      </div> -->
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="mp-slider-container">
          <div class="mp-slider owl-theme owl-carousel">
          <?php foreach ($products as $product) { ?>
            <div class="item">
              <div class="product-container <?php echo (!$product['quantity']) ? 'out-of-stock' : ''; ?>">
                <a href="<?php echo $product['href']; ?>" class="product-link">
                  <div class="product-box <?php echo ($product['discount']) ? 'special-price' : ''; ?>">
                    <div class="product-img <?php echo ($product['noimage']) ? 'no-image' : ''; ?>">
                      <img src="<?php echo $product['thumb']; ?>">
                      <?php if (!$product['quantity']) { ?>
                        <div class="stock-info">
                          <div>Stok Habis</div>
                        </div>
                      <?php } ?>
                    </div>
                    <div class="product-name"><?php echo $product['name']; ?></div>
                    <?php if ($product['special']) { ?>
                    <div class="product-price-wline"><?php echo $product['price']; ?></div>
                    <div class="product-price"><?php echo $product['special']; ?></div>
                    <?php } else { ?>
                    <div class="product-price"><?php echo $product['price']; ?></div>
                    <?php } ?>
                    <!-- <div class="product-weight"><?php echo $product['weight']; ?></div> -->
                  </div>
                </a>                <?php if (!empty($product['label'])) { ?>                <div class="product-label"><?php echo $product['label']; ?></div>                <?php } ?>                <?php if (!empty($product['label_2'])) { ?>                <div class="product-label-2"><?php echo $product['label_2']; ?></div>                <?php } ?>
                <?php if ($product['discount']) { ?>
                <div class="product-label"><?php echo $product['discount']; ?></div>
                <?php } ?>
                <div class="product-btn">
                  <a class="btn-slide green <?php echo (!$product['quantity']) ? 'disabled' : ''; ?>" href="javascript:;" <?php if ($product['quantity']) { ?>onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"<?php } ?>>
                    <img src="image/assets/home/basket.png">
                    <?php echo $button_cart; ?>
                  </a>
                </div>
              </div>
            </div>
          <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php } ?>


<script type="text/javascript">
function updateTotal() {

  var $total = 0;

  var $currency = 'Rp ';

  $('.ns-qty:checked').each(function(index, el) {

    $total += parseInt($(this).val()) * parseInt($(this).parents('.table-row').find('.price').attr('price'));



    $currency = $(this).parents('.table-row').find('.price').attr('currency');

  });

  $('.pd-total').find('.pd-amount').html($currency + numberFormater($total));

}

$(document).ready(function () {

  $('.ns-qty').on('click', function(event) {



    updateTotal();

  });

  $('.mp-slider').owlCarousel({

    loop: false,

    dots: false,

    nav: true,

    navText: ["<i class='owl-btn owl-btn-prev fa fa-angle-left'></i>", "<i class='owl-btn owl-btn-next fa fa-angle-right'></i>"],

    autoplay: true,

    autoplayTimeout: 5500,

    responsive: {

      0: {

        items: 2,

        margin: 0,

      },

      768: {

        items: 3,

        margin: 7,

      },

      995: {

        items: 4,

        margin: 10,

      },

      1025: {

        items: 5,

        margin: 15,

      }

    }

  });



  if ($(window).width() < 768) {

    var url = $('.mmc-banner-link').attr('href');

    $('.mmc-banner-link-mobile').attr('href', url);

  }



  $('.product-detail .ns-step-down').on('click', function(){

    if(!$(this).hasClass('disabled')){

      // var min = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('min'));

      var min = parseInt($(this).parents('.table-row').find('.stock').attr('min'));

      // var max = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('max'));

      var max = parseInt($(this).parents('.table-row').find('.stock').attr('stock'));



      var value = parseInt($(this).parents('.num-stepper').find('.ns-value').text()) - 1;



      $(this).parents('.num-stepper').find('.ns-step').removeClass('disabled')



      if (value == max) {

        $(this).parents('.num-stepper').find('.ns-step-up').addClass('disabled');

      }

      if (value == min) {

        $(this).parents('.num-stepper').find('.ns-step-down').addClass('disabled');

      }



      // buat yang satuan

      // var newPrice = value * $('.pd-price').attr('price');

      // $('.pd-price').html("<sup>" + $('.pd-price').attr('currency') + "</sup>" + numberFormater(newPrice));



      // buat yang option

      var newPrice = value * $(this).parents('.table-row').find('.table-col.price').attr('price');

      var currency = $(this).parents('.table-row').find('.table-col.price').attr('currency');

      $(this).parents('.table-row').find('.table-col.price').html(currency + ' ' + numberFormater(newPrice));



      $(this).parents('.num-stepper').find('.ns-value').text(value);

      $(this).parents('.num-stepper').parents('.table-row').find('.ns-qty').val(value);

      updateTotal();

    }

  });



  $('.product-detail .ns-step-up').on('click', function(){

    if(!$(this).hasClass('disabled')){

      // var min = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('min'));

      var min = parseInt($(this).parents('.table-row').find('.stock').attr('min'));

      // var max = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('max'));

      var max = parseInt($(this).parents('.table-row').find('.stock').attr('stock'));



      var value = parseInt($(this).parents('.num-stepper').find('.ns-value').text()) + 1;



      $(this).parents('.num-stepper').find('.ns-step').removeClass('disabled')



      if (value == max) {

        $(this).parents('.num-stepper').find('.ns-step-up').addClass('disabled');

      }

      if (value == min) {

        $(this).parents('.num-stepper').find('.ns-step-down').addClass('disabled');

      }



      // buat yang satuan

      // var newPrice = value * $('.pd-price').attr('price');

      // $('.pd-price').html("<sup>" + $('.pd-price').attr('currency') + "</sup>" + numberFormater(newPrice));



      // buat yang option

      var newPrice = value * $(this).parents('.table-row').find('.table-col.price').attr('price');

      var currency = $(this).parents('.table-row').find('.table-col.price').attr('currency');

      $(this).parents('.table-row').find('.table-col.price').html(currency + ' ' + numberFormater(newPrice));



      $(this).parents('.num-stepper').find('.ns-value').text(value);

      $(this).parents('.num-stepper').parents('.table-row').find('.ns-qty').val(value);

      updateTotal();

    }

  });

});

</script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b628418978dd450"></script>
<!-- END OF MAIN PRODUCT -->
<?php echo $content_bottom; ?>
<?php echo $column_right; ?>
<?php echo $footer; ?>
