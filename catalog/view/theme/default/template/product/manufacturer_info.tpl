<?php echo $header; ?>
<div class="category-product">
<div class="container">
  <h2><?php echo $heading_title; ?></h2>
  <div class="row">
  <?php if ($products) { ?>
  <?php foreach ($products as $product) { ?>
    <div class="col-md-3 col-sm-3 col-xs-6 col-md-2p5">
      <div class="product-container <?php echo (!$product['quantity']) ? 'out-of-stock' : ''; ?>">
        <a href="<?php echo $product['href']; ?>" class="product-link">
          <div class="product-box <?php echo ($product['discount']) ? 'special-price' : ''; ?>">
            <div class="product-img <?php echo ($product['noimage']) ? 'no-image' : ''; ?>">
              <img src="<?php echo $product['thumb']; ?>">
              <?php if (!$product['quantity']) { ?>
                <div class="stock-info">
                  <div>Stok Habis</div>
                </div>
              <?php } ?>
            </div>
            <div class="product-name"><?php echo $product['name']; ?></div>
            <?php if ($product['special']) { ?>
            <div class="product-price-wline"><?php echo $product['price']; ?></div>
            <div class="product-price"><?php echo $product['special']; ?></div>
            <?php } else { ?>
            <div class="product-price"><?php echo $product['price']; ?></div>
            <?php } ?>
            <!-- <div class="product-weight"><?php echo $product['caption']; ?></div> -->
          </div>
        </a>
        <?php if ($product['discount']) { ?>
        <div class="product-label"><?php echo $product['discount']; ?></div>
        <?php } ?>
        <div class="product-btn">
          <a class="btn-slide green <?php echo (!$product['quantity']) ? 'disabled' : ''; ?>" href="javascript:;" <?php if ($product['quantity']) { ?>onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"<?php } ?>>
            <img src="image/assets/home/basket.png">
            <span><?php echo $button_cart; ?></span>
          </a>
        </div>
      </div>
    </div>
  <?php } ?>
  <?php } else { ?>
    <div class="col-md-12">
      <p><?php echo $text_empty; ?></p>
    </div>
  <?php } ?>
  </div>
  <div class="row">
    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
    <!-- <div class="col-sm-6 text-right"><?php //echo $results; ?></div> -->
  </div>
</div>
</div>
<?php echo $footer; ?>
