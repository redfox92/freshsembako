<?php echo $header; ?>
<!-- CONTACT PAGE -->
<div class="contact-banner" style="background-image: url('image/assets/home/fruits_and_vegetables1440x450.png')"></div>
<div class="contact-page">
  <div class="container">
    <div class="color-title text-center"><span class="orange"><?php echo $heading_title[0]; ?></span> <span class="green"><?php echo $heading_title[1]; ?></span></div>
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="contact-info">
          <div class="contact-info-title">
            <div class="ci-title">Let's get in touch.</div>
            <div class="ci-title bold"><?php echo $heading_title[0] . ' ' . $heading_title[1]; ?> <i class="fa fa-arrow-right"></i></div>
          </div>
          <div class="contact-info-list">
            <div class="contact-info-item">
              <div class="cii-label"><?php echo $text_address; ?></div>
              <div class="cii-desc"><?php echo $address; ?></div>
            </div>
            <div class="contact-info-item">
              <div class="cii-label"><?php echo $text_email; ?></div>
              <div class="cii-desc"><a href="mailto:<?php echo $email_store; ?>" class="hvr-orange"><?php echo $email_store; ?></a></div>
            </div>
            <div class="contact-info-item">
              <div class="cii-label"><?php echo $text_telephone; ?></div>
              <div class="cii-desc"><a href="tel:<?php echo $telephone; ?>" class="hvr-orange"><?php echo $telephone; ?></a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="contact-form">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <div class="field-box wmargin">
              <div class="input-box <?php echo $name ? 'filled' : ''; ?>">
                <div class="input-label"><?php echo $entry_name; ?></div>
                <input class="input-field-line" type="text" name="name" value="<?php echo $name; ?>">
              </div>
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
            <div class="field-box wmargin">
              <div class="input-box <?php echo $email ? 'filled' : ''; ?>">
                <div class="input-label"><?php echo $entry_email; ?></div>
                <input class="input-field-line" type="text" name="email" value="<?php echo $email; ?>">
              </div>
              <?php if ($error_email) { ?>
              <div class="text-danger"><?php echo $error_email; ?></div>
              <?php } ?>
            </div>
            <div class="field-box wmargin">
              <div class="input-box <?php echo $subject ? 'filled' : ''; ?>">
                <div class="input-label"><?php echo $entry_subject; ?></div>
                <input class="input-field-line" type="text" name="subject" value="<?php echo $subject; ?>">
              </div>
            </div>
            <div class="field-box wmargin">
              <div class="textarea-box <?php echo $enquiry ? 'filled' : ''; ?>">
                <div class="textarea-label"><?php echo $entry_enquiry; ?></div>
                <textarea class="textarea-field-line" rows="4" name="enquiry"><?php echo $enquiry; ?></textarea>
              </div>
              <?php if ($error_enquiry) { ?>
              <div class="text-danger"><?php echo $error_enquiry; ?></div>
              <?php } ?>
            </div>
            <div class="field-box wmargin">
              <button class="btn-slide green" type="submit"><?php echo $button_submit; ?></button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END OF CONTACT PAGE -->
<?php echo $footer; ?>