<?php echo $header; ?>
<!-- INFORMATION PAGE -->
<div class="information-page">
  <div class="container">
    <div class="color-title"><?php echo $heading_title; ?></div>
    <div class="ip-content">
      <?php echo $description; ?>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    var text = $(".information-page .color-title").text();
    var first = '<span class="orange">' + text.substring(0, text.indexOf(' ')) + '</span>';
    var last = '<span class="green">' + text.substring(text.indexOf(' ')) + '</span>';

    $(".information-page .color-title").html(first + last);
  });
</script>
<!-- END OF INFORMATION PAGE -->
<?php echo $footer; ?>