$(document).ready(function() {
  // $('body').delegate('.ns-step-down', 'click', function(){
  //   if(!$(this).hasClass('disabled')){
  //     var min = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('min'));
  //     var max = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('max'));

  //     var value = parseInt($(this).parents('.num-stepper').find('.ns-value').text()) - 1;

  //     $(this).parents('.num-stepper').find('.ns-step').removeClass('disabled')

  //     if (value == max) {
  //       $(this).parents('.num-stepper').find('.ns-step-up').addClass('disabled');
  //     }
  //     if (value == min) {
  //       $(this).parents('.num-stepper').find('.ns-step-down').addClass('disabled');
  //     }

  //     $(this).parents('.num-stepper').find('.ns-value').text(value);
  //     $(this).parents('.num-stepper').find('.ns-qty').val(value);
  //   }
  // });

  // $('body').delegate('.ns-step-up', 'click', function(){
  //   if(!$(this).hasClass('disabled')){
  //     var min = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('min'));
  //     var max = parseInt($(this).parents('.num-stepper').find('.ns-qty').attr('max'));

  //     var value = parseInt($(this).parents('.num-stepper').find('.ns-value').text()) + 1;

  //     $(this).parents('.num-stepper').find('.ns-step').removeClass('disabled')

  //     if (value == max) {
  //       $(this).parents('.num-stepper').find('.ns-step-up').addClass('disabled');
  //     }
  //     if (value == min) {
  //       $(this).parents('.num-stepper').find('.ns-step-down').addClass('disabled');
  //     }

  //     $(this).parents('.num-stepper').find('.ns-value').text(value);
  //     $(this).parents('.num-stepper').find('.ns-qty').val(value);
  //   }
  // });

  $(document).delegate('.input-label', 'click', function(event) {
    $(this).on('click', function () {
      $(this).parents('.input-box').addClass('filled focused');
      $(this).parents('.input-box').find('.input-field-line').focus();
    });
  });

  $(document).delegate('.input-field-line', 'focus', function(event) {
    if (!$(this).hasClass('file-field')) {
      $(this).parents('.input-box').addClass('filled focused');
    }
  });

  $(document).delegate('.input-field-line', 'blur', function(event) {
    if (!$(this).hasClass('file-field')) {
      $(this).parents('.input-box').removeClass('focused');

      if ($(this).val() == '') {
        $(this).parents('.input-box').removeClass('filled');
      }
    }
  });

  // $('.file-input').on('click', function(){
  //   $(this).parents('.input-box').find('input[type=file]').trigger('click');
  // });

  $(document).delegate('.textarea-label', 'click', function(event) {
    $(this).parents('.textarea-box').addClass('filled focused');
    $(this).parents('.textarea-box').find('.textarea-field-line').focus();
  });

  $(document).delegate('.textarea-field-line', 'focus', function(event) {
    $(this).parents('.textarea-box').addClass('filled focused');
  });
  
  $(document).delegate('.textarea-field-line', 'blur', function(event) {
    $(this).parents('.textarea-box').removeClass('focused');
    if ($(this).val() == '') {
      $(this).parents('.textarea-box').removeClass('filled');
    }
  });

  $(document).delegate('.select-field-line', 'change', function(event) {
    if ($(this).val() != '') {
      $(this).parents('.select-box').find('.select-label').addClass('filled');
      $(this).addClass('filled');
    } else {
      $(this).removeClass('filled');
      $(this).parents('.select-box').find('.select-label').removeClass('filled');
    }
  });

  $('.apm-close-btn').on('click', function () {
    $('.account-side-menu').removeClass('active');
    $('body').removeClass('lock');
  });

  $('.account-side-menu-button').on('click', function () {
    $('.account-side-menu').addClass('active');
    $('body').addClass('lock');
  });

  $('button[data-toggle="modal"], a[data-toggle="modal"]').on('click', function () {
    var a = $(this).data('target');
    $('.modal').not(a).modal('hide');
    setTimeout(function () {
      $('body').addClass('modal-open');
    }, 500);
  });
});