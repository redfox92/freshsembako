<?php
class ModelAccountSubscribe extends Model {
	public function addSubscribe($data = array()) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "subscribe` SET `email` = '" . $this->db->escape($data['email']) . "', `date_added` = NOW()");
	}

	public function getSubscriber($email)
	{
		$sql = "SELECT * FROM " . DB_PREFIX . "subscribe WHERE email='" . $this->db->escape($email) . "'";

		return $this->db->query($sql);
	}
}