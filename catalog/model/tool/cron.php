<?php
class ModelToolCron extends Model {

	/*
	jangan lupa order_status_id untuk FAILED di sesuaikan
	19 is order_status_id for Expired
	*/

	public function updatePending($day=3) {

		//delete amount Credit in oc_customer_transaction
		$this->db->query("
			DELETE FROM `" . DB_PREFIX . "customer_transaction` WHERE order_id IN (
			SELECT
					GROUP_CONCAT(order_id) as order_ids
				FROM
					(
						SELECT
							DATEDIFF(CURDATE(), o.date_added) AS days,
							o.date_added,
							o.order_id
						FROM
							`".DB_PREFIX."order` o
						WHERE
							o.order_status_id = 1
						ORDER BY
							o.date_added ASC
					) tmp
				WHERE
					tmp.days >= {$day}
			)"
		);
		
		//update order status FAILED to order history
		$this->db->query("
			INSERT INTO 
				" . DB_PREFIX . "order_history 
				(
					order_history_id,
					order_id,
					order_status_id,
					notify,
					comment,
					date_added,
					user_id
				)
				SELECT
					'',
					order_id,
					19,
					'0',
					'Failed by System',
					NOW(),
					1
				FROM
					(
						SELECT
							DATEDIFF(CURDATE(), o.date_added) AS days,
							o.date_added,
							o.order_id
						FROM
							`".DB_PREFIX."order` o
						WHERE
							o.order_status_id = 1
						ORDER BY
							o.date_added ASC
					) tmp
				WHERE
					tmp.days >= {$day}
		");

		//update to order
		$this->db->query("
			UPDATE
				`" . DB_PREFIX . "order` o
			SET 
				o.order_status_id=19
			WHERE
				o.order_id 
			IN 
			(
				SELECT
					order_id
				FROM
					(
						SELECT
							DATEDIFF(CURDATE(), o.date_added) AS days,
							o.date_added,
							o.order_id
						FROM
							`".DB_PREFIX."order` o
						WHERE
							o.order_status_id = 1
						ORDER BY
							o.date_added ASC
					) tmp
				WHERE
					tmp.days >= {$day}
			)
		");
	}

	public function check_pending(){
		$order_ids = $this->db->query("
			SELECT
				GROUP_CONCAT(order_id) as order_ids
			FROM
				(
					SELECT
						DATEDIFF(CURDATE(), o.date_added) AS days,
						o.date_added,
						o.order_id
					FROM
						`".DB_PREFIX."order` o
					WHERE
						o.order_status_id = 1
					ORDER BY
						o.date_added ASC
				) tmp
			WHERE
				tmp.days >= 3
		")->row;

		return $order_ids;
	}

	public function deleteForgottenToken($hours){
		$this->db->query("
			UPDATE 
			`".DB_PREFIX."customer` c
			SET 
				c.code = '',
				c.code_date_added = '0000-00-00 00:00:00'
			WHERE
				HOUR(TIMEDIFF(NOW(),c.code_date_added)) >= {$hours}"
		);
	}

	public function getCities($filter = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "city WHERE status = '1'";

		if ($filter) {
			if (isset($filter['date_modifified'])) {
				$sql .= ' AND date_modifified';		
			}

			if (isset($filter['update_jne'])) {
				$sql .= ' AND date_modified < "'. date('Y-m-d 00:00:00') .'"';
			}

			if (isset($filter['order_by'])) {
				$order_by = $filter['order_by'];
			}
			else {
				$order_by = 'DESC';
			}

			if (isset($filter['sort'])) {
				$sql .= ' ORDER BY ' . $filter['sort'];
			}

			if (isset($filter['limit'])) {
				$sql .= ' LIMIT ' . $filter['limit'];
			}
		}

		$query = $this->db->query($sql);
		return $query->rows;
	}

	/**
	 * Update JNE price
	 * @param  $sql sql
	 */
	public function updateCityJne($sql) {
		return $this->db->query($sql);
	}
}