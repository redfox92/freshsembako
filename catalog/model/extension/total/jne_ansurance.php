<?php
class ModelExtensionTotalJneAnsurance extends Model {
	public function getTotal($total) {
		$this->load->language('extension/total/jne_ansurance');

		$jne_ansurance_status = $this->config->get('jne_ansurance_status');
				
		if ($jne_ansurance_status) {
			$total['totals'][] = array(
				'code'       => 'jne_ansurance',
				'title'      => $this->language->get('text_ansurance'),
				'value'      => $this->config->get('jne_ansurance'),
				'sort_order' => $this->config->get('jne_ansurance_sort_order')
			);
		
			$total['total'] += $this->config->get('jne_ansurance');
		}
	}
}