<?php
class ModelExtensionTotalPorterInsurance extends Model {
	public function getTotal($total) {
		if (isset($this->session->data['shipping_method'])) {
			$this->load->language('extension/total/porter_insurance');

			if (isset($this->session->data['shipping_method']['insurance']) && ($this->session->data['shipping_method']['code'] == 'sameday_jkt_above_15kg.sameday_jkt_above_15kg' || $this->session->data['shipping_method']['code'] == 'sameday_jkt_under_15kg.sameday_jkt_under_15kg')) {
				$porter_insurance_status = $this->config->get('porter_insurance_status');

				$subtotal = $this->cart->getSubTotal();
				if ($porter_insurance_status && $subtotal >= $this->config->get('porter_insurance_min_subtotal')) {
					$price = 0;


					$price = ceil(($subtotal * $this->config->get('porter_insurance_percent'))/100);

					$total['totals'][] = array(
						'code'       => 'porter_insurance',
						'title'      => $this->language->get('text_ansurance'),
						'value'      => $price,
						'sort_order' => $this->config->get('porter_insurance_sort_order')
					);
				
					$total['total'] += $price;
				}
			}

		}
	}
}