<?php
class ModelExtensionTotalUniqueTotalAmount extends Model {
	public function getTotal($total) {
		$this->load->language('extension/total/unique_total_amount');

		if ($this->cart->hasShipping() && isset($this->session->data['shipping_method'])) {
			$sub_total = $this->cart->getSubTotal();

			// if (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) {
			// 	foreach ($this->session->data['vouchers'] as $voucher) {
			// 		$sub_total += $voucher['amount'];
			// 	}
			// }

			$query = "SELECT order_id FROM `" . DB_PREFIX . "order` ORDER BY order_id DESC LIMIT 1";
			$order_id = empty($this->db->query($query)->row) ? 1 : $this->db->query($query)->row['order_id']+1;

			// hitung karakter
			$chr_length = strlen($order_id);

			// ambil 3 karakter terakhir
			if ($chr_length == 3) {
				$final_id = $order_id;
			}
			elseif ($chr_length < 3) {
				$final_id = $order_id;			
			}
			elseif ($chr_length > 3) {
				$final_id = substr($order_id, -3);
			}

			$unique_total_amount = $final_id;

			$total['totals'][] = array(
				'code'       => 'unique_total_amount',
				'title'      => $this->language->get('text_unique_total_amount'),
				'value'      => $unique_total_amount,
				'sort_order' => $this->config->get('unique_total_amount_sort_order')
			);

			$total['total'] += $unique_total_amount;
		}

	}
}