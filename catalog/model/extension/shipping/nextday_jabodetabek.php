<?php
class ModelExtensionShippingNextdayJabodetabek extends Model {
	function getQuote($address) {
		$this->load->language('extension/shipping/nextday_jabodetabek');

		$status = true;
		$disabled = false;
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('nextday_jabodetabek_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (!$this->config->get('nextday_jabodetabek_geo_zone_id')) {
			// $status = true;
			$disabled = false;
		} elseif ($query->num_rows) {
			// $status = true;
			$disabled = false;
		} else {
			// $status = false;
			$disabled = true;
		}

		$dry_food = true;
		$this->load->model('catalog/product');
		foreach ($this->cart->getProducts() as $product) {
			$product_info = $this->model_catalog_product->getProduct($product['product_id']);
			if ($product_info['type_food']) {
				$dry_food = false;
			}
		}

		if (!$dry_food) {
			$disabled = true;
		}

		$method_data = array();

		if ($status) {
			$quote_data = array();

			$quote_data['nextday_jabodetabek'] = array(
				'code'         => 'nextday_jabodetabek.nextday_jabodetabek',
				'title'        => sprintf($this->language->get('text_description'), $this->config->get('nextday_jabodetabek_price')),
				'cost'         => ceil($this->cart->getWeight()/1000) * $this->config->get('nextday_jabodetabek_price'),
				'tax_class_id' => 0,
				'text'         => $this->currency->format(ceil($this->cart->getWeight()/1000) * $this->config->get('nextday_jabodetabek_price'), $this->session->data['currency'])
			);

			$method_data = array(
				'code'       => 'nextday_jabodetabek',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('nextday_jabodetabek_sort_order'),
				'error'      => false,
				'disabled'	 => $disabled
			);
		}

		return $method_data;
	}
}