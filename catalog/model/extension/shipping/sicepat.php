<?php
class ModelExtensionShippingSicepat extends Model {
	function getQuote($address) {
		$this->load->language('extension/shipping/sicepat');
		
		$disabled = false;
		$status = true;
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('sicepat_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
	
		if (!$this->config->get('sicepat_geo_zone_id')) {
			$disabled = false;
			// $status = true;
		} elseif ($query->num_rows) {
			$disabled = false;
			// $status = true;
		} else {
			$disabled = true;
			// $status = false;
		}

		$method_data = array();
		// $status = false; //shipping not save
		if ($status) {
			
			$cost = 0;
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "city WHERE city_id = '" . (int)$address['city_id'] . "'");

			
			$total_weight = ceil($this->weight->convert($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->config->get('config_sicepat_weight_class_id')));

			$type_ship = '';
			if ($query->num_rows) {
				if ($total_weight < 5) {
					$cost = $query->row['sicepat'];
					$type_ship = 'Regular';
				} else {
					$cost = $query->row['sicepat_cargo'];
					$type_ship = 'Cargo';
				}
				if ($cost == 0)
					$disabled = true;
			}

			$total_cost = $total_weight * $cost;

			$insurance = array();

			$quote_data = array();
			$quote_data['sicepat'] = array(
				'code'         => 'sicepat.sicepat',
				'title'        => sprintf($this->language->get('text_description'), $type_ship, $total_weight), 
				'cost'         => $total_cost,
				'tax_class_id' => 0,
				'text'         => $this->currency->format($total_cost, $this->session->data['currency'])
			);

			$method_data = array(
				'code'       => 'sicepat',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('sicepat_sort_order'),
				'error'      => false,
				'disabled'	 => $disabled,
				'insurance'	 => $insurance
			);
		}

		return $method_data;
	}
}
?>