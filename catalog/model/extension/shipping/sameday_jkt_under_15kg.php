<?php

class ModelExtensionShippingSamedayJktUnder15kg extends Model {

	function getQuote($address) {

		$this->load->language('extension/shipping/sameday_jkt_under_15kg');



		$disabled = false;

		$status = true;



		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('sameday_jkt_under_15kg_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");



		if (!$this->config->get('sameday_jkt_under_15kg_geo_zone_id')) {

			// $status = true;

			$disabled = false;

		} elseif ($query->num_rows) {

			// $status = true;

			$disabled = false;

		} else {

			$disabled = true;

			// $status = false;

		}



		$weight = ceil($this->weight->convert($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->config->get('config_sicepat_weight_class_id')));



		if ($weight > $this->config->get('sameday_jkt_under_15kg_weight')) {

			$disabled = true;

		}



		$method_data = array();



		if ($status) {

			$quote_data = array();



			$insurance_price = 0;



			$subtotal = $this->cart->getSubTotal();



			if ($subtotal >= 1000000) {

				$insurance_price = ceil(($subtotal * 0.25)/100);

			}



			$cost = 0;

			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "city WHERE city_id = '" . (int)$address['city_id'] . "'");

			if ($query->num_rows)

				$cost = $query->row['porter_15kg'];

			if ($cost == 0) {
				$disabled = true;
			}

			$quote_data['sameday_jkt_under_15kg'] = array(

				'code'         => 'sameday_jkt_under_15kg.sameday_jkt_under_15kg',

				'title'        => sprintf($this->language->get('text_description'), $address['zone'] . ', ' . $address['city'], $this->config->get('sameday_jkt_under_15kg_weight')),

				'cost'         => $cost,

				'tax_class_id' => 0,

				'text'         => $this->currency->format($cost, $this->session->data['currency'])

			);



			$method_data = array(

				'code'       => 'sameday_jkt_under_15kg',

				'title'      => $this->language->get('text_title'),

				'quote'      => $quote_data,

				'sort_order' => $this->config->get('sameday_jkt_under_15kg_sort_order'),

				'error'      => false,

				'disabled'	 => $disabled,

				'insurance'	 => $insurance_price

			);

		}



		return $method_data;

	}

}