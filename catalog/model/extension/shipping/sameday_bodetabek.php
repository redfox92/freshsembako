<?php
class ModelExtensionShippingSamedayBodetabek extends Model {
	function getQuote($address) {
		$this->load->language('extension/shipping/sameday_bodetabek');

		$status = true;
		$disabled = false;
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('sameday_bodetabek_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (!$this->config->get('sameday_bodetabek_geo_zone_id')) {
			// $status = true;
			$disabled = false;
		} elseif ($query->num_rows) {
			// $status = true;
			$disabled = false;
		} else {
			// $status = false;
			$disabled = true;
		}

		$weight = $this->cart->getWeight() / 1000;

		if ($weight <= $this->config->get('sameday_bodetabek_weight') || $weight > $this->config->get('sameday_bodetabek_max_weight')) {
			$disabled = true;
		}

		$method_data = array();

		if ($status) {
			$quote_data = array();

			$quote_data['sameday_bodetabek'] = array(
				'code'         => 'sameday_bodetabek.sameday_bodetabek',
				'title'        => sprintf($this->language->get('text_description'), $this->config->get('sameday_bodetabek_weight'), $this->config->get('sameday_bodetabek_max_weight')),
				'cost'         => $this->config->get('sameday_bodetabek_price'),
				'tax_class_id' => 0,
				'text'         => $this->currency->format($this->config->get('sameday_bodetabek_price'), $this->session->data['currency'])
			);

			$method_data = array(
				'code'       => 'sameday_bodetabek',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('sameday_bodetabek_sort_order'),
				'error'      => false,
				'disabled'	 => $disabled
			);
		}

		return $method_data;
	}
}