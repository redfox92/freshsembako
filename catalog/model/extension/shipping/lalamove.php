<?php

class ModelExtensionShippingLalamove extends Model {
	function getQuote($address) {
		$this->load->language('extension/shipping/lalamove');

		$status = true;
		$disabled = false;
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('lalamove_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (!$this->config->get('lalamove_geo_zone_id')) {
			// $status = true;
			$disabled = false;
		} elseif ($query->num_rows) {
			// $status = true;
			$disabled = false;
		} else {
			// $status = false;
			$disabled = true;
		}

		$weight = ceil($this->weight->convert($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->config->get('config_sicepat_weight_class_id')));

		if ($weight > $this->config->get('lalamove_max_weight')) {
			$disabled = true;
		}

		$method_data = array();

		if ($status) {
			$quote_data = array();

			$cost = 0;
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "city WHERE city_id = '" . (int)$address['city_id'] . "'");
			if ($query->num_rows)
				$cost = $query->row['lalamove'];

			if ($cost == 0) {
				$disabled = true;
			}
			// hardcode
			// $cost = 15000;
			$total_weight = ceil($this->weight->convert($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->config->get('config_jneyes_weight_class_id')));

			$quote_data['lalamove'] = array(
				'code'         => 'lalamove.lalamove',
				'title'        => sprintf($this->language->get('text_description'), $address['zone'] . ', ' . $address['city'], $total_weight),
				'cost'         => $cost,
				'tax_class_id' => 0,
				'text'         => $this->currency->format($cost, $this->session->data['currency'])
			);

			$method_data = array(
				'code'       => 'lalamove',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('lalamove_sort_order'),
				'error'      => false,
				'disabled'	 => $disabled,
			);
		}

		return $method_data;
	}
}