<?php
/***************
Extension Shipping GOJEK -
Menggunakan API Google Maps (distancematrix),
output yaitu jarak antara 2 lokasi, antara lokasi Store ke lokasi Customer di Address_2
dalam satuan Meter.

--Kyky's Created--
***************/

class ModelExtensionShippingGojek extends Model {
	function getQuote($address) {
		
		$this->load->language('extension/shipping/gojek');
				
		$method_data = array();
	
		$total_cost = 0;
		$total_weight = ceil($this->cart->getWeight());

		$origin_address = urlencode($this->config->get('config_address')); 
		$destination_address = urlencode($address['address_2']);

		$urlApi = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$origin_address."&destinations=".$destination_address."&language=id-ID";	 
		$result = file_get_contents($urlApi);		 
		$data   = json_decode($result, true);
		
		if($data['status'] == 'OK'){

			//distance in Meter
			$distance_value = $data['rows'][0]['elements'][0]['distance']['value'];			
			
			//maksimum 30 KM
			if($distance_value < 30000){
				$this->load->model('setting/setting');
				$gojek_setting = $this->model_setting_setting->getSetting('gojek');
				$total_cost = ($distance_value/1000) * $gojek_setting['gojek_fee'];			
			
				$quote_data = array();
				$quote_data['gojek'] = array(
					'code'         => 'gojek.gojek',
					'title'        => sprintf($this->language->get('text_description'), $address['address_2'].'('. ($distance_value/1000) .' KM)', $total_weight), 
					'cost'         => $total_cost,
					'tax_class_id' => 0,
					'text'         => $this->currency->format($total_cost, $this->session->data['currency'])
				);

				$method_data = array(
					'code'       => 'gojek',
					'title'      => $this->language->get('text_title'),
					'quote'      => $quote_data,
					'sort_order' => $this->config->get('gojek_sort_order'),
					'error'      => false
				);			
			}
			else{
				$quote_data = array();
				$quote_data['gojek'] = array(
					'code'         => 'gojek.gojek',
					'title'        => "Your location is too far distance (Maximum: 30KM)", 
					'cost'         => $total_cost,
					'tax_class_id' => 0,
					'text'         => $this->currency->format($total_cost, $this->session->data['currency'])
				);

				$method_data = array(
					'code'       => 'gojek',
					'title'      => $this->language->get('text_title'),
					'quote'      => $quote_data,
					'sort_order' => $this->config->get('gojek_sort_order'),
					'error'      => sprintf($this->language->get('text_not_available'), $address['address_2'])
				);	
			}
		}

		return $method_data;
	}
}
?>
