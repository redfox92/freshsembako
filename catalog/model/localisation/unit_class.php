<?php
class ModelLocalisationUnitClass extends Model {

	public function getUnitClass($unit_class_id)
	{
		$sql = "SELECT * FROM `" . DB_PREFIX . "unit_class` WHERE `unit_class_id` = " . (int)$unit_class_id;
		return $this->db->query($sql)->row;
	}
}