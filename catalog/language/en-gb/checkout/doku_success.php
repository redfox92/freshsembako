<?php
// Heading
$_['heading_title'] = 'Pesanan anda sudah diproses!';

// Text
$_['text_customer']        = '<p>Pesanan anda sudah berhasil diproses!</p><p>Anda dapat melihat riwayat belanja anda dengan cara melakukan klik pada <a href="%s">profile saya</a> lalu klik <a href="%s">daftar pesanan</a>.</p><p>Jika anda membutuhkan resi belanja, anda dapat mendownloadnya dengan cara klik pada halaman akun <a href="%s">downloads</a>.</p><p>Jika ada yang mau ditanyakan, anda dapat langsung mengkontak ke <a href="%s">pemilik toko</a>.</p><p>Terima kasih telah berbelanja di Moremall!</p>';
$_['text_guest']           = '<p>Pesanan anda sudah berhasil diproses!</p><p>Silahkan tanyakan pertanyaan anda <a href="%s">di sini</a>.</p><p>Terima kasih sudah berbelanja dengan kami!</p>';
$_['text_basket']   = 'Shopping Cart';
$_['text_checkout'] = 'Checkout';
$_['text_success']  = 'Success Payment';

?>