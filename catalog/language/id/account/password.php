<?php
// Heading
$_['heading_title']  = 'Ganti kata sandi';

// Text
$_['text_account']   = 'Akun';
$_['text_password']  = 'Kata sandi Anda';
$_['text_success']   = 'Berhasil: Kata sandi Anda telah berhasil diperbarui.';

// Entry
$_['entry_password'] = 'Kata Sandi';
$_['entry_confirm']  = 'Konfirmasi Kata Sandi';

// Error
$_['error_password'] = 'Kata sandi harus antara 4 dan 20 karakter!';
$_['error_confirm']  = 'konfirmasi kata sandi tidak cocok sandi!';