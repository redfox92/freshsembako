<?php
// Heading
$_['heading_title'] = 'My Wish List';

// Text
$_['text_account']  = 'Akun';
$_['text_instock']  = 'Tersedia';
$_['text_wishlist'] = 'Daftar keinginan (%s)';
$_['text_login']    = 'Kamu harus <a href="%s" class="text-green hvr-orange">login</a> atau <a href="%s" class="text-green hvr-orange">buat sebuah akun</a> untuk menyimpan <a href="%s" class="text-green hvr-orange">%s</a> Daftar Keinginan <a href="%s" class="text-green hvr-orange"> Anda </a>!';
$_['text_success']  = 'Berhasil: Anda telah ditambahkan <a href="%s" class="text-green hvr-orange">%s</a> Daftar Keinginan <a href="%s" class="text-green hvr-orange">Anda</a>!';
$_['text_remove']   = 'Berhasil: Anda telah mengubah daftar keinginan Anda!';
$_['text_empty']    = 'Daftar keinginan anda kosong.';

// Column
$_['column_image']  = 'Image';
$_['column_name']   = 'Nama Produk';
$_['column_model']  = 'Model';
$_['column_stock']  = 'Stok';
$_['column_price']  = 'Unit Price';
$_['column_action'] = 'Aksi';