<?php
// Heading
$_['heading_title']        = 'Registrasi Akun';

// Text
$_['text_account']         = 'Akun';
$_['text_register']        = 'Daftar';
$_['text_account_already'] = 'Jika Anda sudah memiliki account dengan kami, silahkan login di <a href="%s" class="text-orange hvr-green">halaman login</a>.';
$_['text_your_details']    = 'Data Pribadi Anda';
$_['text_your_address']    = 'Alamat Anda';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Password Anda';
$_['text_agree']           = 'Saya telah membaca dan setuju dengan <a href="%s" class="text-orange hvr-green"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Pelanggan Grup';
$_['entry_fullname']      = 'Nama Lengkap';
$_['entry_firstname']      = 'Nama Depan';
$_['entry_lastname']       = 'Nama Belakang';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Telepon';
$_['entry_fax']            = 'Fax';
$_['entry_company']        = 'Perusahaan';
$_['entry_address_1']      = 'Alamat 1';
$_['entry_address_2']      = 'Alamat 2';
$_['entry_postcode']       = 'Kodepos';
$_['entry_city']           = 'Kecamatan';
$_['entry_country']        = 'Provinsi';
$_['entry_zone']           = 'Kabupaten / Kota';
$_['entry_newsletter']     = 'Subscribe';
$_['entry_password']       = 'Kata Sandi';
$_['entry_confirm']        = 'Konfirmasi Kata Sandi';

// Error
$_['error_exists']       = 'Peringatan: Alamat email sudah terdaftar!';
$_['error_firstname']    = 'Nama depan harus antara 1 dan 32 karakter!';
$_['error_lastname']     = 'Nama belakang harus antara 1 dan 32 karakter!';
$_['error_email']        = 'Alamat Email Tidak Valid!';
$_['error_telephone']    = 'Telepon harus antara 3 dan 32 karakter!';
$_['error_address_1']    = 'Alamat 1 harus antara 3 dan 128 karakter!';
$_['error_postcode']     = 'Kode pos harus antara 2 dan 10 karakter!';
$_['error_country']      = 'Silakan pilih provinsi!';
$_['error_zone']         = 'Silakan pilih kota / kabupaten';
$_['error_city']         = 'Silakan pilih kecamatan';
$_['error_custom_field'] = '%s wajib!';
$_['error_password']     = 'Password harus antara 4 dan 20 karakter!';
$_['error_confirm']      = 'konfirmasi kata sandi tidak cocok!';
$_['error_agree']        = 'Peringatan: Anda harus menyetujui %s!';

//custome text
$_['error_fullname']      = 'Nama lengkap harus antara 1 dan 32 karakter!';