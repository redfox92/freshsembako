<?php
// Heading
$_['heading_title']      = 'Poin Hadiah Anda';

// Column
$_['column_date_added']  = 'Tanggal Ditambahkan';
$_['column_description'] = 'Deskripsi';
$_['column_points']      = 'Poin';

// Text
$_['text_account']       = 'Akun';
$_['text_reward']        = 'Poin Hadiah';
$_['text_total']         = 'Poin total anda adalah <span class="account-point">%s</span> poin';
$_['text_empty']         = 'Anda tidak memiliki poin hadiah!';