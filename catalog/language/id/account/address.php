<?php
// Heading
$_['heading_title']        = 'Buku Alamat';

// Text
$_['text_account']         = 'Akun';
$_['text_address_book']    = 'Entry Buku Alamat';
$_['text_edit_address']    = 'Edit Alamat';
$_['text_add']             = 'Alamat Anda telah berhasil dimasukkan';
$_['text_edit']            = 'Alamat Anda telah berhasil diperbarui';
$_['text_delete']          = 'Alamat Anda telah berhasil dihapus';
$_['text_empty']           = 'Anda tidak memiliki alamat di akun Anda.';

// Entry
$_['entry_fullname']       = 'Nama Lengkap';
$_['entry_firstname']      = 'Nama Depan';
$_['entry_lastname']       = 'Nama Belakang';
$_['entry_company']        = 'Perusahaan';
$_['entry_address_1']      = 'Alamat Lengkap';
$_['entry_address_2']      = 'Alamat 2';
$_['entry_postcode']       = 'Kode Pos';
$_['entry_country']        = 'Provinsi';
$_['entry_zone']           = 'Kabupaten / Kota';
$_['entry_city']           = 'Kecamatan';
$_['entry_default']        = 'Alamat Default';

// Error
$_['error_delete']       = 'Peringatan: Anda harus memiliki setidaknya satu alamat!';
$_['error_default']      = 'Peringatan: Anda tidak dapat menghapus alamat default Anda!';
$_['error_fullname']     = 'Nama lengkap harus antara 1 dan 32 karakter!';
$_['error_firstname']    = 'Nama depan harus antara 1 dan 32 karakter!';
$_['error_lastname']     = 'Nama belakang harus antara 1 dan 32 karakter!';
$_['error_vat']          = 'Jumlah PPN tidak valid!';
$_['error_address_1']    = 'Alamat harus antara 3 dan 128 karakter!';
$_['error_postcode']     = 'Kode pos harus antara 2 dan 10 karakter!';
$_['error_country']      = 'Silakan pilih provinsi!';
$_['error_zone']         = 'Silakan pilih kota / kabupaten';
$_['error_city']         = 'Silakan pilih kecamatan';
$_['error_custom_field'] = '%s diperlukan!';