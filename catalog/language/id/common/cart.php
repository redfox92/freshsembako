<?php
// Text
$_['text_items']     = '%s item(s) - %s';
$_['text_empty']     = 'Keranjang belanja anda kosong!';
$_['text_cart']      = 'Checkout';
$_['text_checkout']  = 'Bayar';
$_['text_recurring'] = 'Profil Pembayaran';
$_['text_cart_desc'] = 'Silahkan cek kembali barang belanjaan anda.';