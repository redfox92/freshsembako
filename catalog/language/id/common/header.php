<?php
// Text
$_['text_home']          = 'Beranda';
$_['text_wishlist']      = 'Daftar Keinginan (%s)';
$_['text_shopping_cart'] = 'Keranjang Belanja';
$_['text_category']      = 'Kategori';
$_['text_account']       = 'Akun Saya';
$_['text_register']      = 'Daftar';
$_['text_login']         = 'Masuk';
$_['text_order']         = 'Riwayat Pesanan';
$_['text_address']       = 'Buku Alamat';
$_['text_transaction']   = 'Transaksi';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Keluar';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Pencarian';
$_['text_all']           = 'Tunjukkan Semua';
$_['text_shop']          = 'Belanja';
$_['text_promo']         = 'Promo';
$_['text_menu']          = 'Menu';
$_['text_logged']		  = 'Hi, %s';
$_['text_payment_confirmation'] = 'Konfirmasi Pembayaran';
$_['text_reward'] = 'Poin Hadiah';

$_['text_register_modal'] = "Registrasi Akun";
$_['text_account_already_modal'] = "Sudah menjadi pelanggan? <span class='xs-block'>Silahkan <a class='text-green hvr-orange' href='javascript:;' data-toggle='modal' data-target='#login-modal'>login</a> disini.</span>";
$_['text_account_register_modal'] = "Belum memiliki akun? <span class='xs-block'>Silahkan <a class='text-green hvr-orange' href='javascript:;' data-toggle='modal' data-target='#register-modal'>daftar</a> disini.</span>";
$_['text_customer_area'] = "Area Pelanggan";
$_['text_forgot_password'] = "Lupa password? <a class='text-green hvr-orange' href='javascript:;' data-toggle='modal' data-target='#forgot-modal'>Klik disini</a>";