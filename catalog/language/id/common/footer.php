<?php
// Text
$_['text_home']            = 'Beranda';
$_['text_information']     = 'Informasi';
$_['text_service']         = 'Pelayanan Pelanggan';
$_['text_extra']           = 'Ekstra';
$_['text_contact']         = 'Hubungi Kami';
$_['text_follow']          = 'Ikuti Kami';
$_['text_return']          = 'Pengembalian';
$_['text_sitemap']         = 'Site Map';
$_['text_manufacturer']    = 'Brands';
$_['text_voucher']         = 'Gift Certificates';
$_['text_affiliate']       = 'Afiliasi';
$_['text_special']         = 'Specials';
$_['text_account']         = 'Akun Saya';
$_['text_order']           = 'Riwayat Pesanan';
$_['text_wishlist']        = 'Daftar Keinginan';
$_['text_newsletter']      = 'Newsletter';
$_['text_quick_links']     = 'Tautan Langsung';
$_['text_powered']         = 'Copyright %s &copy; %s';
$_['text_shop']            = 'Belanja';
$_['text_promo']           = 'Promo';
$_['text_grosir']          = 'Grosir';
$_['text_payment_method']  = 'Metode Pembayaran';
$_['text_shipping_method'] = 'Metode Pengiriman';