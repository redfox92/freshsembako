<?php

// Entry
$_['entry_email']       = 'Masukkan alamat email Anda';

// Text
$_['text_title']       = 'Berlangganan Newsletter Kami';
$_['text_description'] = 'Dapatkan tawaran dan promo terbaik kami setiap hari dengan berlangganan newsletter kami';
$_['text_subscribe']   = 'Berlangganan';
$_['text_add']         = 'Terima kasih sudah berlangganan';

// Error
$_['error_exists'] = 'Alamat Email ini sudah terdaftar';
$_['error_email']  = 'Alamat Email ini Tidak Valid!';