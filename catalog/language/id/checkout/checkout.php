<?php
// Heading
$_['heading_title']                  = 'Checkout';

// Text
$_['text_cart']                      = 'Keranjang Belanja';
$_['text_checkout_option']           = '<span class="yellow">Step %s:</span> Langkah Checkout';
$_['text_checkout_account']          = '<span class="yellow">Step %s:</span> Akun &amp; Detail Penagihan';
$_['text_checkout_payment_address']  = '<span class="yellow">Step %s:</span> Detail Penagihan';
$_['text_checkout_shipping_address'] = '<span class="yellow">Step %s:</span> Detail Pengiriman';
$_['text_checkout_shipping_method']  = '<span class="yellow">Step %s:</span> Metode Pengiriman';
$_['text_checkout_payment_method']   = '<span class="yellow">Step %s:</span> Metode Pembayaran';
$_['text_checkout_confirm']          = '<span class="yellow">Step %s:</span> Konfirmasi Pesanan';
$_['text_modify']                    = 'Modifikasi &raquo;';
$_['text_new_customer']              = 'Pelanggan Baru';
$_['text_returning_customer']        = 'Pelanggan Lama';
$_['text_checkout']                  = 'Pilihan Checkout:';
$_['text_i_am_returning_customer']   = 'Saya adalah pelanggan lama';
$_['text_register']                  = 'Registrasi Akun';
$_['text_guest']                     = 'Guest Checkout';
// $_['text_register_account']          = 'Dengan membuat akun Anda akan dapat berbelanja lebih cepat, up to date pada status pesanan, dan bisa meninjau ulang pesanan yang telah Anda buat sebelumnya.';
$_['text_register_account']          = 'Dengan melakukan Registrasi Akun, Anda akan mendapatkan lebih banyak keuntungan seperti: poin belanja, mendapatkan promo terbaru, melihat status pesanan, dan lainnya.';
$_['text_forgotten']                 = 'Lupa Kata Sandi?';
$_['text_your_details']              = 'Data Pribadi Anda';
$_['text_your_address']              = 'Alamat Anda';
$_['text_your_password']             = 'Kata sandi Anda';
$_['text_agree']                     = 'Saya telah membaca dan setuju dengan <a href="%s" target="_blank" class="text-green hvr-orange"><b>%s</b></a>';
$_['text_address_new']               = 'Saya ingin menggunakan alamat baru';
$_['text_address_existing']          = 'Saya ingin menggunakan alamat yang ada';
$_['text_shipping_method']           = 'Silakan pilih metode pengiriman yang lebih disukai untuk digunakan pada pesanan ini.';
$_['text_payment_method']            = 'Pilih metode pembayaran yang diinginkan untuk digunakan pada pesanan ini.';
// $_['text_comments']                  = 'Tambahkan Komentar Tentang Pesanan Anda';
$_['text_comments']                  = 'Komentar';
$_['text_recurring_item']            = 'Item Berulang';
$_['text_payment_recurring']         = 'Profil Pembayaran';
$_['text_trial_description']         = '%s setiap %d %s(s) untuk %d pembayaran(s) kemudian';
$_['text_payment_description']       = '%s setiap %d %s(s) untuk %d pembayaran(s)';
$_['text_payment_cancel']            = '%s setiap %d %s(s) sampai dibatalkan';
$_['text_day']                       = 'Hari';
$_['text_week']                      = 'Minggu';
$_['text_semi_month']                = 'Setengah Bulan';
$_['text_month']                     = 'Bulan';
$_['text_year']                      = 'Tahun';

// Column
$_['column_name']                    = 'Nama Produk';
$_['column_model']                   = 'Model';
$_['column_quantity']                = 'Quantity';
$_['column_price']                   = 'Unit Price';
$_['column_total']                   = 'Total';

// Entry
$_['entry_email_address']            = 'Alamat Email';
$_['entry_email']                    = 'E-Mail';
$_['entry_password']                 = 'Kata Sandi';
$_['entry_confirm']                  = 'Konfirmasi Kata Sandi';
$_['entry_fullname']                 = 'Nama Lengkap';
$_['entry_firstname']                = 'Nama Depan';
$_['entry_lastname']                 = 'Nama Belakang';
$_['entry_telephone']                = 'Telepon';
$_['entry_fax']                      = 'Fax';
$_['entry_address']                  = 'Pilih Alamat';
$_['entry_company']                  = 'Perusahaan';
$_['entry_customer_group']           = 'Pelanggan Grup';
$_['entry_address_1']                = 'Alamat Lengkap';
$_['entry_address_2']                = 'Alamat 2';
$_['entry_postcode']                 = 'Kodepos';
$_['entry_country']                  = 'Provinsi';
$_['entry_zone']                     = 'Kabupaten / Kota';
$_['entry_city']                     = 'Kecamatan';
$_['entry_newsletter']               = 'Saya ingin berlangganan %s newsletter.';
$_['entry_shipping'] 	             = 'Pengiriman dan alamat penagihan sama.';

// Error
$_['error_warning']                  = 'Ada masalah saat mencoba untuk memproses pesanan Anda! Jika masalah berlanjut coba memilih metode pembayaran yang berbeda atau Anda dapat menghubungi pemilik toko oleh<a href="%s" class="text-green hvr-orange">klik di sini</a>.';
$_['error_login']                    = 'Peringatan: Tidak cocok untuk E-Mail dan / atau Password.';
$_['error_attempts']                 = 'Peringatan: Akun Anda telah melampaui jumlah memungkinkan usaha login. Silakan coba lagi dalam 1 jam.';
$_['error_approved']                 = 'Peringatan: Akun Anda memerlukan persetujuan sebelum Anda dapat login.';
$_['error_exists']                   = 'Peringatan: E-Mail sudah terdaftar!';
$_['error_fullname']                 = 'Nama lengkap harus antara 1 dan 32 karakter!';
$_['error_firstname']                = 'Nama depan harus antara 1 dan 32 karakter!';
$_['error_lastname']                 = 'Nama belakang harus antara 1 dan 32 karakter!';
$_['error_email']                    = 'Alamat email tidak valid!';
$_['error_telephone']                = 'Telepon harus antara 3 dan 32 karakter!';
$_['error_password']                 = 'Password harus antara 4 dan 20 karakter!';
$_['error_confirm']                  = 'Konfirmasi kata sandi tidak cocok sandi!';
$_['error_address_1']                = 'Alamat 1 harus antara 3 dan 128 karakter!';
$_['error_postcode']                 = 'Kode pos harus antara 2 dan 10 karakter!';
$_['error_country']                  = 'Silakan pilih provinsi!';
$_['error_zone']                     = 'Silakan pilih kota / kabupaten';
$_['error_city']                     = 'Silakan pilih kecamatan';
$_['error_agree']                    = 'Peringatan: Anda harus menyetujui %s!';
$_['error_address']                  = 'Peringatan: Anda harus memilih alamat!';
$_['error_shipping']                 = 'Peringatan: Metode Pengiriman diperlukan!';
$_['error_no_shipping']              = 'Peringatan: Tidak ada pilihan Pengiriman tersedia. Silahkan <a href="%s" class="text-green hvr-orange">hubungi kami</a> untuk bantuan!';
$_['error_payment']                  = 'Peringatan: Metode Pembayaran diperlukan!';
$_['error_no_payment']               = 'Peringatan: Tidak ada pilihan pembayaran yang tersedia. Silahkan <a href="%s" class="text-green hvr-orange">hubungi kami</a> untuk bantuan!';
$_['error_custom_field']             = '%s diperlukan!';