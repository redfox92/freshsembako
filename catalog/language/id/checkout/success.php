<?php
// Heading
$_['heading_title']        = 'Pesanan Anda telah ditempatkan!';

// Text
$_['text_basket']          = 'Keranjang Belanja';
$_['text_checkout']        = 'Checkout';
$_['text_success']         = 'Berhasil';
$_['text_customer_success_2'] = '<p>Pesanan Anda telah berhasil diproses!</p>';
$_['text_customer_success_3'] = '<p>Anda dapat melihat riwayat pesanan Anda dengan pergi ke <a href="%s" class="text-green hvr-orange">akun saya</a> halaman dan dengan mengklik <a href="%s" class="text-green hvr-orange">riwayat</a>.</p>';
$_['text_customer_success_4'] = '<p>Jika pembelian Anda memiliki mendownload terkait, Anda bisa pergi ke account <a href="%s" class="text-green hvr-orange">downloads</a> Halaman untuk melihatnya.</p>';
$_['text_customer_success_5'] = '<p>Ajukan pertanyaan apapun yang Anda miliki ke <a href="%s" class="text-green hvr-orange">%s</a>.</p>';
$_['text_customer_success_6'] = '<p>Terima kasih untuk berbelanja dengan kami secara online!</p>';
$_['text_customer']        = '
<p>Pesanan Anda telah berhasil diproses!</p>
<p>Anda dapat melihat riwayat pesanan Anda dengan pergi ke <a href="%s" class="text-green hvr-orange">akun saya</a> halaman dan dengan mengklik <a href="%s" class="text-green hvr-orange">riwayat</a>.</p>
<p>Jika pembelian Anda memiliki mendownload terkait, Anda bisa pergi ke account <a href="%s" class="text-green hvr-orange">downloads</a> Halaman untuk melihatnya.</p>
<p>Ajukan pertanyaan apapun yang Anda miliki ke <a href="%s" class="text-green hvr-orange">pemilik toko</a>.</p>
<p>Terima kasih untuk berbelanja dengan kami secara online!</p>';
$_['text_guest']           = '
<p>Pesanan Anda telah berhasil diproses!</p>
<p>Ajukan pertanyaan apapun yang Anda miliki ke <a href="%s" class="text-green hvr-orange">pemilik toko</a>.</p>
<p>Terima kasih untuk berbelanja dengan kami secara online!</p>';
$_['text_guest_2'] = '<p>Pesanan Anda telah berhasil diproses!</p>';
$_['text_guest_3'] = '<p>Ajukan pertanyaan apapun yang Anda miliki ke <a href="%s" class="text-green hvr-orange">%s</a>.</p>';
$_['text_guest_4'] = '<p>Terima kasih untuk berbelanja dengan kami secara online!</p>';