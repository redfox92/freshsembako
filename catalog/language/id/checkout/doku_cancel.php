<?php
// Heading
// $_['heading_title'] = 'Your Payment Process Fails!';
$_['heading_title'] = 'Pembayaran gagal';

// Text
$_['text_customer'] = 'Pembayaran anda gagal diproses.<br>
Silahkan coba kembali menggunakan metode pembayaran yang lain.<br>
Jika masalah terus berlanjut, silahkan hubungi layanan konsumen.<br><br>
Terima kasih sudah berbelanja dengan kami!';

$_['text_guest'] = 'Pembayaran anda gagal diproses.<br>
Silahkan coba kembali menggunakan metode pembayaran yang lain.<br>
Jika masalah terus berlanjut, silahkan hubungi layanan konsumen.<br><br>
Terima kasih sudah berbelanja dengan kami!';

// $_['text_customer'] = '<p>Your Payment Process Fails..!</p><p>You can view your order history by going to the <a href="%s">my account</a> page and by clicking on <a href="%s">history</a>.</p><p>Please direct any questions you have to the <a href="%s">store owner</a>.</p><p>Thanks for shopping with us online!</p>';
// $_['text_guest']    = '<p>Your Payment Process Fails..!</p><p>Please direct any questions you have to the <a href="%s">store owner</a>.</p><p>Thanks for shopping with us online!</p>';
$_['text_basket']   = 'Shopping Cart';
$_['text_checkout'] = 'Checkout';
$_['text_success']  = 'Fail Payment';
?>