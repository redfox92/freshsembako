<?php

class ControllerExtensionModuleLatest extends Controller {

	public function index($setting) {



		$this->document->addStyle('catalog/view/javascript/jquery/owl.carousel/owl.carousel.min.css');

		$this->document->addStyle('catalog/view/javascript/jquery/owl.carousel/owl.theme.default.min.css');

		$this->document->addScript('catalog/view/javascript/jquery/owl.carousel/owl.carousel.min.js');



		$this->load->language('extension/module/latest');



		$data['button_cart'] = $this->language->get('button_cart');

		$data['button_view_all'] = $this->language->get('button_view_all');



		$this->load->model('catalog/product');



		$this->load->model('catalog/category');



		$this->load->model('tool/image');



		$categories = $setting['category'];



		foreach ($categories as $key => $category_id) {

			$category_info = $this->model_catalog_category->getCategory($category_id);

			$products = array();

			if (!empty($category_info)) {

				$filter_data_product = array(

					'filter_category_id' => $category_info['category_id'],

					'sort'               => 'p.sort_order',

					'order'              => 'DESC',

					'start'              => 0,

					'limit'              => $setting['limit']

				);

				$products = $this->model_catalog_product->getProducts($filter_data_product);

			}



			if (!empty($products)) {

				$data['categories'][$category_id] = array(

					'name'   => $category_info['name'],

					'desc'   => htmlspecialchars_decode($category_info['description']),

					'banner' => $this->config->get('config_url') . 'image/' . $category_info['banner'],

					'href'   => $this->url->link('product/category', 'path=' . $category_id)

				);



				foreach ($products as $product) {

					$product_info = $this->model_catalog_product->getProduct($product['product_id']);



					$noimage = false;

					if ($product['image']) {

						$image = HTTP_SERVER . 'image/' . $product['image'];

						// $image = $this->model_tool_image->resize($product['image'], $setting['width'], $setting['height']);

					} else {

						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);

						$noimage = true;

					}



					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {

						// $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

						$price = $this->currency->format($this->tax->calculate($product_info['price_display'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

					} else {

						$price = false;

					}



					if ((float)$product['special']) {

						$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

					} else {

						$special = false;

					}



					if ($this->config->get('config_tax')) {

						$tax = $this->currency->format((float)$product['special'] ? $product['special'] : $product['price'], $this->session->data['currency']);

					} else {

						$tax = false;

					}



					if ($this->config->get('config_review_status')) {

						$rating = $product['rating'];

					} else {

						$rating = false;

					}



					// pre($this->weight->convert((float)$product['weight'], $product['weight_class_id'], 1));

					$this->load->model('localisation/unit_class');



					$unit_class = $this->model_localisation_unit_class->getUnitClass($product['unit_class_id']);

					if (!empty($unit_class)) {

						$unit_class_name = $unit_class['unit_name'];

					} else {

						$unit_class_name = '';

					}



					if ((float)$this->weight->convert((float)$product['weight'], $product['weight_class_id'], 1) >= 1) {

						$weight = $product['min_weight'] != 0 ? (float)$this->weight->convert((float)$product['min_weight'], $product['weight_class_id'], 1) . '-' . (float)$this->weight->convert((float)$product['weight'], $product['weight_class_id'], 1) . $this->weight->getUnit(1) : (float)$this->weight->convert((float)$product['weight'], $product['weight_class_id'], 1) . $this->weight->getUnit(1);

					} else {

						$weight = $product['min_weight'] != 0 ? (float)$this->weight->convert((float)$product['min_weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id')) . '-' . (float)$this->weight->convert((float)$product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id')) . $this->weight->getUnit($this->config->get('config_weight_class_id')) : (float)$this->weight->convert((float)$product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id')) . $this->weight->getUnit($this->config->get('config_weight_class_id'));

					}



					// $weight = $product['min_weight'] != 0 ? (float)$this->weight->convert((float)$product['min_weight'], $product['weight_class_id'], 1) . '-' . (float)$this->weight->convert((float)$product['weight'], $product['weight_class_id'], 1) . $this->weight->getUnit(1) : (float)$this->weight->convert((float)$product['weight'], $product['weight_class_id'], 1) . $this->weight->getUnit(1);



					if (empty($product['model'])) {

						$caption = sprintf($this->language->get('text_unitprice'), $unit_class_name, $weight);

					} else {

						$caption = $product['model'];

					}



					$data['categories'][$category_id]['products'][] = array(

						'product_id'  => $product['product_id'],

						'thumb'       => $image,

						'name'        => $product['name'],

						'price'       => $price,
						'label'       => $product['label'],
						'label_2'     => $product['label_2'],

						'special'     => $special,

						'tax'         => $tax,

						'rating'      => $rating,

						'noimage'	  => $noimage,

						'minimum'     => $product['minimum'] > 0 ? $product['minimum'] : 1,

						'quantity'	  => $product['quantity'],

						'discount'	  => $special ? get_disc($product['price'], $product['special']) : '',

						// 'weight'	  => (float)$product['weight'] . $this->weight->getUnit($product['weight_class_id']),

						'caption'	  => $caption,

						'href'        => $this->url->link('product/product', 'product_id=' . $product['product_id'])

					);

				}

			}

		}



		return $this->load->view('extension/module/latest', $data);

	}

}

