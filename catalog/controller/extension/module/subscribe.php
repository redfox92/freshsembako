<?php
class ControllerExtensionModuleSubscribe extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/subscribe');

		$data['text_title'] = $this->language->get('text_title');
		$data['text_description'] = $this->language->get('text_description');

		$data['entry_email'] = $this->language->get('entry_email');

		$data['button_subscribe'] = $this->language->get('button_subscribe');

		$data['action'] = $this->url->link('extension/module/subscribe/subscribe');

		return $this->load->view('extension/module/subscribe', $data);
	}

	public function subscribe()
	{
		$this->load->language('extension/module/subscribe');
		$json = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->model('account/subscribe');

			$this->model_account_subscribe->addSubscribe($this->request->post);

			$json['success'] = $this->language->get('text_add');
		}

		if (isset($this->error['email'])) {
			$json['error_email'] = $this->error['email'];
		} else {
			$json['error_email'] = '';
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	private function validate()
	{
		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		$this->load->model('account/subscribe');

		$subscriber = $this->model_account_subscribe->getSubscriber($this->request->post['email']);

		if ($subscriber->num_rows > 0) {
			$this->error['email'] = $this->language->get('error_exists');
		}

		return !$this->error;
	}
}