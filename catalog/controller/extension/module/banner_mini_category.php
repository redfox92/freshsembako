<?php

class ControllerExtensionModuleBannerMiniCategory extends Controller {

	public function index($setting) {

		static $module = 0;



		$this->document->addStyle('catalog/view/javascript/jquery/owl.carousel/owl.carousel.min.css');

		$this->document->addStyle('catalog/view/javascript/jquery/owl.carousel/owl.theme.default.min.css');

		$this->document->addScript('catalog/view/javascript/jquery/owl.carousel/owl.carousel.min.js');



		$this->load->model('design/banner');

		$this->load->model('tool/image');



		$data['button_cart'] = $this->language->get('button_cart');



		$data['banners'] = array();



		$results = $this->model_design_banner->getBanner($setting['banner_id']);



		foreach ($results as $result) {

			if (is_file(DIR_IMAGE . $result['image'])) {

				$data['banner'] = array(

					'title' => $result['title'],

					'link'  => $result['link'],

					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])

				);

			}

		}



		$this->load->model('catalog/category');



		$categories = $this->model_catalog_category->getCategories();



		foreach ($categories as $category) {

			$data['categories'][] = array(

				'name'     => $category['name'],

				'image'	   => $this->model_tool_image->resize($category['image'], $this->config->get('theme_default_image_category_width'), $this->config->get('theme_default_image_category_height')),

				// 'image'	   => base_url() . 'image/' . $category['image'],

				'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])

			);

		}



		$data['module'] = $module++;



		return $this->load->view('extension/module/banner_mini_category', $data);

	}

}