<?php
class ControllerExtensionPaymentBankTransfer extends Controller {
	public function index() {
		$this->load->language('extension/payment/bank_transfer');

		$data['text_instruction'] = $this->language->get('text_instruction');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_payment'] = $this->language->get('text_payment');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['button_confirm'] = $this->language->get('button_confirm');
		
		// edited by wisnu
		// get payment method code
		$payment_method = $this->session->data['payment_method']['code'];
		// get payment method key from setting
		$payment_method_key = array_search($payment_method, $this->config->get('bank_transfer_list_code'));
		// get payment method term
		$this->load->model('localisation/language');
		foreach ($this->model_localisation_language->getLanguages() as $lang) {
			if ($lang['code'] == $this->language->get('code')) {
				$bank_term_list = $this->config->get('bank_transfer_list_bank'.$lang['language_id']);
			}
		}
		// if language not found
		if (!isset($bank_term_list)) {
			$bank_term_list = $this->config->get('bank_transfer_list_bank1');
		}

		// bank name list
		$bank_name_list = $this->config->get('bank_transfer_list_name');
		// bank code list
		$bank_code_list = $this->config->get('bank_transfer_list_code');
		// bank code list
		$bank_status_list = $this->config->get('bank_transfer_list_status');
		// bank sort order list
		$bank_sort_list = $this->config->get('bank_transfer_list_sort');
		// bank sort order list
		$bank_image_list = $this->config->get('bank_transfer_list_image');

		$get_payment_term = html_entity_decode($bank_term_list[$payment_method_key], ENT_QUOTES, 'UTF-8');
// pre($get_payment_term);
		// $data['bank'] = $get_payment_term;
		// $data['bank'] = nl2br($this->config->get('bank_transfer_bank' . $this->config->get('config_language_id')));
		$data['bank'] = array(
			'name'  => $bank_name_list[$payment_method_key],
			'image' => base_url() . 'image/' . $bank_image_list[$payment_method_key],
			'term'  => $get_payment_term,
		);

		$data['text_selection'] = $this->language->get('text_selection');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['continue'] = $this->url->link('checkout/success');
// pre($data);
		return $this->load->view('extension/payment/bank_transfer', $data);
	}

	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'bank_transfer' || in_array($this->session->data['payment_method']['code'], $this->config->get('bank_transfer_list_code'))) {
			$this->load->language('extension/payment/bank_transfer');

			$this->load->model('checkout/order');

			// edited by wisnu
			// get payment method code
			$payment_method = $this->session->data['payment_method']['code'];
			// get payment method key from setting
			$payment_method_key = array_search($payment_method, $this->config->get('bank_transfer_list_code'));
			// get payment method term
			$this->load->model('localisation/language');
			foreach ($this->model_localisation_language->getLanguages() as $lang) {
				if ($lang['code'] == $this->language->get('code')) {
					$bank_term_list = $this->config->get('bank_transfer_list_bank'.$lang['language_id']);
				}
			}
			// if language not found
			if (!isset($bank_term_list)) {
				$bank_term_list = $this->config->get('bank_transfer_list_bank1');
			}

			$get_payment_term = html_entity_decode($bank_term_list[$payment_method_key], ENT_QUOTES, 'UTF-8');

			$comment  = $this->language->get('text_instruction') . "\n\n";
			$comment .= $this->config->get('bank_transfer_bank' . $this->config->get('config_language_id'));
			$comment .= $this->config->get('bank_transfer_list_name')[$payment_method_key] . "\n";
			$comment .= $get_payment_term . "\n\n";
			$comment .= $this->language->get('text_payment');

			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('bank_transfer_order_status_id'), $comment, true);
		}
	}
}