<?php
class ControllerCommonHeader extends Controller {
	public function index() {
		$this->load->model('tool/image');
		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		$data['favicon']=array();
		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$data['favicon'][] = array(
				'rel'   => 'icon',
				'src'   =>  $server . 'image/' . $this->config->get('config_icon'),
				'sizes' => ''
			);
		}

		// Apple favicon
		$master_apple = array(
			0 => array('width'=>60,'height'=>60),
			1 => array('width'=>72,'height'=>72),
			2 => array('width'=>120,'height'=>120),
			3 => array('width'=>152,'height'=>152),
		);

		foreach ($master_apple as $apple) {
			if (is_file(DIR_IMAGE . $this->config->get("config_icon_{$apple['width']}_{$apple['height']}"))) {
				$data['favicon'][] = array(
					'rel'   => 'apple-touch-icon',
					'src'   =>  $server . 'image/' . $this->config->get("config_icon_{$apple['width']}_{$apple['height']}"),
					'sizes' => "{$apple['width']}x{$apple['height']}"
				);
			}
		}
		// End of Apple Favicon

		$data['title'] = $this->document->getTitle();

		$data['base']        = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords']    = $this->document->getKeywords();
		$data['links']       = $this->document->getLinks();
		$data['styles']      = $this->document->getStyles();
		$data['scripts']     = $this->document->getScripts();
		$data['lang']        = $this->language->get('code');
		$data['direction']   = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		//custome og image here (jika pada suatue page ogimage tidak di set, maka akan menggunakan image LOGO)
		$data['og_image'] = $this->document->getOgimage() ? $this->document->getOgimage() : $data['logo'];

		$this->load->language('common/header');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
			// $data['reward_points'] = number_format_short($this->customer->getRewardPoints());
			$data['reward_points'] = $this->currency->format($this->customer->getRewardPoints(), $this->session->data['currency']);
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
			$data['reward_points'] = '';
		}
// pre($data['reward_points']);
		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] 		= sprintf($this->language->get('text_logged'), $this->customer->getFirstName());

		$data['text_home']        = $this->language->get('text_home');
		$data['text_account']     = $this->language->get('text_account');
		$data['text_register']    = $this->language->get('text_register');
		$data['text_login']       = $this->language->get('text_login');
		$data['text_order']       = $this->language->get('text_order');
		$data['text_address']     = $this->language->get('text_address');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download']    = $this->language->get('text_download');
		$data['text_logout']      = $this->language->get('text_logout');
		$data['text_checkout']    = $this->language->get('text_checkout');
		$data['text_category']    = $this->language->get('text_category');
		$data['text_all']         = $this->language->get('text_all');
		$data['text_shop']        = $this->language->get('text_shop');
		$data['text_promo']       = $this->language->get('text_promo');
		$data['text_menu']        = $this->language->get('text_menu');
		$data['text_contact']     = $this->language->get('text_contact');
		$data['text_follow']      = $this->language->get('text_follow');
		$data['text_reward'] = $this->language->get('text_reward');
		$data['text_payment_confirmation'] = $this->language->get('text_payment_confirmation');

		$this->load->language('account/login');

		$data['text_forgotten'] = $this->language->get('text_forgotten');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['button_login'] = $this->language->get('button_login');

		$this->load->language('account/register');
		$data['entry_fullname'] = $this->language->get('entry_fullname');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_confirm'] = $this->language->get('entry_confirm');
		$data['button_submit'] = $this->language->get('button_submit');
		$data['text_account_already_modal'] = $this->language->get('text_account_already_modal');
		$data['text_customer_area'] = $this->language->get('text_customer_area');
		$data['text_forgot_password'] = $this->language->get('text_forgot_password');
		$data['text_account_register_modal'] = $this->language->get('text_account_register_modal');
		$data['text_register_modal'] = $this->language->get('text_register_modal');

		$data['home']          = $this->config->get('config_url');
		$data['wishlist']      = $this->url->link('account/wishlist', '', true);
		$data['logged']        = $this->customer->isLogged();
		$data['account']       = $this->url->link('account/account', '', true);
		$data['register']      = $this->url->link('account/register', '', true);
		$data['login']         = $this->url->link('account/login', '', true);
		$data['forgotten']     = $this->url->link('account/forgotten', '', true);
		$data['order']         = $this->url->link('account/order', '', true);
		$data['address']       = $this->url->link('account/address', '', true);
		$data['transaction']   = $this->url->link('account/transaction', '', true);
		$data['download']      = $this->url->link('account/download', '', true);
		$data['logout']        = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout']      = $this->url->link('checkout/checkout', '', true);
		$data['contact']       = $this->url->link('information/contact');
		$data['category']      = $this->url->link('product/category', '', true);
		$data['reward'] 	   = $this->url->link('account/reward', '', true);
		$data['payment_confirmation'] = $this->url->link('account/payment_confirmation', '', true);
		$data['telephone']     = $this->config->get('config_telephone');
		$data['email']        = $this->config->get('config_email');
		$data['instagram']    = $this->config->get('config_ig') ? 'https://www.instagram.com/' . $this->config->get('config_ig') : '';
		if ($this->config->get('config_wa')) {
			if($this->config->get('config_wa')[0] == '+'){
				$data['whatsapp']	= 'https://api.whatsapp.com/send?phone=' . str_replace('+62', '0', substr($this->config->get('config_wa'), 1));
			}else{
				$data['whatsapp']	= 'https://api.whatsapp.com/send?phone=' . $this->config->get('config_wa');
			}
		} else {
			$data['whatsapp'] = '';
		}
		$data['facebook']		= $this->config->get('config_fb') ? 'https://www.facebook.com/' . $this->config->get('config_fb') : '';
		$data['line']			= $this->config->get('config_line') ? 'https://line.me/R/ti/p/'. $this->config->get('config_line') : '';
		$data['youtube']		= $this->config->get('config_youtube') ? 'https://www.youtube.com/channel/' . $this->config->get('config_youtube') : '';


		$this->load->model('catalog/category');

		$category = $this->model_catalog_category->getCategory(10);

		$data['grosir'] = '';
		if (!empty($category)) {
			$data['text_grosir'] = $category['name'];
			$data['grosir'] = $this->url->link('product/category', 'path=10', true);
		}
		// Menu
		// $this->load->model('catalog/category');

		// $this->load->model('catalog/product');

		// $data['categories'] = array();

		// $categories = $this->model_catalog_category->getCategories(0);

		// foreach ($categories as $category) {
		// 	if ($category['top']) {
		// 		// Level 2
		// 		$children_data = array();

		// 		$children = $this->model_catalog_category->getCategories($category['category_id']);

		// 		foreach ($children as $child) {
		// 			$filter_data = array(
		// 				'filter_category_id'  => $child['category_id'],
		// 				'filter_sub_category' => true
		// 			);

		// 			$children_data[] = array(
		// 				'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
		// 				'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
		// 			);
		// 		}

		// 		// Level 1
		// 		$data['categories'][] = array(
		// 			'name'     => $category['name'],
		// 			'children' => $children_data,
		// 			'column'   => $category['column'] ? $category['column'] : 1,
		// 			'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
		// 		);
		// 	}
		// }

		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search']   = $this->load->controller('common/search');
		$data['cart']     = $this->load->controller('common/cart');

		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'];
			} elseif (isset($this->request->get['path'])) {
				$class = '-' . $this->request->get['path'];
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'];
			} elseif (isset($this->request->get['information_id'])) {
				$class = '-' . $this->request->get['information_id'];
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'common-home';
		}

		$data['route'] = isset($this->request->get['route']) ? $this->request->get['route'] : 'common/home';

		$data['category_id'] = isset($this->request->get['path']) ? $this->request->get['path'] : '';

		return $this->load->view('common/header', $data);
	}
}
