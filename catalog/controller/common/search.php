<?php
class ControllerCommonSearch extends Controller {
	public function index() {
		$this->load->language('common/search');

		$data['text_search'] = $this->language->get('text_search');
		$data['text_empty'] = $this->language->get('text_empty');

		$data['action'] = $this->url->link('common/search/ajax', '', true);
		$data['action2'] = $this->url->link('product/search', '');

		if (isset($this->request->get['search'])) {
			$data['search'] = $this->request->get['search'];
		} else {
			$data['search'] = '';
		}

		return $this->load->view('common/search', $data);
	}

	public function ajax() {
		if (isset($this->request->get['search'])) {
			$search = $this->request->get['search'];
		} else {
			$search = '';
		}

		if (isset($this->request->get['tag'])) {
			$tag = $this->request->get['tag'];
		} elseif (isset($this->request->get['search'])) {
			$tag = $this->request->get['search'];
		} else {
			$tag = '';
		}

		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$filter_data = array(
			'filter_name'         => $search,
			'filter_tag'          => $tag,
			'start'               => 0,
			'limit'               => 5
		);

		$results = $this->model_catalog_product->getProducts($filter_data);

		$json['products'] = array();
		foreach ($results as $result) {
			// if ($result['image']) {
			// 	$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
			// } else {
			// 	$image = '';
			// }

			$json['products'][] = array(
				'name' => $result['name'],
				// 'image'=> $image,
				'href' => $this->url->link('product/product', 'product_id=' . $result['product_id']),
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}