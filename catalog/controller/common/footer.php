<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$data['scripts'] = $this->document->getScripts('footer');

		$data['text_home']            = $this->language->get('text_home');
		$data['text_information']     = $this->language->get('text_information');
		$data['text_service']         = $this->language->get('text_service');
		$data['text_extra']           = $this->language->get('text_extra');
		$data['text_contact']         = $this->language->get('text_contact');
		$data['text_follow']          = $this->language->get('text_follow');
		$data['text_return']          = $this->language->get('text_return');
		$data['text_sitemap']         = $this->language->get('text_sitemap');
		$data['text_manufacturer']    = $this->language->get('text_manufacturer');
		$data['text_voucher']         = $this->language->get('text_voucher');
		$data['text_affiliate']       = $this->language->get('text_affiliate');
		$data['text_special']         = $this->language->get('text_special');
		$data['text_account']         = $this->language->get('text_account');
		$data['text_order']           = $this->language->get('text_order');
		$data['text_wishlist']        = $this->language->get('text_wishlist');
		$data['text_newsletter']      = $this->language->get('text_newsletter');
		$data['text_quick_links']     = $this->language->get('text_quick_links');
		$data['text_shop']            = $this->language->get('text_shop');
		$data['text_promo']           = $this->language->get('text_promo');
		$data['text_brands']          = $this->language->get('text_brands');
		$data['text_payment_method']  = $this->language->get('text_payment_method');
		$data['text_shipping_method'] = $this->language->get('text_shipping_method');

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'id'	=> $result['information_id'],
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}

		$data['home']         = $this->url->link('common/home');
		$data['contact']      = $this->url->link('information/contact');
		$data['return']       = $this->url->link('account/return/add', '', true);
		$data['sitemap']      = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher']      = $this->url->link('account/voucher', '', true);
		$data['affiliate']    = $this->url->link('affiliate/account', '', true);
		$data['special']      = $this->url->link('product/special');
		$data['account']      = $this->url->link('account/account', '', true);
		$data['order']        = $this->url->link('account/order', '', true);
		$data['wishlist']     = $this->url->link('account/wishlist', '', true);
		$data['newsletter']   = $this->url->link('account/newsletter', '', true);
		$data['category']     = $this->url->link('product/category', '', true);
		$data['telephone']    = $this->config->get('config_telephone');
		$data['email']        = $this->config->get('config_email');
		$data['instagram']    = $this->config->get('config_ig') ? 'https://www.instagram.com/' . $this->config->get('config_ig') : '';
		if ($this->config->get('config_wa')) {
			if($this->config->get('config_wa')[0] == '+'){
				$data['whatsapp']	= 'https://api.whatsapp.com/send?phone=' . str_replace('+62', '0', substr($this->config->get('config_wa'), 1));
			}else{
				$data['whatsapp']	= 'https://api.whatsapp.com/send?phone=' . $this->config->get('config_wa');
			}
		} else {
			$data['whatsapp'] = '';
		}
		$data['facebook']		= $this->config->get('config_fb') ? 'https://www.facebook.com/' . $this->config->get('config_fb') : '';
		$data['line']			= $this->config->get('config_line') ? 'https://line.me/R/ti/p/'. $this->config->get('config_line') : '';
		$data['youtube']		= $this->config->get('config_youtube') ? 'https://www.youtube.com/channel/' . $this->config->get('config_youtube') : '';

		$data['powered'] = sprintf($this->language->get('text_powered'), date('Y', time()), $this->config->get('config_name'));

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		$this->load->model('extension/extension');

		// Payment Methods
		$payment_method = array();
		$payment_images = array();

		$results = $this->model_extension_extension->getExtensions('payment');

		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {
				$payment_method[] = $result;
			}
		}

		foreach ($payment_method as $method) {
			if ($method['code'] == 'bank_transfer') {
				$this->load->model('localisation/language');
				// get terms
				foreach ($this->model_localisation_language->getLanguages() as $lang) {
					if ($lang['code'] == $this->language->get('code')) {
						$bank_term_list = $this->config->get('bank_transfer_list_bank'.$lang['language_id']);
					}
				}

				if (!isset($bank_term_list)) {
					$bank_term_list = $this->config->get('bank_transfer_list_bank1');
				}

				// bank name list
				$bank_name_list = $this->config->get('bank_transfer_list_name');
				// bank code list
				$bank_code_list = $this->config->get('bank_transfer_list_code');
				// bank code list
				$bank_status_list = $this->config->get('bank_transfer_list_status');
				// bank sort order list
				$bank_sort_list = $this->config->get('bank_transfer_list_sort');
				// bank sort order list
				$bank_image_list = $this->config->get('bank_transfer_list_image');

				if (isset($bank_name_list)) {
					foreach ($bank_name_list as $key => $value) {
						// if bank enable
						if ($bank_status_list[$key]) {

							// add bank transfer
							$payment_images[$bank_code_list[$key]] = array( 
								'code'       => $bank_code_list[$key],
								'title'      => $bank_name_list[$key],
								'sort_order' => $bank_sort_list[$key],
								'image' => base_url() . 'image/' . $bank_image_list[$key],
							);			
						}
					}
				}
			}
		}

		// Shipping Methods
		$method_data = array();
		$shipping_images = array();

		$results = $this->model_extension_extension->getExtensions('shipping');

		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {

				$this->load->language('extension/shipping/' . $result['code']);
				if (substr($result['code'], 0, 7) == 'sameday') {
					if (!in_array('sameday', $method_data)) {
						$method_data[] = 'sameday';
					}
				} else {
					$method_data[] = $result['code'];
				}

				$shipping_images[] = array(
					'code'  => $result['code'],
					'image' => base_url() . 'image' . $this->language->get('images')
				);
			}
		}

		$data['payment_images'] = $payment_images;
		$data['shipping_images'] = array();
		
		$data['route'] = isset($this->request->get['route']) ? $this->request->get['route'] : 'common/home';
		$data['category_id'] = isset($this->request->get['path']) ? $this->request->get['path'] : '';
		$data['information_id'] = isset($this->request->get['information_id']) ? $this->request->get['information_id'] : '';

		return $this->load->view('common/footer', $data);
	}
}
