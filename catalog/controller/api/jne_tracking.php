<?php
class ControllerApiJneTracking extends Controller {
    private $error = array();
    private $username = '';
    private $api_key = '';
    public function __construct($params) {
        parent::__construct($params);
        // Get language
        $this->load->language('api/jne_tracking');

        // **Required module tracking

        // get username
        $this->username = $this->config->get('jne_api_username');
        // get api key
        $this->api_key = $this->config->get('jne_api_key');

        if (!$this->username || !$this->api_key) {
            $this->username = 'TESTAPI';
            $this->api_key = '25c898a9faea1a100859ecd9ef674548';
        }
    }


    /* 
    ** Tracking JNE
    ** @required post 'resi'
    ** @return json
    */
    public function track() {
        // init
        $resi = '';

        if (isset($this->request->post['resi'])) {
            $resi = $this->request->post['resi'];
        }

        if (isset($this->request->post['type'])) {
            $type = $this->request->post['type'];
        }

        if ($this->validate('track') && $resi) {
            $url = "http://api.jne.co.id:8889/tracing/apitest/list/cnote/";
            $url .= $resi;

            $data = array(
                    'username'  => $this->username,
                    'api_key'   => $this->api_key
                );
            
            $ch = curl_init();

            // CURL
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            // receive server response
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $json = curl_exec ($ch);

            curl_close ($ch);

            if (isset($type)) {
                if ($type == 'array') {
                    $json = json_decode($json, true);
                    $json = json_encode($json);
                }
            }

        } else {
            $json = $this->error;
            $json = json_encode($json);
        }


        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput($json);
    }


    /* 
    ** Get origin city
    ** @required post 'city'
    ** @return json
    */
    public function get_origin() {
        // init
        $city = '';

        if (isset($this->request->post['city'])) {
            $city = $this->request->post['city'];
        }

        if ($this->validate('get_origin') && $city) {
            $url = "http://api.jne.co.id:8889/tracing/avione/origin/key/";
            $url .= $city;

            $data = array(
                    'username'  => $this->username,
                    'api_key'   => $this->api_key
                );

            $ch = curl_init();

            // CURL
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            // receive server response
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $json = curl_exec ($ch);

            curl_close ($ch);
        }
        else {
            $json = $this->error;
            $json = json_encode($json);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput($json);
    }

    /* 
    ** Get destination city
    ** @required post 'city'
    ** @return json
    */
    public function get_dest() {
        // init
        $city = '';

        if (isset($this->request->post['city'])) {
            $city = $this->request->post['city'];
        }

        if ($this->validate('get_dest') && $city) {
            $url = "http://api.jne.co.id:8889/tracing/avione/dest/key/";
            $url .= $city;

            $data = array(
                    'username'  => $this->username,
                    'api_key'   => $this->api_key
                );

            $ch = curl_init();

            // CURL
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            // receive server response
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $json = curl_exec ($ch);

            curl_close ($ch);
        }
        else {
            $json = $this->error;
            $json = json_encode($json);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput($json);
    }

    /* 
    ** Get fare result
    ** @required post 'from'
    ** @required post 'thru'
    ** @required post 'weight'
    ** @required post 'service'
    ** @return json
    */
    public function get_fare() {
        // init
        $from = '';
        $thru = '';
        $weight = '';
        $service = '';
        $service_code = '';
        $data = array(
                    'username'  => $this->username,
                    'api_key'   => $this->api_key
                );

        if (isset($this->request->post['from'])) {
            $data['from'] = $this->request->post['from'];
        }

        if (isset($this->request->post['thru'])) {
            $data['thru'] = $this->request->post['thru'];
        }

        if (isset($this->request->post['weight'])) {
            $data['weight'] = $this->request->post['weight'];
        }

        if (isset($this->request->post['service'])) {
            $service = $this->request->post['service'];
        }

        if (isset($this->request->post['service_code'])) {
            $service_code = $this->request->post['service_code'];
        }

        if ($this->validate('get_fare') && $data['from'] && $data['thru']) {
            $url = "http://api.jne.co.id:8889/tracing/avione/price/";

            $ch = curl_init();

            // CURL
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            // receive server response
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $json = curl_exec ($ch);

            curl_close ($ch);

            // Service type
            if ($service) {
                $json = json_decode($json, true);
                $temp = array();

                if ($service == 'jtr') {
                    $srv_code = array(
                            'JTR',
                            'JTR250',
                            'JTR<150',
                            'JTR>250',
                        );

                    if (isset($json['price'] )) {
                        foreach ($json['price'] as $key => $srv) {
                            if (in_array($srv['service_code'], $srv_code)) {
                                $temp['price'][] = $srv;
                            }                    
                        }
                    }
                }

                if ($service == 'jne') {
                    $srv_code = array(
                            'OKE15',
                            'REG15',
                            'YES15',
                        );

                    if (isset($json['price'] )) {
                        foreach ($json['price'] as $key => $srv) {
                            if (in_array($srv['service_code'], $srv_code)) {
                                $temp['price'][] = $srv;
                            }                    
                        }
                    }
                }

                $json = $temp;
                $json = json_encode($json);
            }

            // Service code
            if ($service_code) {
                $json = json_decode($json, true);
                $temp = array();
                $service_code = str_replace(' ', '', $service_code);
                $service_code = explode(',', $service_code);

                foreach ($json['price'] as $key => $srv) {
                    if (array_search($srv['service_code'], $service_code) !== false) {
                        $temp[] = $srv;
                    }                    
                }

                $json = $temp;
                $json = json_encode($json);
            }
        }
        else {
            $json = $this->error;
            $json = json_encode($json);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput($json);
    }

    /*
    ** Validate
    ** @return array
    */
    protected function validate($case) {
        switch ($case) {
            case 'track':
                if (!isset($this->request->post['resi'])) {
                    $this->error['warning'] = $this->language->get('error_awb');
                }
                
                break;

            case 'get_origin':
                if (!isset($this->request->post['city'])) {
                    $this->error['warning'] = $this->language->get('error_city');
                }
                
                break;

            case 'get_dest':
                if (!isset($this->request->post['city'])) {
                    $this->error['warning'] = $this->language->get('error_city');
                }
                
                break;

            case 'get_fare':
                if (!isset($this->request->post['from'])) {
                    $this->error['warning']['from'] = $this->language->get('error_from');
                }

                if (!isset($this->request->post['thru'])) {
                    $this->error['warning']['thru'] = $this->language->get('error_thru');
                }
                
                break;
        }

        return !$this->error;
    }
}