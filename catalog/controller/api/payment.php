<?php
class ControllerApiPayment extends Controller {
	public function address() {
		$this->load->language('api/payment');

		// Delete old payment address, payment methods and method so not to cause any issues if there is an error
		unset($this->session->data['payment_address']);
		unset($this->session->data['payment_methods']);
		unset($this->session->data['payment_method']);

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error']['warning'] = $this->language->get('error_permission');
		} else {
			// Add keys for missing post vars
			$keys = array(
				'firstname',
				'lastname',
				'company',
				'address_1',
				'address_2',
				'postcode',
				'city_id',
				'zone_id',
				'country_id'
			);

			foreach ($keys as $key) {
				if (!isset($this->request->post[$key])) {
					$this->request->post[$key] = '';
				}
			}

			if ((utf8_strlen(trim($this->request->post['fullname'])) < 1) || (utf8_strlen(trim($this->request->post['fullname'])) > 32)) {
				$json['error']['fullname'] = $this->language->get('error_fullname');
			}

			// if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			// 	$json['error']['firstname'] = $this->language->get('error_firstname');
			// }

			// if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			// 	$json['error']['lastname'] = $this->language->get('error_lastname');
			// }

			if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
				$json['error']['address_1'] = $this->language->get('error_address_1');
			}

			// if ((utf8_strlen($this->request->post['city']) < 2) || (utf8_strlen($this->request->post['city']) > 32)) {
			// 	$json['error']['city'] = $this->language->get('error_city');
			// }

			$this->load->model('localisation/country');

			$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);

			if ($country_info && $country_info['postcode_required'] && (utf8_strlen(trim($this->request->post['postcode'])) < 2 || utf8_strlen(trim($this->request->post['postcode'])) > 10)) {
				$json['error']['postcode'] = $this->language->get('error_postcode');
			}

			if ($this->request->post['country_id'] == '') {
				$json['error']['country'] = $this->language->get('error_country');
			}

			if (!isset($this->request->post['zone_id']) || $this->request->post['zone_id'] == '') {
				$json['error']['zone'] = $this->language->get('error_zone');
			}

			if (!isset($this->request->post['city_id']) || $this->request->post['city_id'] == '') {
				$json['error']['city'] = $this->language->get('error_city');
			}

			// Custom field validation
			$this->load->model('account/custom_field');

			$custom_fields = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

			foreach ($custom_fields as $custom_field) {
				if (($custom_field['location'] == 'address') && $custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['custom_field_id']])) {
					$json['error']['custom_field' . $custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
				} elseif (($custom_field['location'] == 'address') && ($custom_field['type'] == 'text') && !empty($custom_field['validation']) && !filter_var($this->request->post['custom_field'][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
					$json['error']['custom_field' . $custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
				}
				
			}

			if (!$json) {
				$this->load->model('localisation/country');

				$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);

				if ($country_info) {
					$country = $country_info['name'];
					$iso_code_2 = $country_info['iso_code_2'];
					$iso_code_3 = $country_info['iso_code_3'];
					$address_format = $country_info['address_format'];
				} else {
					$country = '';
					$iso_code_2 = '';
					$iso_code_3 = '';
					$address_format = '';
				}

				$this->load->model('localisation/zone');

				$zone_info = $this->model_localisation_zone->getZone($this->request->post['zone_id']);

				if ($zone_info) {
					$zone = $zone_info['name'];
					$zone_code = $zone_info['code'];
				} else {
					$zone = '';
					$zone_code = '';
				}

				$this->load->model('localisation/city');

				$city_info = $this->model_localisation_city->getCity($this->request->post['city_id']);

				if ($city_info) {
					$city = $city_info['name'];
					$city_code = $city_info['code'];
				} else {
					$city = '';
					$city_code = '';
				}

				//split fullname
				if(!empty($this->request->post['fullname']))
				{
					$customer_name = explode(' ',$this->request->post['fullname'], 2);

					$firstname = '';
					$lastname = '';
					if(isset($customer_name[0])){
						$firstname = $customer_name[0];
					}
					if(isset($customer_name[1])) {
						$lastname = $customer_name[1];
					}
				}

				$this->session->data['payment_address'] = array(
					'firstname'      => $firstname,
					'lastname'       => $lastname,
					'company'        => $this->request->post['company'],
					'address_1'      => $this->request->post['address_1'],
					'address_2'      => $this->request->post['address_2'],
					'postcode'       => $this->request->post['postcode'],
					'city'           => $city,
					'city_id'        => $this->request->post['city_id'],
					'zone_id'        => $this->request->post['zone_id'],
					'zone'           => $zone,
					'zone_code'      => $zone_code,
					'country_id'     => $this->request->post['country_id'],
					'country'        => $country,
					'iso_code_2'     => $iso_code_2,
					'iso_code_3'     => $iso_code_3,
					'address_format' => $address_format,
					'custom_field'   => isset($this->request->post['custom_field']) ? $this->request->post['custom_field'] : array()
				);

				$json['success'] = $this->language->get('text_address');
				
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
			}
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function methods() {
		$this->load->language('api/payment');
		
		// Delete past shipping methods and method just in case there is an error
		unset($this->session->data['payment_methods']);
		unset($this->session->data['payment_method']);

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			// Payment Address
			if (!isset($this->session->data['payment_address'])) {
				$json['error'] = $this->language->get('error_address');
			}
			
			if (!$json) {
				// Totals
				$totals = array();
				$taxes = $this->cart->getTaxes();
				$total = 0;

				// Because __call can not keep var references so we put them into an array. 
				$total_data = array(
					'totals' => &$totals,
					'taxes'  => &$taxes,
					'total'  => &$total
				);

				$this->load->model('extension/extension');

				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);
						
						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				// Payment Methods
				$json['payment_methods'] = array();

				$this->load->model('extension/extension');

				$results = $this->model_extension_extension->getExtensions('payment');

				$recurring = $this->cart->hasRecurringProducts();

				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('extension/payment/' . $result['code']);

						$method = $this->{'model_extension_payment_' . $result['code']}->getMethod($this->session->data['payment_address'], $total);

						if ($method) {
							if ($recurring) {
								if (property_exists($this->{'model_extension_payment_' . $result['code']}, 'recurringPayments') && $this->{'model_extension_payment_' . $result['code']}->recurringPayments()) {
									$json['payment_methods'][$result['code']] = $method;
								}
							} else {
								$json['payment_methods'][$result['code']] = $method;
							}
						}
					}
				}

				// Rebuild doku onecheckout & bank transfer
				// edit by wisnu
				$this->load->language('extension/payment/doku_onecheckout');

				foreach ($json['payment_methods'] as $key => $value) {
					// check if doku exist
					if ($key == 'doku_onecheckout') {
						$payment_select = $this->config->get('doku_onecheckout_payment_select');

						if ( $payment_select == 1 ) {

							//remove doku_onecheckout from list
							unset($json['payment_methods']['doku_onecheckout']);
							// remove doku_onecheckout from session
							unset($this->session->data['payment_methods']['doku_onecheckout']);

							// get doku onecheckout settings
							$payment_list =  $this->config->get('doku_onecheckout_payment_list');
							$payment_name =  $this->config->get('doku_onecheckout_payment_name');
							$payment_code =  $this->config->get('doku_onecheckout_payment_code');

							foreach ($payment_list as $key => $value) {
								if (!empty($value)) {
									
									// add to payment method list
									$json['payment_methods'][$payment_code[$key]] = array( 
										'code'       => $payment_code[$key],
										'title'      => $payment_name[$key],
						        		'terms'      => '' . $this->language->get('text_logo_'.$key),
										'sort_order' => $this->config->get('doku_onecheckout_sort_order')
									);

								}
							}
						}  
					}

					// Bank transfer
					if ($key == 'bank_transfer') {
						$this->load->model('localisation/language');
						// get terms
						foreach ($this->model_localisation_language->getLanguages() as $lang) {
							if ($lang['code'] == $this->language->get('code')) {
								$bank_term_list = $this->config->get('bank_transfer_list_bank'.$lang['language_id']);
							}
						}

						if (!isset($bank_term_list)) {
							$bank_term_list = $this->config->get('bank_transfer_list_bank1');
						}

						// bank name list
						$bank_name_list = $this->config->get('bank_transfer_list_name');
						// bank code list
						$bank_code_list = $this->config->get('bank_transfer_list_code');
						// bank code list
						$bank_status_list = $this->config->get('bank_transfer_list_status');
						// bank sort order list
						$bank_sort_list = $this->config->get('bank_transfer_list_sort');
						// bank sort order list
						$bank_image_list = $this->config->get('bank_transfer_list_image');

						//remove bank transfer from list
						unset($json['payment_methods']['bank_transfer']);
						// remove bank transfer from session
						unset($this->session->data['payment_methods']['bank_transfer']);

						if (isset($bank_name_list)) {
							foreach ($bank_name_list as $key => $value) {
								// if bank enable
								if ($bank_status_list[$key] == 1) {

									// add bank transfer
									$json['payment_methods'][$bank_code_list[$key]] = array( 
										'code'       => $bank_code_list[$key],
										'title'      => $bank_name_list[$key],
						        		'terms'      => $bank_term_list[$key] . '<img class="img-responsive" style="display:inline-block;width:70px" src="'. HTTP_SERVER . 'image/' .$bank_image_list[$key] .'">',
										'sort_order' => $bank_sort_list[$key]
									);			
			
								}
							}
						}
					}
				}

				$sort_order = array();

				foreach ($json['payment_methods'] as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $json['payment_methods']);

				if ($json['payment_methods']) {
					$this->session->data['payment_methods'] = $json['payment_methods'];
				} else {
					$json['error'] = $this->language->get('error_no_payment');
				}
			}
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function method() {
		$this->load->language('api/payment');

		// Delete old payment method so not to cause any issues if there is an error
		unset($this->session->data['payment_method']);

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			// Payment Address
			if (!isset($this->session->data['payment_address'])) {
				$json['error'] = $this->language->get('error_address');
			}

			// Payment Method
			if (empty($this->session->data['payment_methods'])) {
				$json['error'] = $this->language->get('error_no_payment');
			} elseif (!isset($this->request->post['payment_method'])) {
				$json['error'] = $this->language->get('error_method');
			} elseif (!isset($this->session->data['payment_methods'][$this->request->post['payment_method']])) {
				$json['error'] = $this->language->get('error_method');
			}

			if (!$json) {
				$this->session->data['payment_method'] = $this->session->data['payment_methods'][$this->request->post['payment_method']];

				$json['success'] = $this->language->get('text_method');
			}
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
