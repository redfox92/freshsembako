<?php
class ControllerApiCron extends Controller {
	public function auto_cancel_order()
	{
		$this->load->model('account/order');

		$filter = array(
			'order_status_id' => 1
		);

		$sql = "SELECT `order_id`, `order_status_id`, `date_added` FROM `" . DB_PREFIX . "order` WHERE `order_status_id` = '1'";
		$results = $this->db->query($sql)->rows;

		$this->load->model('checkout/order');

		foreach ($results as $order) {
			$hour_diff = round((strtotime(date('Y-m-d H:i:s')) - strtotime($order['date_added']))/3600, 1);
			if ($hour_diff > 24) {
				$this->model_checkout_order->addOrderHistory($order['order_id'], 8, 'Pesanan otomatis dibatalkan.', true);
			}
		}
	}
}