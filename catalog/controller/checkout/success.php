<?php
class ControllerCheckoutSuccess extends Controller {
	public function index() {
		$this->load->language('checkout/success');

		if (isset($this->session->data['order_id'])) {
			$this->cart->clear();

			// Add to activity log
			if ($this->config->get('config_customer_activity')) {
				$this->load->model('account/activity');

				if ($this->customer->isLogged()) {
					$activity_data = array(
						'customer_id' => $this->customer->getId(),
						'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
						'order_id'    => $this->session->data['order_id']
					);

					$this->model_account_activity->addActivity('order_account', $activity_data);
				} else {
					$activity_data = array(
						'name'     => $this->session->data['guest']['firstname'] . ' ' . $this->session->data['guest']['lastname'],
						'order_id' => $this->session->data['order_id']
					);

					$this->model_account_activity->addActivity('order_guest', $activity_data);
				}
			}

			$this->load->model('account/order');

			$order = $this->model_account_order->getOrder($this->session->data['order_id']);

			$payment_method_key = array_search($this->session->data['payment_method']['code'], $this->config->get('bank_transfer_list_code'));
			$bank_name_list = $this->config->get('bank_transfer_list_name');
			$bank_image_list = $this->config->get('bank_transfer_list_image');
			$bank_term_list = $this->config->get('bank_transfer_list_bank2');
			$get_payment_term = explode('-', html_entity_decode($bank_term_list[$payment_method_key], ENT_QUOTES, 'UTF-8'));
			$data['order'] = array(
				'order_id'       => $this->session->data['order_id'],
				'payment_method' => array(
					'name' => $bank_name_list[$payment_method_key],
					'image' => base_url() . 'image/' . $bank_image_list[$payment_method_key],
					'user_name' => $get_payment_term[0],
					'user_account' => strip_tags($get_payment_term[1]),
				),
				'invoice_no'     => $order['invoice_no']
			);

			$data['confirm'] = $this->url->link('information/payment_confirmation', 'order_id=' . $this->session->data['order_id'], true);

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);
		} else {
			$this->response->redirect(HTTP_SERVER);
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_basket'),
			'href' => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_checkout'),
			'href' => $this->url->link('checkout/checkout', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_success'),
			'href' => $this->url->link('checkout/success')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['account'] = $this->url->link('account/account', '', true);
		$data['history'] = $this->url->link('account/order', '', true);
		$data['contact'] = $this->url->link('information/contact', '', true);

		$data['text_message'] = array();
		if ($this->customer->isLogged()) {
			$data['text_message'][] = $this->language->get('text_customer_success_2');
			$data['text_message'][] = sprintf($this->language->get('text_customer_success_3'), $this->url->link('account/account', '', true), $this->url->link('account/order', '', true));
			$data['text_message'][] = sprintf($this->language->get('text_customer_success_5'), 'mailto:' . $this->config->get('config_email'), $this->config->get('config_email'));
			$data['text_message'][] = $this->language->get('text_customer_success_6');
		// 	$data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', true), $this->url->link('account/order', '', true), $this->url->link('account/download', '', true), $this->url->link('information/contact'));
		} else {
			$data['text_message'][] = $this->language->get('text_guest_2');
			$data['text_message'][] = sprintf($this->language->get('text_guest_3'), 'mailto:' . $this->config->get('config_email'), $this->config->get('config_email'));
			$data['text_message'][] = $this->language->get('text_guest_4');
		// 	$data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
		}

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('checkout/success', $data));
	}
}