<?php
class ControllerToolCron extends Controller {
	// limit city to update jne price
	var $limitCity = 100;

	public function updatePendingOrder() {

		$this->load->model('tool/cron');
		$this->model_tool_cron->updatePending($this->config->get('config_cron'));
	}

	public function deleteForgottenToken() {
		
		$this->load->model('tool/cron');

		/*in hours ---- 3 hours*/
		$this->model_tool_cron->deleteForgottenToken(3);
	}


	/**
	 * Update JNE price through JNE API
	 * @return
	 */
	public function updateJNE() {
		$this->load->model('localisation/city');
		$this->load->model('tool/cron');

		// init
		// limit row
		$limit = $this->limitCity;
		$weight = 1;
		$url = $this->url->link('api/jne_tracking/get_fare', '', true);
		$from = $this->model_localisation_city->getCity($this->config->get('config_city_id'))['code'];

		// jakarta
		$from = 'CGK10000';

		$filter = array(
				'update_jne'	=> true,
				'sort'			=> 'city_id',
				'order_by'		=> 'DESC',
				'limit'			=> $limit
			);
		$cities = $this->model_tool_cron->getCities($filter);
		$citiesCount = count($cities);

		$ch = curl_init();

		// build sql query
		// to update shipping price
		$sql = '';
		$sql .= "UPDATE " . DB_PREFIX . "city SET ";

		$sql_reg = '';
		$sql_oke = '';
		$sql_yes = '';
		$sql_date = '';

		// number of row
		$i = 0;
		// START foreach cities
		foreach ($cities as $key => $value) {
			$i++;

			$data = array(
					'from'			=> $from,
					'thru'			=> $value['code'],
					'weight'		=> $weight,
					'service'		=> '',
					'service_code'	=> '',
				);

	        // CURL
	        curl_setopt($ch, CURLOPT_URL, $url);
	        curl_setopt($ch, CURLOPT_POST, 1);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	        // receive server response
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	        $json = curl_exec($ch);
	        $json = json_decode($json, true);
        	
        	if (isset($json['error']) && $json['error']) {
        		echo 'Error: '. $json['error'];
        		die;
        	}
        	else {
        		$prices = $json['price'];

        		if (!empty($prices)) {
        			// search jne reg
    				$regArrKey = array_search('REG15', array_column($prices, 'service_code'));

					if ($regArrKey) {
						$regArr = $prices[$regArrKey];
						$sql_reg .= " WHEN " . $value['city_id'] . " THEN " . $regArr['price'];
					}
					else {
						$sql_reg .= " WHEN " . $value['city_id'] . " THEN " . $value['jnereg'];	
					}

					// search jne oke
    				$okeArrKey = array_search('OKE15', array_column($prices, 'service_code'));

					if ($okeArrKey) {
						$okeArr = $prices[$okeArrKey];
						$sql_oke .= " WHEN " . $value['city_id'] . " THEN " . $okeArr['price'];
					}
					else {
						$sql_oke .= " WHEN " . $value['city_id'] . " THEN " . $value['jneoke'];	
					}

					// search jne yes
    				$yesArrKey = array_search('YES15', array_column($prices, 'service_code'));

					if ($yesArrKey) {
						$yesArr = $prices[$yesArrKey];
						$sql_yes .= " WHEN " . $value['city_id'] . " THEN " . $yesArr['price'];
					}
					else {
						$sql_yes .= " WHEN " . $value['city_id'] . " THEN " . $value['jneyes'];	
					}

					// date modified
					$sql_date .= " WHEN " . $value['city_id'] . " THEN NOW()";

					// echo $value['city_id'] . ' finish<br />';
        		}
        	}
	    } // END foreach cities
	    curl_close ($ch);

	    $sql .= "jnereg = CASE city_id" . $sql_reg . " ELSE jnereg END";
	    $sql .= ", jneoke = CASE city_id" . $sql_oke . " ELSE jneoke END";
	    $sql .= ", jneyes = CASE city_id" . $sql_yes . " ELSE jneyes END";
	    $sql .= ", date_modified = CASE city_id" . $sql_date . " ELSE date_modified END";

	    // update database
	    $updateJne = $this->model_tool_cron->updateCityJne($sql);
	}
}