<?php
class ControllerAccountPaymentConfirmation extends Controller {
    private $error = array();

	public function index() {
		if (!$this->customer->isLogged() && !$this->request->get['from'] && !isset($this->session->data['from'])) {
			$this->response->redirect(base_url());
		}

		if (!isset($this->session->data['from'])) {
			$this->session->data['from'] = 'email';
		} else {
			unset($this->session->data['from']);
		}

		$this->document->addStyle('catalog/view/javascript/jquery/jquery-ui/jquery-ui.css');
		$this->document->addScript('catalog/view/javascript/jquery/jquery-ui/jquery-ui.min.js');

		$this->load->language('account/payment_confirmation');
        $this->document->setTitle($this->language->get('heading_title'));
        $data['heading_title'] = $this->language->get('heading_title');
		$this->load->model('account/payment_confirmation');

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/payment_confirmation', '', 'SSL')
		);
        
        $data['action'] = $this->url->link('account/payment_confirmation', '', 'SSL');
        $data['back'] = $this->url->link('account/order', '', 'SSL');
        
		$data['text_bank']            = $this->language->get('text_bank');
		$data['text_payment']         = $this->language->get('text_payment');
		$data['ph_bank']              = $this->language->get('ph_bank');
		$data['ph_norekening']        = $this->language->get('ph_norekening');
		$data['ph_atasnama']          = $this->language->get('ph_atasnama');
		$data['ph_jumlah_pembayaran'] = $this->language->get('ph_jumlah_pembayaran');
		$data['ph_tgl_pembayaran']    = $this->language->get('ph_tgl_pembayaran');
		$data['ph_keterangan']        = $this->language->get('ph_keterangan');
		$data['ph_kode']              = $this->language->get('ph_kode');
        
		$data['text_user_bank']       = $this->language->get('text_user_bank');
		$data['text_user_account']    = $this->language->get('text_user_account');
		$data['text_user_name']       = $this->language->get('text_user_name');
		$data['text_payment_invoice'] = $this->language->get('text_payment_invoice');
		$data['text_payment_order']   = $this->language->get('text_payment_order');
		$data['text_payment_bank']    = $this->language->get('text_payment_bank');
		$data['text_payment_total']   = $this->language->get('text_payment_total');
		$data['text_payment_date']    = $this->language->get('text_payment_date');
		$data['text_payment_memo']    = $this->language->get('text_payment_memo');
		$data['text_payment_captcha'] = $this->language->get('text_payment_captcha');
		$data['text_select_bank']     = $this->language->get('text_select_bank');
		$data['text_select_order']    = $this->language->get('text_select_order');     
		$data['attachment_text_file'] = $this->language->get('attachment_text_file');     
                
		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back']     = $this->language->get('button_back');
        
        $filelink = '';

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate() && $this->request->files) {

			$filename = basename(preg_replace('/[^a-zA-Z0-9\.\-\s+]/', '', html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8')));
			
			$file = $filename . '.' . md5(mt_rand());
			move_uploaded_file($this->request->files['file']['tmp_name'], DIR_PAYMENT_CONFIRM . $file);
			$filelink = HTTP_SERVER.'payment_confirm/'.$file;


			if(! isset($this->error['attachment_file'])){
				// unset($this->session->data['captcha']);
				$this->load->model('checkout/order');
				$order_id = (int)$this->request->post['payment_order'];
				$order_status_id = 2;

				$memo = strip_tags($this->request->post['payment_memo']);
				$tanggal = date('d M Y',strtotime($this->request->post['payment_date']));
				$bank = $this->request->post['payment_bank'];
				$this->load->language("payment/{$bank}");
				$title = $this->language->get('text_title');
				$accountno   = $this->config->get("{$bank}_accountno");
				$accountname = $this->config->get("{$bank}_accountname");
              
              $comment = "<b>Data pembayar</b><br>
                  Bank: {$this->request->post['user_bank']}<br>
                  No. Rekening: {$this->request->post['user_account']}<br>
                  Atas Nama: {$this->request->post['user_name']}<br>
                  <br/><b>Data pembayaran</b><br>
                  Jumlah Bayar: {$this->request->post['payment_total']}<br>
                  Tanggal Bayar: {$tanggal}<br>
              ";

              if(! empty($filelink)){
	              $comment .= "
	                  File:
	                  <a href='{$filelink}' target='_blank' class='red hvr-yellow'>Lihat file</a><br>
	              ";
              }

              $comment .= "
                  Memo:
                  {$memo}
              ";
              $notify = 1;

              $this->model_checkout_order->addOrderHistory($order_id, $order_status_id, $comment, $notify);
              
			  $this->session->data['success'] = $this->language->get('text_success');
			  $this->response->redirect($this->url->link('account/order', '', 'SSL'));
            }
		}
        // Payment Methods
		$this->load->model('localisation/language');
		// get terms
		foreach ($this->model_localisation_language->getLanguages() as $lang) {
			if ($lang['code'] == $this->language->get('code')) {
				$bank_term_list = $this->config->get('bank_transfer_list_bank'.$lang['language_id']);
			}
		}

		if (!isset($bank_term_list)) {
			$bank_term_list = $this->config->get('bank_transfer_list_bank1');
		}

		// bank name list
		$bank_name_list = $this->config->get('bank_transfer_list_name');
		// bank code list
		$bank_code_list = $this->config->get('bank_transfer_list_code');
		// bank code list
		$bank_status_list = $this->config->get('bank_transfer_list_status');
		// bank sort order list
		$bank_sort_list = $this->config->get('bank_transfer_list_sort');
		// bank sort order list
		$bank_image_list = $this->config->get('bank_transfer_list_image');

		if (isset($bank_name_list)) {
			foreach ($bank_name_list as $key => $value) {
				// if bank enable
				if ($bank_status_list[$key] == 1) {
					$data['payment_banks'][$bank_code_list[$key]] = array(
						'code'  => $bank_code_list[$key],
						'title' => $bank_name_list[$key]
                    );
				}
			}
		}

		// $results = $this->model_catalog_payment_confirmation->getBank();
		// foreach ($results as $result) {
		// 	if ($this->config->get($result['code'] . '_status')) {
		// 	    $this->load->language("payment/{$result['code']}");
  //               $title = $this->language->get('text_title');
  //               $accountno   = $this->config->get("{$result['code'] }_accountno");
  //               $accountname = $this->config->get("{$result['code'] }_accountname");
		// 		if ($title && $accountno && $accountname) {
		// 		    $data['payment_banks'][$result['code']] = array(
  //                       'accountno' => $accountno,
  //                       'accountname' => $accountname,
  //                       'code'  => $result['code'],
  //                       'title' => $title
  //                   );
		// 		}
		// 	}
		// }
        
        if (isset($this->error['captcha'])) {
			$data['error_captcha'] = $this->error['captcha'];
		} else {
			$data['error_captcha'] = '';
		}
        
        if (isset($this->request->post['captcha'])) {
			$data['captcha'] = $this->request->post['captcha'];
		} else {
			$data['captcha'] = '';
		}
                
        if (isset($this->error['user_bank'])) {
			$data['error_user_bank'] = $this->error['user_bank'];
		} else {
			$data['error_user_bank'] = '';
		}
        
        if (isset($this->request->post['user_bank'])) {
			$data['user_bank'] = $this->request->post['user_bank'];
		} else {
			$data['user_bank'] = '';
		}
                
        if (isset($this->error['user_account'])) {
			$data['error_user_account'] = $this->error['user_account'];
		} else {
			$data['error_user_account'] = '';
		}
        
        if (isset($this->request->post['user_account'])) {
			$data['user_account'] = $this->request->post['user_account'];
		} else {
			$data['user_account'] = '';
		}
        
        if (isset($this->error['user_name'])) {
			$data['error_user_name'] = $this->error['user_name'];
		} else {
			$data['error_user_name'] = '';
		}
        
        if (isset($this->request->post['user_name'])) {
			$data['user_name'] = $this->request->post['user_name'];
		} else {
			$data['user_name'] = '';
		}
                
        if (isset($this->error['payment_order'])) {
			$data['error_payment_order'] = $this->error['payment_order'];
		} else {
			$data['error_payment_order'] = '';
		}
        
        if (isset($this->request->get['order_id'])) {
        	$data['payment_order'] = $this->request->get['order_id'];
        } else if (isset($this->request->post['payment_order'])) {
			$data['payment_order'] = $this->request->post['payment_order'];
		} else {
			$data['payment_order'] = '';
		}
                
        if (isset($this->error['payment_bank'])) {
			$data['error_payment_bank'] = $this->error['payment_bank'];
		} else {
			$data['error_payment_bank'] = '';
		}
        
        if (isset($this->request->post['payment_bank'])) {
			$data['payment_bank'] = $this->request->post['payment_bank'];
		} else {
			$data['payment_bank'] = '';
		}
        
        if (isset($this->error['payment_total'])) {
			$data['error_payment_total'] = $this->error['payment_total'];
		} else {
			$data['error_payment_total'] = '';
		}
        
        if (isset($this->request->post['payment_total'])) {
			$data['payment_total'] = $this->request->post['payment_total'];
		} else {
			$data['payment_total'] = '';
		}
        
        if (isset($this->error['payment_date'])) {
			$data['error_payment_date'] = $this->error['payment_date'];
		} else {
			$data['error_payment_date'] = '';
		}
        
        if (isset($this->request->post['payment_date'])) {
			$data['payment_date'] = $this->request->post['payment_date'];
		} else {
			$data['payment_date'] = '';
		}
        
        if (isset($this->error['payment_memo'])) {
			$data['error_payment_memo'] = $this->error['payment_memo'];
		} else {
			$data['error_payment_memo'] = '';
		}

        if (isset($this->error['attachment_file'])) {
			$data['error_attachment_file'] = $this->error['attachment_file'];
		} else {
			$data['error_attachment_file'] = '';
		}
        
        if (isset($this->request->post['payment_memo'])) {
			$data['payment_memo'] = $this->request->post['payment_memo'];
		} else {
			$data['payment_memo'] = '';
		}

		if (isset($this->request->get['from'])) {
			$data['from'] = $this->request->get['from'];
		} else {
			$data['from'] = '';
		}
        
        // order no
        $this->load->model('account/order');
        $filter = array(
            'order_status_id' => 1
        );
        $data['orders'] = array();
        $orders = $this->model_account_order->getOrdersByFilter($filter);

        foreach($orders as $item){
            $total = $this->currency->format($item['total'], $item['currency_code'], $item['currency_value']);
            $data['orders'][] = array(
				'order_id'   => $item['order_id'],
				'invoice_no' => $item['invoice_no'],
				'total'      => $this->currency->format(floor($item['total']), $this->session->data['currency']), 
				'label'      => "#{$item['invoice_no']}, ". (date('d M Y',strtotime($item['date_added']))) . " ({$total})" 
            );

            if ($item['order_id'] == $data['payment_order']) {
            	$data['payment_total'] = $this->currency->format(floor($item['total']), $this->session->data['currency']);
            } else {
       //      	if ($data['from'] == 'email') {
			  		// $this->response->redirect($this->url->link('account/order', '', 'SSL'));
       //      	}
            }
        }

        if(empty($data['orders'])){
			  $this->session->data['warning'] = $this->language->get('error_no_order');
			  $this->response->redirect($this->url->link('account/order', '', 'SSL'));
        }

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/payment_confirmation', $data));
	}
    
	protected function validate() {
		if ((utf8_strlen(trim($this->request->post['user_bank'])) < 1)) {
			$this->error['user_bank'] = $this->language->get('error_user_bank');
		}
        
        if ((utf8_strlen(trim($this->request->post['user_account'])) < 1)) {
			$this->error['user_account'] = $this->language->get('error_user_account');
		}
        
        if ((utf8_strlen(trim($this->request->post['user_name'])) < 1)) {
			$this->error['user_name'] = $this->language->get('error_user_name');
		}

		if (empty($this->request->post['payment_order'])) {
			$this->error['payment_order'] = $this->language->get('error_payment_order');
		} else {
        	$this->load->model('account/order');

	        $order = $this->model_account_order->getOrder((int)$this->request->post['payment_order']);
	        
	        if (empty($order) || $order['order_status_id']!= 1) {
				$this->error['payment_order'] = $this->language->get('error_payment_order');
			}
	        
	        if (empty($order)) {
				$this->error['payment_total'] = $this->language->get('error_payment_total');
			}

	        if (empty($this->request->post['payment_bank'])) {
				$this->error['payment_bank'] = $this->language->get('error_payment_bank');
			}
		}
        
        
        if ((utf8_strlen(trim($this->request->post['payment_date'])) < 1)) {
			$this->error['payment_date'] = $this->language->get('error_payment_date');
		}
        
        if ((utf8_strlen(trim($this->request->post['payment_memo'])) > 36)) {
			$this->error['payment_memo'] = $this->language->get('error_payment_memo');
		}

		if (!empty($this->request->files)) {
			if ($this->request->files['file']['error'] == 4) {
				$this->error['attachment_file'] = $this->language->get('error_attachment_file_required');
			}
	        
			$filename = basename(preg_replace('/[^a-zA-Z0-9\.\-\s+]/', '', html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8')));
			// Validate the filename length
			if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 64)) {
				$this->error['attachment_file'] = $this->language->get('error_attachment_filename');
			}
			// Allowed file extension types
			$allowed = array(
				'png',
				'jpeg',
				'jpg',
				'bmp',
			);

			if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
				$this->error['attachment_file'] = $this->language->get('error_attachment_filetype');
			}
			// Allowed file extension types
			$allowed = array(
				'image/png',
				'image/jpeg',
				'image/bmp',
			);

			if (!in_array($this->request->files['file']['type'], $allowed)) {
				$this->error['attachment_file'] = $this->language->get('error_attachment_file');
			}

			// Check to see if any PHP files are trying to be uploaded
			$content = '';
			if ($this->request->files['file']['error'] != 4) {
				$content = file_get_contents($this->request->files['file']['tmp_name']);
			}

			if (preg_match('/\<\?php/i', $content)) {
				$this->error['attachment_file'] = $this->language->get('error_attachment_filetype');
			}
			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
				$this->error['attachment_file'] = $this->language->get('error_attachment_fileupload');
			}
			// pre($this->request->files);

			// check if size more than 2mb
			if ($this->request->files['file']['size'] > 2000000) {
				$this->error['attachment_file'] = sprintf($this->language->get('error_attachment_filesize'), '2MB');
			}
		} else {
			$this->error['attachment_file'] = $this->language->get('error_attachment_file_required');
		}

  //       if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
		// 	$this->error['captcha'] = $this->language->get('error_captcha');
		// }

/*
        if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
			$this->error['attachment_file'] = $this->language->get('error_attachment_file');
		}
*/
		return !$this->error;
	}
}