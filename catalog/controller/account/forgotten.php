<?php
class ControllerAccountForgotten extends Controller {
	private $error = array();

	public function index() {
		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		$this->load->language('account/forgotten');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->language('mail/forgotten');

			$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

			$diff = dateDiffrent($customer_info['code_date_added'], date("Y-m-d H:i:s"));

			if(!empty($customer_info['code']) && $customer_info['code_date_added'] && $diff < 3){
				$code = $customer_info['code'];				
			}else{
				$code = token(40);
				$this->model_account_customer->editCode($this->request->post['email'], $code);				
			}

			$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

			$message  = sprintf($this->language->get('text_greeting'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";
			$message .= $this->language->get('text_change') . "\n\n";
			$message .= $this->url->link('account/reset', 'code=' . $code, true) . "\n\n";
			$message .= sprintf($this->language->get('text_ip'), $this->request->server['REMOTE_ADDR']) . "\n\n";

			// TEMPLATE EMAIL

			$content = array();
			$content['text_greeting'] = sprintf($this->language->get('text_greeting'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$content['text_change'] = $this->language->get('text_change');
			$content['link_reset'] = $this->url->link('account/reset', 'code=' . $code, true);
			$content['text_clickhere'] = $this->language->get('text_clickhere');
			$content['text_ip'] = sprintf($this->language->get('text_ip'), $this->request->server['REMOTE_ADDR']);

			$config = array(
				'title' 		=> $subject,
				'logo'          => $this->config->get('config_url') . 'image/' . $this->config->get('config_logo'),
				'url'           => $this->config->get('config_url'),
				'instagram'     => !empty($this->config->get('config_ig')) ? 'https://www.instagram.com/' . $this->config->get('config_ig') : '',
				'whatsapp'      => !empty($this->config->get('config_wa')) ? 'https://api.whatsapp.com/send?phone=' . $this->config->get('config_wa') : '',
				'facebook'      => !empty($this->config->get('config_fb')) ? 'https://www.facebook.com/' . $this->config->get('config_fb') : '',
				'line'          => !empty($this->config->get('config_line')) ? 'https://line.me/R/ti/p/' . $this->config->get('config_line') : '',
				'youtube'       => !empty($this->config->get('config_youtube')) ? 'https://www.youtube.com/channel/' . $this->config->get('config_youtube') : '',
				'img_instagram' => $this->config->get('config_url') . 'catalog/view/theme/default/image/socmed/instagram.png',
				'img_whatsapp'  => $this->config->get('config_url') . 'catalog/view/theme/default/image/socmed/whatsapp.png',
				'img_facebook'  => $this->config->get('config_url') . 'catalog/view/theme/default/image/socmed/facebook.png',
				'img_line'      => $this->config->get('config_url') . 'catalog/view/theme/default/image/socmed/line.png',
				'img_youtube'   => $this->config->get('config_url') . 'catalog/view/theme/default/image/socmed/youtube.png',
			);

			$content['header'] = $this->load->view('mail/layout/header', $config);
			$content['footer'] = $this->load->view('mail/layout/footer', $config);

			$html = $this->load->view('mail/forgotten', $content);
			// TEMPLATE EMAIL

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->request->post['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($html);
			$mail->send();

			$this->session->data['success'] = $this->language->get('text_success');

			// Add to activity log
			if ($this->config->get('config_customer_activity')) {
				$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

				if ($customer_info) {
					$this->load->model('account/activity');

					$activity_data = array(
						'customer_id' => $customer_info['customer_id'],
						'name'        => $customer_info['firstname'] . ' ' . $customer_info['lastname']
					);

					$this->model_account_activity->addActivity('forgotten', $activity_data);
				}
			}

			if (isset($this->request->post['mode']) && $this->request->post['mode'] == 'popup') {
				$data['redirect'] = $this->url->link('account/forgotten');

				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($data));
			} else {
				$this->response->redirect($this->url->link('account/forgotten', '', true));
			}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_forgotten'),
			'href' => $this->url->link('account/forgotten', '', true)
		);

		$data['heading_title'] = explode(' ', $this->language->get('heading_title'));

		$data['text_your_email'] = $this->language->get('text_your_email');
		$data['text_email'] = $this->language->get('text_email');

		$data['entry_email'] = $this->language->get('entry_email');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['action'] = $this->url->link('account/forgotten', '', true);

		$data['back'] = $this->url->link('account/login', '', true);

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = '';
		}

		//custome here
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['mode']) && $this->request->post['mode'] == 'popup') {
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($data));
		} else {
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('account/forgotten', $data));
		}
	}

	protected function validate() {
		if (!isset($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_email');
		} elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_email');
		}

		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

		if ($customer_info && !$customer_info['approved']) {
			$this->error['warning'] = $this->language->get('error_approved');
		}

		return !$this->error;
	}
}
