<?php
// HTTP
define('HTTP_SERVER', 'http://localhost:8813/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost:8813/');

// DIR
define('DIR_APPLICATION', '/Library/WebServer/Documents/freshsembako/catalog/');
define('DIR_SYSTEM', '/Library/WebServer/Documents/freshsembako/system/');
define('DIR_IMAGE', '/Library/WebServer/Documents/freshsembako/image/');
define('DIR_LANGUAGE', '/Library/WebServer/Documents/freshsembako/catalog/language/');
define('DIR_TEMPLATE', '/Library/WebServer/Documents/freshsembako/catalog/view/theme/');
define('DIR_CONFIG', '/Library/WebServer/Documents/freshsembako/system/config/');
define('DIR_CACHE', '/Library/WebServer/Documents/freshsembako/system/storage/cache/');
define('DIR_DOWNLOAD', '/Library/WebServer/Documents/freshsembako/system/storage/download/');
define('DIR_LOGS', '/Library/WebServer/Documents/freshsembako/system/storage/logs/');
define('DIR_MODIFICATION', '/Library/WebServer/Documents/freshsembako/system/storage/modification/');
define('DIR_UPLOAD', '/Library/WebServer/Documents/freshsembako/system/storage/upload/');
define('DIR_PAYMENT_CONFIRM', '/Library/WebServer/Documents/freshsembako/payment_confirm/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '12345');
define('DB_DATABASE', 'freshsem_bakodb');  // copy db fresh di oc_db_fresh
define('DB_PORT', '3306');
define('DB_PREFIX', '');
