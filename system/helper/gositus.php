<?php
/**
 * gositus helper
 *
 * A helper file for common function
 *
 * @package		Helper
 * @author		Gositus
 * @copyright	Copyright (c) 2016, Gositus.
 * @link			http://gositus.com
 * @since		Version 1.0
 * @filesource
 */

// --------------------------------------------------------------------

if (!function_exists('number_format_short')) {
    function number_format_short( $n, $precision = 1 ) {
        if ($n < 900) {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } else if ($n < 900000) {
            // 0.9k-850k
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'rb';
        } else if ($n < 900000000) {
            // 0.9m-850m
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'jt';
        } else if ($n < 900000000000) {
            // 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } else {
            // 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }
      // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
      // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ( $precision > 0 ) {
            $dotzero = '.' . str_repeat( '0', $precision );
            $n_format = str_replace( $dotzero, '', $n_format );
        }
        return $n_format . $suffix;
    }
}

/**
 * Print variable in readable format and stop php execution by default
 *
 * @param	mixed	the variable to be printed. Support all variable types (array, object, string, integer, etc) 
 * @param	boolean	whether to stop php execution or not. (Default=true)
 */
if(!function_exists('pre')) {
	function pre($variable, $exit=true) {
		echo '<pre>';
		print_r($variable);
		echo '</pre>';
		if($exit) exit;
	}
}

/**
 * Check if current IP is Gositus IP
 *
 * @return boolean check result (true or false)
 */
if(!function_exists('ip_gositus')) {
	function ip_gositus() {
		if($_SERVER['REMOTE_ADDR']=='122.102.40.67') return true;
		return false;
	}
}

/**
 * Random AlphaNumeric
 *
 * @return 5 digits Aplha Numeric
 */
if(!function_exists('random_alphanumeric')) {
    function random_alphanumeric($random_string_length = 5) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $string = '';
        $max = strlen($characters) - 1;
        for ($i = 0; $i < $random_string_length; $i++) {
          $string .= $characters[mt_rand(0, $max)];
        }
        return $string;
    }
}

/*
 * Base URL
 * return example: http://lab.gositus.com/ocDevelopment/goadmin/ (admin)
 */

if (!function_exists('base_url')) {
    function base_url() {        
        if(strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'){
            $base = HTTPS_SERVER;
        }else {
            $base = HTTP_SERVER;
        }
        
        return $base;
    }
}


/*
 * Base URL
 * return example: http://lab.gositus.com/ocDevelopment/
 */

if (!function_exists('catalog_url')) {
    function catalog_url() {        
        if(strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'){
            $base = HTTPs_CATALOG;
        }else {
            $base = HTTP_CATALOG;
        }
        
        return $base;
    }
 }

 
if (!function_exists('get_disc')) {
    function get_disc($price, $special) {        
        $disc = floor(($price - $special) / $price * 100 ). '%';
        return $disc;
    }
 }

/*
 * Clean/replace string
 * return is string
 */

if (!function_exists('cleanString')) {
    function cleanString($text) {
        $utf8 = array(
            '/[áàâãªä]/u'   =>   'a',
            '/[ÁÀÂÃÄ]/u'    =>   'A',
            '/[ÍÌÎÏ]/u'     =>   'I',
            '/[íìîï]/u'     =>   'i',
            '/[éèêë]/u'     =>   'e',
            '/[ÉÈÊË]/u'     =>   'E',
            '/[óòôõºö]/u'   =>   'o',
            '/[ÓÒÔÕÖ]/u'    =>   'O',
            '/[úùûü]/u'     =>   'u',
            '/[ÚÙÛÜ]/u'     =>   'U',
            '/ç/'           =>   'c',
            '/Ç/'           =>   'C',
            '/ñ/'           =>   'n',
            '/Ñ/'           =>   'N',
            '/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u'    =>   ' ', // Literally a single quote
            '/[“”«»„]/u'    =>   ' ', // Double quote
            '/ /'           =>   ' ', // nonbreaking space (equiv. to 0x160)
        );
        return preg_replace(array_keys($utf8), array_values($utf8), $text);
    }
}

/**
 * Replace space with dush
 * return is string
 */
if (!function_exists('hyphenize')) {
    function hyphenize($string) {
        return 
        strtolower(
              preg_replace(
                array('#[\\s-]+#', '#[^A-Za-z0-9\. -]+#'),
                array('-', ''),
                 cleanString(
                  urldecode($string)
                 )
            )
        )
        ;
    }
}

/**
 * replace/remove dot(.) in string
 * return is string date
 * Kyky's created
 */
if(!function_exists('set_number_format'))
{
    function set_number_format($string=NULL,$decimals=0,$to_insert_db=FALSE)
    {
        //Jika ingin di jadikan format mysql maka harus dibersihkan tanda pemisah 
        if(!empty($string)){
            $dot = '.';
            if($to_insert_db){

                $string = str_replace(".", "", $string);
                $dot = '';
                //$decimals = 12;//Ini data yang masuk ke database adalah double
            }

            return number_format($string,$decimals,',',$dot);
        }
    }
}

/*******
* remove comma(,) in numeric string
* return is string 
* Kyky's created
********/
function removeComma($num) {

     if(is_numeric($num)) {
         $num = preg_replace('/[^0-9,]/s', '', $num);
     }

     return $num;
}

/*******
* get diffrent from two date
* return is hour 
* Kyky's created
********/
function dateDiffrent($date_1, $date_2){
    if(!empty($date_1) && !empty($date_2)){

        /*$date1 = new DateTime($date_1);
        $date2 = new DateTime($date_2);

        //determine what interval should be used - can change to weeks, months, etc
        $interval = new DateInterval('PT1H');

        //create periods every hour between the two dates
        $periods = new DatePeriod($date1, $interval, $date2);

        //count the number of objects within the periods
        $hours = iterator_count($periods);*/
        //pre($date_2);

        // $date1 = strtotime($date_1);
        // $date2 = strtotime($date_2);
        // $diff = $date2 - $date1;
        // //pre($diff);
        // $hours = ($diff / 3600);

        $date1 = new DateTime($date_1);
        $date2 = new DateTime($date_2);
        $diff = $date2->diff($date1);
        
        $hours = $diff->h;
        $hours = $hours + ($diff->days*24);

        return $hours;
    }
}


if(!function_exists('rebuildMeta'))
{
    /**
     * Meta Setting
     * Added by Wisnu
     * @param  array $product_description    product/category/brand/etc description that inputed by user
     * @return array                   
     */
    function rebuildMeta($product_description)
    {
        // if more than 1 language
        foreach ($product_description as $language_id => $value) {
            // if meta title is empty use product/category/brand name
            if (isset($value['meta_title']) && isset($value['name'])) {
                if(empty($value['meta_title'])){                    
                    $product_description[$language_id]['meta_title'] = $value['name'];
                }
            }
            elseif (isset($value['meta_title']) && isset($value['title'])) {
                if(empty($value['meta_title'])){                    
                    $product_description[$language_id]['meta_title'] = $value['title'];
                }
            }
            
            // if meta description is empty
            // use product/category/brand description
            // if product/category/brand description is empty too, use product/category/brand name
            if (isset($value['meta_description']) && isset($value['name'])) {
                if(empty($value['meta_description'])){
                    // limit 200 character
                    $product_description[$language_id]['meta_description'] = substr(strip_tags(html_entity_decode($value['description'])), 0, 200);

                    if(empty($value['description'])){
                        $product_description[$language_id]['meta_description'] = $value['name'];
                    }
                }
            }
            elseif (isset($value['meta_description']) && isset($value['title'])) {
                if(empty($value['meta_description'])){
                    // limit 200 character
                    $product_description[$language_id]['meta_description'] = substr(strip_tags(html_entity_decode($value['description'])), 0, 200);

                    if(empty($value['description'])){
                        $product_description[$language_id]['meta_description'] = $value['title'];
                    }
                }
            }

            // if meta keyword is empty use tag
            if (isset($value['meta_keyword']) && isset($value['tag'])) {
                if(empty($value['meta_keyword'])){
                    $product_description[$language_id]['meta_keyword'] = $value['tag'];
                } 
            }
        }

        return $product_description;
    }
}

if(!function_exists('rebuildSeo'))
{
    /**
     * Generate seo
     * Added by Wisnu
     * @param  array  $product_description product/category/brand description
     * @param  string  $keyword             
     * @param  integer $language_id         
     * @return string                       
     */
    function rebuildSeo($product_description, $keyword, $language_id = 1)
    {
        if($keyword){
            $keyword = hyphenize(trim(strtolower(htmlspecialchars_decode($keyword))));
        }
        else{
            // if language id exist
            if (isset($product_description[$language_id])) {
                // if name exist use name, if not use title
                if (isset($product_description[$language_id]['name'])) {
                    $name =  $product_description[$language_id]['name'];
                }
                elseif (isset($product_description[$language_id]['title'])) {
                    $name =  $product_description[$language_id]['title'];
                }
                
            }
            elseif (isset($product_description['name'])) {
                $name =  $product_description['name'];
            }
            
            $keyword = hyphenize(trim(strtolower(htmlspecialchars_decode($name))));       
        }   

        return $keyword;
    }
}


if(!function_exists('checkBrightness'))
{
    /**
     * Check brightness from hex color
     * Added by Wisnu
     * @param  string $hex hex color without #   
     * @return boolean      
     */
    function checkBrightness($hex)
    {
        //break up the color in its RGB components
        $r = hexdec(substr($hex,0,2));
        $g = hexdec(substr($hex,2,2));
        $b = hexdec(substr($hex,4,2));

        // differently. That is a green of 128 might be brighter than a red of 128.
        if($r + $g + $b > 382){
            return false;
        }else{
            return true;
        }
    }
}

if(!function_exists('generateRandomString'))
{
    function generateRandomString($length = 10)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}


