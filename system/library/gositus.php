<?php
class Gositus {
	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->db = $registry->get('db');
		$this->session = $registry->get('session');
		$this->request = $registry->get('request');
	}

	public function user(){
		if(! empty($this->session->data['user_id']) && (int)$this->session->data['user_id'] < 0){
			return true;
		}
		return false;
	}

	public function hide(){
		if(! $this->user()) return 'gohide';
		return false;
	}

	public function ip(){
		$ip = gethostbyname('lab.gositus.com');
		
		if($this->request->server['REMOTE_ADDR']==$ip){
			return true;
		}
		return false;
	}

	/**
	 * Generate unix code from fullname
	 * return is string 
	 * Created by Kyky Sukiawan
	 */
	public function affiliateNameCode($firstname=null, $lastname=null){		
		$name = $firstname && $lastname ? $firstname. ' ' .$lastname : $firstname;

        $code = trim(strtolower(str_replace(' ','',$name)));
        
        if($code){  

            $affiliate = $this->checkAffiliateCode($code);
            if($affiliate){
                $str = explode('-',$affiliate['code']);
                $last = end($str);

                if ((int)$last != 0) {
                    $key = key($str);  
                    $str[$key] = (int)$last + 1;
                    $code = implode($str,'-');                      
                } else {
                    $code .= '-1';
                }   
            }               
        }

        return $code;
	}

	public function checkAffiliateCode($affiliate_code) {

		if(!empty($affiliate_code)){
			/*global $loader, $registry;

		    $loader->model('marketing/affiliate');
		    $model = $registry->get('model_marketing_affiliate');
		    $result = $model->getAffiliateByCode($affiliate_code);

			return $result;*/

			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "affiliate WHERE code = '" . $this->db->escape($affiliate_code) . "'");
			
			return $query->num_rows;
		}
	}
}